package com.fitfarm.demo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import user.techokissan.R
import user.techokissan.databinding.ItemListDateLayoutBinding
import user.techokissan.models.SubscriptionTypeModel

class DateAdapter(
    private var context: Context,
    private var subscriptionTypeModel: ArrayList<SubscriptionTypeModel>
) : RecyclerView.Adapter<DateAdapter.ViewHolder>() {

    private var selectedPosition = -1
    class ViewHolder(var viewBinding: ItemListDateLayoutBinding) : RecyclerView.ViewHolder(viewBinding.root)

     private var   onDateListener : OnSubscriptionListener

     init {
         onDateListener  = context as OnSubscriptionListener
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListDateLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false))
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.viewBinding.mDateTV.text = subscriptionTypeModel[position].type

             if (selectedPosition == position ){
                holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.stoke_edt_black)
            }else{

                holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.white))
                holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.draw_not_selected)
            }





            holder.viewBinding.mDateTV.setOnClickListener {
                selectedPosition = position
                onDateListener.onSubscriptionClick(position , subscriptionTypeModel )
                notifyDataSetChanged()


        }


        /*  println("==>" + Gson().toJson(data))

          // val ischecked = if (position === checkedposition) true else false
          holder.viewBinding.mDateLL.isActivated = data[position].isSelected*/
        /* if (!data[position].isSelected){
            holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.edit_bg_)
        }else{
            holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.black))
            holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.edt_bg_grey)

        }
*/



    }

    override fun getItemCount(): Int {
        return subscriptionTypeModel.size;

    }


    interface OnSubscriptionListener{
        fun onSubscriptionClick(
            position: Int,
            subscriptionTypeModel: ArrayList<SubscriptionTypeModel>
        )
    }
}