package user.techokissan.demo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.fitfarm.demo.DateAdapter
import user.techokissan.R
import user.techokissan.databinding.ItemListDaysLayoutBinding
import user.techokissan.databinding.ItemListDaysLayoutNewBinding
import user.techokissan.models.SubscriptionDaysTypeModel

class DayAdapterNew(
    private var context: Context,
    private var subscriptionDaysTypeModel: ArrayList<SubscriptionDaysTypeModel>,var callbackk:OnWeeklyCountListeners
) : RecyclerView.Adapter<DayAdapterNew.ViewHolder>() {

    private var selectedPosition = -1

    class ViewHolder(var viewBinding: ItemListDaysLayoutNewBinding) :
        RecyclerView.ViewHolder(viewBinding.root)



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListDaysLayoutNewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.viewBinding.mTitle.text = subscriptionDaysTypeModel[position].days

        if (selectedPosition == position) {
            holder.viewBinding.mAddLL.visibility = View.GONE
            holder.viewBinding.mCountLL.visibility = View.VISIBLE
            holder.viewBinding.mCountTV.text ="Selected"
        } else {
            holder.viewBinding.mAddLL.visibility = View.VISIBLE
            holder.viewBinding.mCountLL.visibility = View.GONE
            holder.viewBinding.mCountTV.text = ""
            subscriptionDaysTypeModel[position].cartCount = 0
        }

        holder.viewBinding.mAddLL.setOnClickListener {
            subscriptionDaysTypeModel[position].cartCount++
            holder.viewBinding.mCountLL.visibility = View.VISIBLE
            holder.viewBinding.mAddLL.visibility = View.GONE
            holder.viewBinding.mCountTV.text =
                subscriptionDaysTypeModel[position].cartCount.toString()
            selectedPosition = position
            callbackk.onWeekClick(position, subscriptionDaysTypeModel)
            notifyDataSetChanged()
            //  addToCart(mPackage[position].cartCount.toString() ,subscriptionDaysTypeModel[position].product_id , subscriptionDaysTypeModel[position].id )

        }



    }

    override fun getItemCount(): Int {
        return subscriptionDaysTypeModel.size

    }


    interface OnWeeklyCountListeners {
        fun onWeekClick(position: Int, data: ArrayList<SubscriptionDaysTypeModel>)
    }
}