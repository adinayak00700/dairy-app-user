package com.fitfarm.demo

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import user.techokissan.databinding.ItemListDateLayoutBinding

class OrderOnGoningAdapter(
) : RecyclerView.Adapter<OrderOnGoningAdapter.ViewHolder>() {

    private var selectedPosition = -1
    class ViewHolder(var viewBinding: ItemListDateLayoutBinding) : RecyclerView.ViewHolder(viewBinding.root)

    // private var   onDateListener : OnDateListener

    /* init {
         onDateListener  = context as OnDateListener
     }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListDateLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false))
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        /*  println("==>" + Gson().toJson(data))

          // val ischecked = if (position === checkedposition) true else false
          holder.viewBinding.mDateLL.isActivated = data[position].isSelected*/
        /* if (!data[position].isSelected){
            holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.edit_bg_)
        }else{
            holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.black))
            holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.edt_bg_grey)

        }
*/


        /*     if (selectedPosition == position ){
                 holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context.requireContext(), R.color.white))
                 holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context.requireContext(), R.drawable.coustom_button)
             }else{

                 holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context.requireContext(), R.color.black))
                 holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context.requireContext(), R.drawable.unselected_bg)
             }


             holder.viewBinding.mDateTV.text =
                 AppUtils.dateFormat(data[position].date) + " ( " + data[position].days + " ) "



             holder.viewBinding.mDateTV.setOnClickListener {
                 selectedPosition = position
                 onDateListener.onDateClick(position , data , vendorID)
                 notifyDataSetChanged()


                 *//* if (!data[position].isSelected) {
                 data[position].isSelected = true
                 //   holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.white))
                 // holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.edit_bg_)
                 onDateListener.onDateClick(position, data, vendorID)
                 notifyItemChanged(position)
             }*//**//*else{
                data[position].isSelected = false
                notifyItemChanged(position)
                onDateListener.onDateClick(position , data , vendorID)
                holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.edt_bg_grey)

            }*//*

        }*/
    }

    override fun getItemCount(): Int {
        return 6

    }
}