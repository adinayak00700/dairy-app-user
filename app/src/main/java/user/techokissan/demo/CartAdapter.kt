package user.techokissan.demo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.adapter.AddressListAdapter
import user.techokissan.databinding.ItemListCartLayoutBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.CartListDataRes
import user.techokissan.models.UserAddressDataListRes
import user.techokissan.networking.RestClient
import java.util.HashMap

class CartAdapter(private var context : Context, private var data: ArrayList<CartListDataRes>) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    //  private  var viewModel: AddressVM? = null
    private lateinit  var  onItemListener : OnRemoveCartListener


    init {
        onItemListener  = context as OnRemoveCartListener
    }

    class ViewHolder(var viewBinding: ItemListCartLayoutBinding) : RecyclerView.ViewHolder(viewBinding.root)


    /*  @SuppressLint("UseCompatLoadingForDrawables")
      private val images = intArrayOf(
          R.drawable.ic_rectangle_first ,
          R.drawable.ic_rectangle_second  , R.drawable.ic_rectangle_third , R.drawable.ic_rectangle_fourth)
  */

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListCartLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.viewBinding.title.text =data[position].product_name
       // holder.viewBinding.mPriceTV.text ="₹ "+data[position].netPrice
        holder.viewBinding.qtyCount.text ="Qty "+data[position].cart_qty
        holder.viewBinding.qty.text ="₹ "+data[position].price+" / Qty"

        Glide.with(context).load(data[position].product_image).into(holder.viewBinding.restroImage)
        // holder.viewBinding.mBgLL.setBackgroundResource(images[position % 3])
        holder.viewBinding.delete.setOnClickListener{
            notifyItemChanged(position)
            onItemListener.onRemoveClick(data , position)
        }
    }

    override fun getItemCount(): Int {
        return  data.size

    }


    interface OnRemoveCartListener{
        fun onRemoveClick(data: ArrayList<CartListDataRes>, position: Int)
    }
}