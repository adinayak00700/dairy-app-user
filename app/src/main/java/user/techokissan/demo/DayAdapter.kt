package user.techokissan.demo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.fitfarm.demo.DateAdapter
import user.techokissan.R
import user.techokissan.databinding.ItemListDaysLayoutBinding
import user.techokissan.models.SubscriptionDaysTypeModel

class DayAdapter(
    private var context: Context,
    private var subscriptionDaysTypeModel: ArrayList<SubscriptionDaysTypeModel>
) : RecyclerView.Adapter<DayAdapter.ViewHolder>() {

    private var selectedPosition = -1

    class ViewHolder(var viewBinding: ItemListDaysLayoutBinding) :
        RecyclerView.ViewHolder(viewBinding.root)

    private var onDateListener: OnWeeklyCountListener

    init {
        onDateListener = context as OnWeeklyCountListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListDaysLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.viewBinding.mTitle.text = subscriptionDaysTypeModel[position].days

        if (selectedPosition == position) {
            holder.viewBinding.mAddLL.visibility = View.GONE
            holder.viewBinding.mCountLL.visibility = View.VISIBLE
            holder.viewBinding.mCountTV.text =
                subscriptionDaysTypeModel[position].cartCount.toString()
        } else {
            holder.viewBinding.mAddLL.visibility = View.VISIBLE
            holder.viewBinding.mCountLL.visibility = View.GONE
            holder.viewBinding.mCountTV.text = ""
            subscriptionDaysTypeModel[position].cartCount = 0
        }

        holder.viewBinding.mAddLL.setOnClickListener {
            subscriptionDaysTypeModel[position].cartCount++
            holder.viewBinding.mCountLL.visibility = View.VISIBLE
            holder.viewBinding.mAddLL.visibility = View.GONE
            holder.viewBinding.mCountTV.text =
                subscriptionDaysTypeModel[position].cartCount.toString()
            selectedPosition = position
            onDateListener.onWeekClick(position, subscriptionDaysTypeModel)
            notifyDataSetChanged()
            //  addToCart(mPackage[position].cartCount.toString() ,subscriptionDaysTypeModel[position].product_id , subscriptionDaysTypeModel[position].id )

        }

        holder.viewBinding.mPlusLL.setOnClickListener {
            subscriptionDaysTypeModel[position].cartCount++
            holder.viewBinding.mCountLL.visibility = View.VISIBLE
            holder.viewBinding.mAddLL.visibility = View.GONE
            holder.viewBinding.mCountTV.text = subscriptionDaysTypeModel[position].cartCount.toString()
            selectedPosition = position
            onDateListener.onWeekClick(position, subscriptionDaysTypeModel)
            notifyDataSetChanged()
            //addToCart(mPackage[position].cartCount.toString() ,mPackage[position].product_id , mPackage[position].id)
        }

        holder.viewBinding.mMinusLL.setOnClickListener {
            subscriptionDaysTypeModel[position].cartCount--
            if (subscriptionDaysTypeModel[position].cartCount > 0) {
                holder.viewBinding.mCountTV.text =
                    subscriptionDaysTypeModel[position].cartCount.toString()
                holder.viewBinding.mCountLL.visibility = View.VISIBLE
                holder.viewBinding.mAddLL.visibility = View.GONE
                selectedPosition = position
                onDateListener.onWeekClick(position, subscriptionDaysTypeModel)
                notifyDataSetChanged()
                // addToCart(mPackage[position].cartCount.toString() ,mPackage[position].product_id , mPackage[position].id)
            } else {
                holder.viewBinding.mCountLL.visibility = View.GONE
                holder.viewBinding.mAddLL.visibility = View.VISIBLE
                //  addToCart(mPackage[position].cartCount.toString() ,mPackage[position].product_id , mPackage[position].id)
            }
        }


    }

    override fun getItemCount(): Int {
        return subscriptionDaysTypeModel.size

    }


    interface OnWeeklyCountListener {
        fun onWeekClick(position: Int, data: ArrayList<SubscriptionDaysTypeModel>)
    }
}