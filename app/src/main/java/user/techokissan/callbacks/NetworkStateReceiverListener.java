package user.techokissan.callbacks;

public interface NetworkStateReceiverListener {

    void onInternetConnectionEstablished();

    void onInternetConnectionNotFound();


}
