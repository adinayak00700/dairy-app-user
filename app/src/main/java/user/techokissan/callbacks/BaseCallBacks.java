package user.techokissan.callbacks;

public interface BaseCallBacks {



    void showLoader();

    void dismissLoader();

    void onFragmentDetach(String fragmentTag);

}
