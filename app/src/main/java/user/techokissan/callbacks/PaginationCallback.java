package user.techokissan.callbacks;

public interface PaginationCallback{
    public void loadNextPage();
}