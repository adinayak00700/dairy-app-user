package user.techokissan.callbacks;


public interface AddressCallbacks {

    void onPickUpAddressFound(String pickAddress);

    void onDropAddressFound(String dropAddress);

}
