package user.techokissan.models

import java.io.Serializable

data class SubscriptionDaysTypeModel(var id : Int , var days : String , var cartCount : Int) : Serializable
