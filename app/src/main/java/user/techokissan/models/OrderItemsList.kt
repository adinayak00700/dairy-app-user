package user.techokissan.models

import java.io.Serializable

data class OrderItemsList(

    var result : Boolean,
    var message : String,
    var data : ArrayList<OrderItemsDataList> ,




) : Serializable

data class OrderItemsDataList(

    var id : String,
    var orderID : String,
    var productID : String,
    var package_id : String,
    var qty : String,
    var productName:String,
    var price : String,
    var net_price : String,
    var status : String,
    var added_on : String,
    var updated_on : String,
    var image : String,

): Serializable
