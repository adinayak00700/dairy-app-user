package user.techokissan.models

import java.io.Serializable

data class ModelMySubscription(
    val `data`: List<SubscriptionData>,
    val message: String,
    val result: Boolean
)

data class SubscriptionData(

    val created_at: String,
    val description: String,
    val id: String,
 val subscription:String,
    val exp_date:String,
    val order_id:String,
    val packageDetail: Package,
    val amount:String,
    val payment:String,
    val billAmount:String
):Serializable

