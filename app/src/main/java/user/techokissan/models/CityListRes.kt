package user.techokissan.models

import java.io.Serializable

data class CityListRes(

    var result: Boolean,
    var message: String,
    var data: ArrayList<CityListDataRes>
)  : Serializable

    data class CityListDataRes(
        var id: String,
        var name: String,
        var state_id: String,
        var status: String,
        var is_deleted: String,
        var created_at: String,
        var updated_at: String,

    ) : Serializable
