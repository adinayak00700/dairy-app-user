package user.techokissan.models

data class ModelDays(
    val `data`: List<DaysData>,
    val message: String,
    val result: Boolean
)

data class DaysData(
    val amount: String,
    val created_at: String,
    val delivery_date: String,
    val id: String,
    val order_id: String,
    val package_id: String,
    val status: String,
    val subscription_id: String,
    val subscription_type: String,
    val updated_at: String,
    val user_id: String
)