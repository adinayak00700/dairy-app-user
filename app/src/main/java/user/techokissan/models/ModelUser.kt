package user.techokissan.models

data class ModelUser(
    val `data`: UserDetails,
    val message: String,
    val result: Boolean
)

data class UserDetails(
    val email: String,
    val image: String,
    val mobile: String,
    val token: String,
    val name: String,
    val address: String,
    val wallet:Double
)