package user.techokissan.models

import java.io.Serializable

data class LocalityListRes(

    var result: Boolean,
    var message: String,
    var data: ArrayList<LocalityListDataRes>
)  : Serializable

data class LocalityListDataRes(
    var id: String,
    var name: String,
    var state_id: String,
    var city_id: String,
    var pin_code: String,
    var status: String,
    var is_deleted: String,
    var created_at: String,
    var updated_at: String,
) : Serializable
