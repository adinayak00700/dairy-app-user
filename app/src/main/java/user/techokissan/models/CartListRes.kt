package user.techokissan.models

import java.io.Serializable

data class CartListRes(

 var result : Boolean,
 var message : String,
 var order_amt : String,
 var total_amount : String,
 var data : ArrayList<CartListDataRes>,
)  : Serializable

data class CartListDataRes(
    var id : String,
    var parent : String,
    var category_id : String,
    var product_name : String,
    var description : String,
    var unit_value : String,
    var unit : String,
    var mrp : String,
    var price : String,
    var product_image : String,
    var stock_status : String,
    var stock_count : String,
    var status : String,
    var is_deleted : String,
    var created_at : String,
    var updated_at : String,
    var cart_qty : String,
    var netPrice : String,
) :Serializable

data class CustomData(
    var productID : String ,
                      var package_id : String ,
    var quantity : String

    ) : Serializable




