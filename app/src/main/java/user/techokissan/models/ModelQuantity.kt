package user.techokissan.models

import java.io.Serializable

data class ModelQuantity( var title : String , var quantity : Int) : Serializable
