package user.techokissan.models

import java.io.Serializable
import java.lang.reflect.Array

data class UserAddressListRes(
    var result : Boolean,
    var message : String,
    var data : ArrayList<UserAddressDataListRes>

)  : Serializable


data class UserAddressDataListRes(

    var id : String,
    var user_id : String,
    var flat_no : String,
    var building_name : String,
    var landmark : String,
    var type : String,
    var pin_code : String,
    var country : String,
    var state_id : String,
    var city_id : String,
    var contact_person_mobile : String,
    var locality_id : String,
    var userID : String,
    var phone : String,
    var created_at : String,
    var updated_at : String,
    var full_address : String,


)  : Serializable