package user.techokissan.models

import java.io.Serializable

data class ModelProducts(
    val `data`: List<Product>,
    val message: String,
    val result: Boolean
)

data class Product(
    val category_id: String,
    val cost_price: String,
    val created_at: String,
    val description: String,
    val id: String,
    val product_image: String,
    val is_deleted: String,
    val mrp: String,
    val product_name: String,
    val price: String,
    val status: String,
    val unit: String,
    val unit_value: String,
    val updated_at: String,
    var cartCount: Int,
    var count: Int,
    val `package`: List<Package>,
) : Serializable

data class Package(
    val amount: String,
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val name: String,
    val trial: String,
    val product_id: String,
    val qty: String,
    val status: String,
    val updated_at: String
) : Serializable

