package user.techokissan.models

import java.io.Serializable

data class SubscriptionTypeModel(var id : Int , var type : String) : Serializable
