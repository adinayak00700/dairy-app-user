package user.techokissan.models

import java.io.Serializable

data class ModelVendor(
    val `data`: List<Vendor>,
    val message: String,
    val result: Boolean
)

data class Vendor(
    val created_at: String,
    val doc: String,
    val email: String,
    val id: String,
    val is_deleted: String,
    val mobile: String,
    val name: String,
    val ratingAvg: Int,
    val lab_report: String,
    val image: String,
    val status: String,
    val updated_at: String
)