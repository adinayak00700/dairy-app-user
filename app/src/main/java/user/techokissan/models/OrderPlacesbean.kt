package user.techokissan.models

import java.io.Serializable

data class OrderPlacesbean(
    var result : Boolean,
    var message : String,
    var orderID : Int
) : Serializable
