package user.techokissan.models


import java.io.Serializable

data class ModelTransaction(
    val `data`: List<Transactionn>,
    val message: String,
    val result: Boolean
)
data class Transactionn(
    val id: String,
    val userID: String,
    val txn_no: String,
    val amount: String,
    val type: String,
    val note: String,
    val against_for: String,
    val paid_by: String,
    val orderID: String,
    val status: String,
    val transaction_at: String,
)
