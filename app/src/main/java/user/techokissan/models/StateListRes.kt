package user.techokissan.models

import java.io.Serializable

data class StateListRes(

    var result: Boolean,
    var message: String,
    var data: ArrayList<StateListDataRes>
)  : Serializable

data class StateListDataRes(
    var id: String,
    var name: String,
    var status: String,
    var is_deleted: String,
    var created_at: String,
    var updated_at: String,
) : Serializable

