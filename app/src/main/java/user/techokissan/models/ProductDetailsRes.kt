package user.techokissan.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProductDetailsRes(

 var result : Boolean,
 var message : String,
 var data : ProductDetailsDataRes
)  : Serializable

data class ProductDetailsDataRes(
    var message : String,
    var category_id : String,
    var parent : String,
    var product_name : String,
    var description : String,
    var unit_value : String,
    var unit : String,
    var mrp : String,
    var price : String,
    var cost_price : String,
    var product_image : String,
    var status : String,
    var is_deleted : String,
    var created_at : String,
    var updated_at : String,
    var cartCount : String,
    var stock_count : String,
    var stock_status : String,
    var id : String,
    @SerializedName("package")
    var mPackage : ArrayList<ProductPackageRes>

) : Serializable


data class ProductPackageRes(
    var id : String,
    var product_id : String,
    var name : String,
    var qty : String,
    var amount : String,
    var is_deleted : String,
    var created_at : String,
    var updated_at : String,
    var cartCount: Int,
    var trial: String
): Serializable

