package user.techokissan.models

import java.io.Serializable

data class AddProductModelRes(
    var result : Boolean,
    var message : String,
    var data : String,
    var addressID : String,
)  : Serializable
