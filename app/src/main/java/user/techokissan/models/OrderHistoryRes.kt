package user.techokissan.models

import java.io.Serializable

data class OrderHistoryRes(

    var result : Boolean,
    var message : String,
    var data : ArrayList<OrderHistoryDataRes>,



): Serializable

        data class OrderHistoryDataRes(
            var id : String,
            var cityID : String,
            var userID : String,
            var vendorID : String,
            var customer_name : String,
            var contact_no : String,
            var flat_no : String,
            var building_name : String,
            var landmark : String,
            var location : String,
            var latitude : String,
            var longitude : String,
            var address_type : String,
            var agentID : String,
            var coupon_code : String,
            var delivery_charges : String,
            var coupon_discount : String,
            var order_amount : String,
            var total_amount : String,
            var payment_method : String,
            var instruction : String,
            var delivery_slot : String,
            var delivery_date : String,
            var status : String,
            var is_deleted : String,
            var created_at : String,
            var updated_at : String,
        ): Serializable
