package user.techokissan.models

data class ModelReview(
    val `data`: List<ReviewData>,
    val message: String,
    val result: Boolean
)

data class ReviewData(
    val created_at: String,
    val id: String,
    val is_deleted: String,
    val product_id: String,
    val product_image: String,
    val rate: String,
    val review: String,
    val status: String,
    val updated_at: String,
    val user_id: String,
    val user_name: String
)