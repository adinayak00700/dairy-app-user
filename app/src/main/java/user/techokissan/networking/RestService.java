package user.techokissan.networking;




import com.example.datingapp.utilities.Constants;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import user.techokissan.models.AddProductModelRes;
import user.techokissan.models.CartListRes;
import user.techokissan.models.CityListRes;
import user.techokissan.models.LocalityListRes;
import user.techokissan.models.ModelBanner;
import user.techokissan.models.ModelCategory;
import user.techokissan.models.ModelDays;
import user.techokissan.models.ModelMySubscription;
import user.techokissan.models.ModelProducts;
import user.techokissan.models.ModelReview;
import user.techokissan.models.ModelSubcategory;
import user.techokissan.models.ModelSuccess;
import user.techokissan.models.ModelTransaction;
import user.techokissan.models.ModelUser;
import user.techokissan.models.ModelVendor;
import user.techokissan.models.OrderHistoryRes;
import user.techokissan.models.OrderItemsList;
import user.techokissan.models.OrderPlacesbean;
import user.techokissan.models.ProductDetailsRes;
import user.techokissan.models.StateListRes;
import user.techokissan.models.UserAddressListRes;

public interface RestService {

    @POST(Constants.SIGNUPAPI)
    Call<ModelUser> signup(@Body HashMap hashMap);

    @POST("login")
    Call<ModelUser> login(@Body HashMap hashMap);


    @POST(Constants.BANNER_API)
    Call<ModelBanner> getBanners(@Body HashMap hashMap);

    @POST(Constants.CATEGORY_API)
    Call<ModelCategory> getCategories(@Body HashMap hashMap);
    @POST(Constants.GET_PROFILE)
    Call<ModelUser> getProfile(@Body HashMap hashMap);
    @POST(Constants.ADD_WALLET)
    Call<ModelSuccess> addtowallet(@Body HashMap hashMap);

    @POST("forgot_password")
    Call<ModelSuccess> forgotPassword(@Body HashMap hashMap);

    @POST("change_password")
    Call<ModelSuccess> changePaassword(@Body HashMap hashMap);
    @POST("update_location")
    Call<ModelSuccess> updateCoordinates(@Body HashMap hashMap);



    @POST(Constants.WALLET_TRANSACTION)
    Call<ModelTransaction> walletTransaction(@Body HashMap hashMap);





    @POST(Constants.SUBCATEGORY_API)
    Call<ModelSubcategory> subcategory(@Body HashMap hashMap);
    @POST(Constants.PRODUCTS_LIST_API)
    Call<ModelProducts> products(@Body HashMap hashMap);

    @POST(Constants.VENDOR_LIST)
    Call<ModelVendor> vendor_list(@Body HashMap hashMap);



    @POST(Constants.HOME_PRODUCTS)
    Call<ModelProducts> home_products(@Body HashMap hashMap);

    @POST(Constants.PRODUCTS_DETAILS)
    Call<ProductDetailsRes> PRODUCTS_DETAILS(@Body HashMap hashMap);

    @POST(Constants.ADD_PRODUCT)
    Call<AddProductModelRes> ADD_PRODUCT(@Body HashMap hashMap);

    @POST(Constants.USER_CART)
    Call<CartListRes> USER_CART(@Body HashMap hashMap);

    @POST(Constants.ADD_ADDRESS)
    Call<AddProductModelRes> ADD_ADDRESS(@Body HashMap hashMap);


    @POST(Constants.LIST_ADDRESS)
    Call<UserAddressListRes> LIST_ADDRESS(@Body HashMap hashMap);

    @POST(Constants.STATE_LIST)
    Call<StateListRes> stateList(@Body HashMap hashMap);


    @POST(Constants.CITY_LIST)
    Call<CityListRes> CITY_LIST(@Body HashMap hashMap);

    @POST(Constants.UPDATE_LOCATION)
    Call<ModelSuccess> updateLocation(@Body HashMap hashMap);
    @POST(Constants.MY_SUBSCRIPTONS)
    Call<ModelMySubscription> getMySubscriptions(@Body HashMap hashMap);



    @POST(Constants.LOCALITY)
    Call<LocalityListRes> LOCALITY(@Body HashMap hashMap);

    @POST(Constants.ORDER_HISTORY)
    Call<OrderHistoryRes> ORDER_HISTORY(@Body HashMap hashMap);

    @POST(Constants.DELETE_ADDRESS)
    Call<AddProductModelRes> DELETE_ADDRESS(@Body HashMap hashMap);




    @POST("user_subscription_package")
    Call<ModelDays> getAllDays(@Body HashMap hashMap);


    @POST(Constants.PLACE_ORDER)
    Call<OrderPlacesbean> PLACE_ORDER(@Body HashMap hashMap);

    @POST(Constants.PLACE_ORDER_PACKAGE)
    Call<ModelSuccess> package_place_order(@Body HashMap hashMap);


    @POST("cancel_bill")
    Call<ModelSuccess> noAVailabilityApi(@Body HashMap hashMap);





    @POST(Constants.bill_payment)
    Call<ModelSuccess> bill_payment(@Body HashMap hashMap);



    @POST(Constants.ADD_RATING)
    Call<ModelSuccess> ADD_RATING(@Body HashMap hashMap);

    @POST(Constants.GET_REVIEWS)
    Call<ModelReview> GET_REVIEWS(@Body HashMap hashMap);


    @POST(Constants.GET_VENDOR_REVIEWS)
    Call<ModelReview> GET_VENDOR_REVIEWS(@Body HashMap hashMap);


    @POST(Constants.ORDER_ITEMS)
    Call<OrderItemsList> ORDER_ITEMS(@Body HashMap hashMap);


    @POST(Constants.ORDER_CANCEL)
    Call<ModelSuccess> ORDER_CANCEL(@Body HashMap hashMap);




    @POST(Constants.GET_BILLS)
    Call<ModelMySubscription> GET_BILLS(@Body HashMap hashMap);


    @Multipart
    @POST(Constants.UPDATE_PROFILE_API)
    Call<ModelSuccess> updateProfileImage(
            @Part MultipartBody.Part profile_image,
            @Part("token") RequestBody jwtToken);


    @Multipart
    @POST(Constants.UPDATE_PROFILE_API)
    Call<ModelSuccess> editProfileApi(
            @Part("token") RequestBody jwtToken,
            @Part("name") RequestBody name,
            @Part("email") RequestBody email
         );


/*    @Multipart
    @POST("profile_update")
    Call<ModelSuccess> updateProfileImage(
            @Part MultipartBody.Part profile_image,
            @Part("jwtToken") RequestBody jwtToken);


    @Multipart
    @POST("profile_update")
    Call<ModelSuccess> editProfileApi(
            @Part MultipartBody.Part profile_image,
            @Part("jwtToken") RequestBody jwtToken,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
         *//*   @Part("email") RequestBody email,*//*
            @Part("dob") RequestBody dob,
            @Part("gender") RequestBody gender);

    @Multipart
    @POST("profile_update")
    Call<ModelSuccess> editProfileApi(
            @Part("jwtToken") RequestBody jwtToken,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
       *//*     @Part("email") RequestBody email,*//*
            @Part("dob") RequestBody dob,
            @Part("gender") RequestBody gender);
    @Multipart
    @POST("profile_update")
    Call<ModelSuccess> editProfileApi(
            @Part("jwtToken") RequestBody jwtToken,
            @Part("number") RequestBody first_name);*/



}


