package user.techokissan.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import user.techokissan.models.AddProductModelRes;
import user.techokissan.models.CartListRes;
import user.techokissan.models.CityListRes;
import user.techokissan.models.LocalityListRes;
import user.techokissan.models.ModelBanner;
import user.techokissan.models.ModelCategory;
import user.techokissan.models.ModelDays;
import user.techokissan.models.ModelMySubscription;
import user.techokissan.models.ModelProducts;
import user.techokissan.models.ModelReview;
import user.techokissan.models.ModelSubcategory;
import user.techokissan.models.ModelSuccess;
import user.techokissan.models.ModelTransaction;
import user.techokissan.models.ModelUser;
import user.techokissan.models.ModelVendor;
import user.techokissan.models.OrderHistoryRes;
import user.techokissan.models.OrderItemsList;
import user.techokissan.models.OrderPlacesbean;
import user.techokissan.models.ProductDetailsRes;
import user.techokissan.models.StateListRes;
import user.techokissan.models.UserAddressListRes;
import user.techokissan.utilities.UrlFactory;


public class RestClient {


    private static RestClient mInstance = new RestClient();
    OkHttpClient client;
    Retrofit retrofit;
    Retrofit retrofitTest;
    private RestService mRestService;
    private RestService mRestServiceTest;
    private RestClient() {
    }

    public static RestClient getInst() {
        return mInstance;
    }

    /**
     * Here is the setup call for http service for this app. setup is done once in MyApplication class.
     * Step 1 : Logging is added using HttpLoggingInterceptor. Logging is removed from retrofit2 so
     * it has to be added as a part of OkHttp interceptor.
     * Step 2 : Build an OkHttpClient with logging interceptor.
     * Step 3 : Retrofit is build with baseUrl, okhttp client, Gson Converter factory for easy JSON to POJO conversion.
     */
    public void setup() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES);
        // Should be used only in Debug Mode.
        if (true) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(true ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE); //// TODO: 21-07-2016
            builder.addInterceptor(interceptor);
        }
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        client = builder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(UrlFactory.getBaseUrl())
                .client(client)
                //.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mRestService = retrofit.create(RestService.class);



        retrofitTest = new Retrofit.Builder()
                .baseUrl(UrlFactory.URL_TEST)
                .client(client)
                //.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mRestServiceTest = retrofitTest.create(RestService.class);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

   public Call<ModelUser> userSignUp(HashMap hashMap) {
        return mRestService.signup(hashMap);
    }


    public Call<ModelBanner> getBanners(HashMap hashMap) {
        return mRestService.getBanners(hashMap);
    }

    public Call<ModelCategory> getCategories(HashMap hashMap) {
        return mRestService.getCategories(hashMap);
    }

    public Call<ModelUser> userLogin(HashMap hashMap) {
        return mRestService.login(hashMap);
    }

    public Call<ModelUser> getProfile(HashMap hashMap) {
        return mRestService.getProfile(hashMap);
    }

    public Call<ModelSuccess> addtowallet(HashMap hashMap) {
        return mRestService.addtowallet(hashMap);
    }

    public Call<ModelSuccess> forgotPassword(HashMap hashMap) {
        return mRestService.forgotPassword(hashMap);
    }
    public Call<ModelSuccess> changePaassword(HashMap hashMap) {
        return mRestService.changePaassword(hashMap);
    }

    public Call<ModelSuccess> updateCoordinates(HashMap hashMap) {
        return mRestService.updateCoordinates(hashMap);
    }

    public Call<ModelTransaction> walletTransaction(HashMap hashMap) {
        return mRestService.walletTransaction(hashMap);
    }


    public Call<ModelSubcategory> subcategory(HashMap hashMap) {
        return mRestService.subcategory(hashMap);
    }


    public Call<ModelProducts> products(HashMap hashMap) {
        return mRestService.products(hashMap);
    }
    public Call<ModelVendor> vendor_list(HashMap hashMap) {
        return mRestService.vendor_list(hashMap);
    }
    public Call<ModelProducts> home_products(HashMap hashMap) {
        return mRestService.home_products(hashMap);
    }

    public Call<ProductDetailsRes> PRODUCTS_DETAILS(HashMap hashMap) {
        return mRestService.PRODUCTS_DETAILS(hashMap);
    }
    public Call<CartListRes> USER_CART(HashMap hashMap) {
        return mRestService.USER_CART(hashMap);
    }
    public Call<AddProductModelRes> ADD_PRODUCT(HashMap hashMap) {
        return mRestService.ADD_PRODUCT(hashMap);
    }

    public Call<AddProductModelRes> ADD_ADDRESS(HashMap hashMap) {
        return mRestService.ADD_ADDRESS(hashMap);
    }

    public Call<UserAddressListRes> LIST_ADDRESS(HashMap hashMap) {
        return mRestService.LIST_ADDRESS(hashMap);
    }

    public Call<StateListRes> stateList(HashMap hashMap) {
        return mRestService.stateList(hashMap);
    }

    public Call<CityListRes> CITY_LIST(HashMap hashMap) {
        return mRestService.CITY_LIST(hashMap);
    }

    public Call<ModelSuccess> updateLocation(HashMap hashMap) {
        return mRestService.updateLocation(hashMap);
    }
    public Call<ModelMySubscription> getMySubscriptions(HashMap hashMap) {
        return mRestService.getMySubscriptions(hashMap);
    }


    public Call<LocalityListRes> LOCALITY(HashMap hashMap) {
        return mRestService.LOCALITY(hashMap);
    }
    public Call<OrderHistoryRes> ORDER_HISTORY(HashMap hashMap) {
        return mRestService.ORDER_HISTORY(hashMap);
    }
    public Call<AddProductModelRes> DELETE_ADDRESS(HashMap hashMap) {
        return mRestService.DELETE_ADDRESS(hashMap);
    }

    public Call<ModelDays> getAllDays(HashMap hashMap) {
        return mRestService.getAllDays(hashMap);
    }



    public Call<OrderPlacesbean> PLACE_ORDER(HashMap hashMap) {
        return mRestService.PLACE_ORDER(hashMap);
    }

    public Call<ModelSuccess> PLACE_ORDER_Package(HashMap hashMap) {
        return mRestService.package_place_order(hashMap);
    }

    public Call<ModelSuccess> noAVailabilityApi(HashMap hashMap) {
        return mRestService.noAVailabilityApi(hashMap);
    }

    public Call<ModelSuccess> billpayment(HashMap hashMap) {
        return mRestService.bill_payment(hashMap);
    }

    public Call<OrderItemsList> ORDER_ITEMS(HashMap hashMap) {
        return mRestService.ORDER_ITEMS(hashMap);
    }
    public Call<ModelSuccess> ORDER_CANCEL(HashMap hashMap) {
        return mRestService.ORDER_CANCEL(hashMap);
    }


    public Call<ModelSuccess> ADD_RATING(HashMap hashMap) {
        return mRestService.ADD_RATING(hashMap);
    }
    public Call<ModelReview> GET_REVIEWS(HashMap hashMap) {
        return mRestService.GET_REVIEWS(hashMap);
    }

    public Call<ModelReview> GET_VENDOR_REVIEWS(HashMap hashMap) {
        return mRestService.GET_VENDOR_REVIEWS(hashMap);
    }
    public Call<ModelMySubscription> getBills(HashMap hashMap) {
        return mRestService.GET_BILLS(hashMap);
    }

/*
   public void hitEditProfileApi(
            String jwtToken,
            String phone,
            String first_name,
            String last_name,
            String email,
            String dob,
            String gender,

            File file_image, Boolean isImageSelected,
            Callback<ModelSuccess> callback) {
        RequestBody phone1 = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
        RequestBody first_name1 = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody last_name1 = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody email1 = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody dob1 = RequestBody.create(MediaType.parse("text/plain"), dob);
        RequestBody gender1 = RequestBody.create(MediaType.parse("text/plain"), gender);
        if (isImageSelected) {
            RequestBody thumbnailBody = RequestBody.create(MediaType.parse("image/*"), file_image);
            MultipartBody.Part profile_image1 = MultipartBody.Part.createFormData("image", file_image.getName(), thumbnailBody); //image[] for multiple image
            Call<ModelSuccess> call = mRestService.editProfileApi(profile_image1,jwtToken1,first_name1,email1);
            call.enqueue(callback);
        } else {
            Call<ModelSuccess> call = mRestService.editProfileApi(jwtToken1,first_name1,email1);
            call.enqueue(callback);
        }

    }


    public void hitEditNumberProfileApi(
            String jwtToken,
            String phone,
            Callback<ModelSuccess> callback) {
        RequestBody phone1 = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
            Call<ModelSuccess> call = mRestService.editProfileApi(jwtToken1,phone1);
            call.enqueue(callback);
        }

*/



    public void etProfileApi(
            String jwtToken,
            String name,
            String email,
            Callback<ModelSuccess> callback) {
        RequestBody name1 = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody email1 = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);
        Call<ModelSuccess> call = mRestService.editProfileApi(jwtToken1,name1,email1);
        call.enqueue(callback);
    }
    public void hitEditProfileApiOnlyPhoto(
            String jwtToken,
            File file_image,
            Callback<ModelSuccess> callback) {

        RequestBody jwtToken1 = RequestBody.create(MediaType.parse("text/plain"), jwtToken);

        RequestBody thumbnailBody = RequestBody.create(MediaType.parse("image/*"), file_image);
        MultipartBody.Part profile_image1 = MultipartBody.Part.createFormData("image", file_image.getName(), thumbnailBody); //image[] for multiple image
        Call<ModelSuccess> call = mRestService.updateProfileImage(profile_image1,jwtToken1);
        call.enqueue(callback);

    }
}
