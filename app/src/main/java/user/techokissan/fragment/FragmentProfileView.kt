package user.techokissan.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentProfileViewBinding
import user.techokissan.models.ModelUser
import user.techokissan.models.UserDetails
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentProfileView.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentProfileView : BaseFragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var userDetails:UserDetails
private lateinit var binding:FragmentProfileViewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding=DataBindingUtil.bind<FragmentProfileViewBinding>(inflater.inflate(R.layout.fragment_profile_view, container, false))!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateUI()
        getProfile()
        binding.tvEditEmail.setOnClickListener {
            findNavController().navigate(R.id.idFragmentProfileViewToFrafmentProfileEdit)
        }

        binding.layoutLogout.setOnClickListener {
            PrefManager.getInstance(requireContext())!!.clearPrefs()
            PrefManager.getInstance(requireContext())!!.keyIsLoggedIn=false

            startActivity(IntentHelper.getLoginScreen(requireContext()))
            requireActivity().finishAffinity()
        }
        binding.layoutSubscription.setOnClickListener {

            startActivity(IntentHelper.getMySubscriptionScreen(requireContext()))
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentProfileView().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =PrefManager.getInstance(requireContext())!!.userDetail.token

        RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {

                if (response.body()!!.result) {
                    PrefManager.getInstance(requireContext())!!.userDetail=response.body()!!.data
                    updateUI()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }


  fun updateUI(){
      userDetails=PrefManager.getInstance(requireContext())!!.userDetail
        binding.tvname.text=userDetails.name
      binding.tvEmail.text=userDetails.email
      binding.tvNumber.text=userDetails.mobile
      Glide.with(requireActivity())
          .load(PrefManager.getInstance(requireContext())!!.userDetail.image)
          .into(binding.ivUserImage)



    }

}