package user.techokissan.fragment

import ReviewAdapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants.USER_TOKEN
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentReviewsBinding
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val CATEGORY_ID = "catId"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentReviews.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentReviews() : BaseFragment() {
    private  var search=""
    private var categoryId: String? = ""
    private var type: String? = ""
    private var param2: String? = ""
    private lateinit var binding: FragmentReviewsBinding
    private var isViewCartButtonShowing=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            categoryId = it.getString(CATEGORY_ID)
            type=it.getString("type")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind<FragmentReviewsBinding>(
            inflater.inflate(
                R.layout.fragment_reviews,
                container,
                false
            )
        )!!
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentReviews.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(categoryId: String,type:String) =
            FragmentReviews().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)
                    putString("type", type)
                }
            }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (type=="product"){
            getAllProductReviews(categoryId!!)
        }else{
            getAllVendorReviews(categoryId!!)
        }


     

    }

    private fun getAllProductReviews(productId: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["product_id"] = productId

        RestClient.getInst().GET_REVIEWS(hashMap).enqueue(object : Callback<ModelReview?> {
            override fun onResponse(
                call: Call<ModelReview?>?,
                response: Response<ModelReview?>
            ) {
                if (response.body()!!.result) {
                    binding.rvProducts.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    var adapter = ReviewAdapter(requireContext(),object :ReviewAdapter.Callbackk{
                        override fun onCartValueChanged() {
                         
                        }

                    })
                    adapter.addReviews(response.body()!!.data)
                    binding.rvProducts.adapter = adapter


                    if (response.body()!!.data.size==0){
                        binding.layoutNoData.visibility=View.VISIBLE
                    }else{
                        binding.layoutNoData.visibility=View.GONE
                    }
                } else {
                }
            }
            override fun onFailure(call: Call<ModelReview?>?, t: Throwable) {
            }
        })
    }


    private fun getAllVendorReviews(vendorId: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["vendor_id"] = vendorId

        RestClient.getInst().GET_VENDOR_REVIEWS(hashMap).enqueue(object : Callback<ModelReview?> {
            override fun onResponse(
                call: Call<ModelReview?>?,
                response: Response<ModelReview?>
            ) {
                if (response.body()!!.result) {
                    binding.rvProducts.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    var adapter = ReviewAdapter(requireContext(),object :ReviewAdapter.Callbackk{
                        override fun onCartValueChanged() {

                        }

                    })
                    adapter.addReviews(response.body()!!.data)
                    binding.rvProducts.adapter = adapter


                    if (response.body()!!.data.size==0){
                        binding.layoutNoData.visibility=View.VISIBLE
                    }else{
                        binding.layoutNoData.visibility=View.GONE
                    }
                } else {
                }
            }
            override fun onFailure(call: Call<ModelReview?>?, t: Throwable) {
            }
        })
    }



}