package user.techokissan.fragment


import CategoryAdapter
import HomeProductAdapter
import SliderAdapterExample
import android.R.attr.minHeight
import android.content.Intent
import android.os.Bundle
import android.support.annotation.NonNull
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.databinding.FragmentFlagmentHomeBinding
import user.techokissan.models.Category
import user.techokissan.models.ModelBanner
import user.techokissan.models.ModelCategory
import user.techokissan.models.ModelProducts
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import kotlin.collections.set


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FlagmentHome : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentFlagmentHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_flagment_home, container, false)
        binding = DataBindingUtil.bind<FragmentFlagmentHomeBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBanner()
        getCategories()
        getProducts()


    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FlagmentHome().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


    fun getCategories() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token

        binding.rvCategory.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)








        RestClient.getInst().getCategories(hashMap).enqueue(object : Callback<ModelCategory> {
            override fun onResponse(call: Call<ModelCategory>, response: Response<ModelCategory>) {
             if(response.body()!!.result){
                 var catAdapter = CategoryAdapter(requireContext(),
                     response.body()!!.data as ArrayList<Category>,object :CategoryAdapter.Callbackk{


                         override fun onClickOnCategory(cat: Category) {




                                changeTheme(cat.id)






                             val bundle = Intent()
                             bundle.putExtra("catId", cat.id)
                             if (cat.is_vendor.equals("Y")){
                                 startActivity(IntentHelper.getVendorListScreen(requireContext()).putExtras(bundle))
                             }else{
                                 startActivity(IntentHelper.getProductsLIstScreen(requireContext()).putExtras(bundle))
                             }

                         }
                     })

                 binding.rvCategory.adapter = catAdapter
             }


            }

            override fun onFailure(call: Call<ModelCategory>, t: Throwable) {
                //   makeToast(t.message)
            }
        })
    }


    fun getProducts() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        binding.rvProducts.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

   
        RestClient.getInst().home_products(hashMap).enqueue(object : Callback<ModelProducts?> {
            override fun onResponse(
                call: Call<ModelProducts?>?,
                response: Response<ModelProducts?>
            ) {
                if (response.body()!!.result) {
                    var productAdapter = HomeProductAdapter(requireContext())
                    productAdapter.addProducts(response.body()!!.data)
                    binding.rvProducts.adapter = productAdapter

                } else {
                }
            }

            override fun onFailure(call: Call<ModelProducts?>?, t: Throwable) {

            }
        })
    }
    var appTheme = 0
    var themeColor = 0
    var appColor = 0
    fun changeTheme(catId:String){

    }


    fun getBanner() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token


        val urls: MutableList<String> = ArrayList()



      var slider=  SliderAdapterExample(requireContext())



        RestClient.getInst().getBanners(hashMap).enqueue(object : Callback<ModelBanner> {
            override fun onResponse(call: Call<ModelBanner>, response: Response<ModelBanner>) {




                if (response.body()!!.result){
                    for (i in response.body()!!.data.indices){
                        urls.add(response.body()!!.data[i].banner_img)
                    }
                    slider.addItem(urls as ArrayList<String>)
                    binding.imageSlider.setSliderAdapter(slider);
                    binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
                    binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                    binding.imageSlider.startAutoCycle();


                }

            }

            override fun onFailure(call: Call<ModelBanner>, t: Throwable) {
             //   makeToast(t.message)
            }
        })
    }




}