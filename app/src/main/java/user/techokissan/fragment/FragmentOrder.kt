package user.techokissan.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.ActivityOrderScreenBinding
import user.techokissan.databinding.FragmentOrderBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentOrder.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentOrder : BaseFragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding : FragmentOrderBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentOrderBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val personalInfoFragment = OngoingOrderScreenFragment()
        replaceFragment(personalInfoFragment)
        binding.mPersonalLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_order_bg_unselected )
        binding.mBusinessProfileLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_list_order_bg_selected )
        binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.white ))
        binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.black ))

        binding.mPersonalLL.setOnClickListener(this)
        binding.mBusinessProfileLL.setOnClickListener(this)
        binding.mCancelledLL.setOnClickListener(this)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentOrder.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentOrder().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.mPersonalLL ->{
                binding.mPersonalLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_list_order_bg_selected )
                binding.mBusinessProfileLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_order_bg_unselected )
                binding.mCancelledLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_order_bg_unselected )
                binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.black ))
                binding.mCancelledTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.black ))
                binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.white ))
                val personalInfoFragment = DeliveryOrderScreenFragment()
                replaceFragment(personalInfoFragment)
            }
            R.id.mBusinessProfileLL ->{
                binding.mPersonalLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_order_bg_unselected )
                binding.mCancelledLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_order_bg_unselected )
                binding.mBusinessProfileLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_list_order_bg_selected )
                binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.white ))
                binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.black))
                binding.mCancelledTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.black ))
                val businessProfile = OngoingOrderScreenFragment()
                replaceFragment(businessProfile)
            }
            R.id.mCancelledLL->{
                binding.mBusinessProfileLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_order_bg_unselected )
                binding.mPersonalLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_order_bg_unselected )
                binding.mCancelledLL.background = ContextCompat.getDrawable(requireContext() ,R.drawable.item_list_order_bg_selected )
                binding.mCancelledTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.white))
                binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.black))
                binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(requireContext() ,R.color.black))
                val personalInfoFragment = CancelledOrderScreenFragment()
                replaceFragment(personalInfoFragment)

            }

        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.mContainerViewContainer, fragment)
        transaction.commit()
    }


}