package user.techokissan.fragment

import ProductAdapter
import SubCategoryAdapter
import VendorAdapter
import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.datingapp.utilities.Constants.USER_TOKEN
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentProductListBinding
import user.techokissan.databinding.FragmentVendorListBinding
import user.techokissan.demo.CartAdapter
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val CATEGORY_ID = "catId"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentVendorList.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentVendorList : BaseFragment() {
    private  var search=""
    private var categoryId: String? = ""
    private var param2: String? = ""
    private lateinit var binding: FragmentVendorListBinding
    private var isViewCartButtonShowing=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            categoryId = it.getString(CATEGORY_ID)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind<FragmentVendorListBinding>(
            inflater.inflate(
                R.layout.fragment_vendor_list,
                container,
                false
            )
        )!!
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentVendorList.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(categoryId: String) =
            FragmentVendorList().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)

                }
            }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

      getAllVendor("")



    }

    private fun getAllVendor(subCategoryId: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
      hashMap["category_id"] = categoryId!!


        RestClient.getInst().vendor_list(hashMap).enqueue(object : Callback<ModelVendor?> {
            override fun onResponse(
                call: Call<ModelVendor?>?,
                response: Response<ModelVendor?>
            ) {
                if (response.body()!!.result) {
                    binding.rvvendors.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    var adapter = VendorAdapter(requireContext(),object :VendorAdapter.Callbackk{
                        override fun onClickOnVendor(vendor: Vendor) {

                            val bundle = Bundle()
                            bundle.putString("catId", categoryId)
                            bundle.putString("vendorId", vendor.id)
                            startActivity(IntentHelper.getProductsLIstScreen(requireContext()).putExtras(bundle))



                        }


                    })
                    adapter.addProducts(response.body()!!.data)
                    binding.rvvendors.adapter = adapter


                    if (response.body()!!.data.size==0){
                        binding.layoutNoData.visibility=View.VISIBLE
                    }else{
                        binding.layoutNoData.visibility=View.GONE
                    }
                } else {
                }
            }
            override fun onFailure(call: Call<ModelVendor?>?, t: Throwable) {
            }
        })
    }





    fun searchData(dataa:String){
        this.search=dataa
        getAllVendor("")
    }




}