package user.techokissan.fragment

import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentSignUpBinding
import user.techokissan.models.ModelUser
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import user.techokissan.utilities.ValidationHelper
import java.util.HashMap

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentSignUp : BaseFragment() {
    private var param1: String? = null
    private var param2: String? = null
private  lateinit var  binding:FragmentSignUpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.bind<FragmentSignUpBinding>(inflater.inflate(R.layout.fragment_sign_up, container, false))!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonSignUp.setOnClickListener {
           if (isValid()){
               signUpApi()
           }
        }
    }

    fun isValid(): Boolean {
        if(ValidationHelper.isNull(binding.etName.text.toString())){
            makeToast("Please Enter name")
            return false
        }else
        if(ValidationHelper.isNull(binding.etEmail.text.toString())){
            makeToast("Please Enter email")
            return false
        }else if(ValidationHelper.isNull(binding.etPhone.text.toString())){
                makeToast("Please Enter Phone")
                return false
            }
            else if(ValidationHelper.isNull(binding.etPassword.text.toString())){
                makeToast("Please Enter Password")
                return false
            }
        else if(!binding.radioButton.isChecked){
            makeToast("Please Agree to TnC")
            return false
        }
        return true;
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentSignUp().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun signUpApi() {
        val hashMap = HashMap<String, String>()
        hashMap["email"] =binding.etEmail.text.toString()
        hashMap["mobile"] =binding.etPhone.text.toString()
        hashMap["password"] =binding.etPassword.text.toString()
        hashMap["name"] =binding.etName.text.toString()
        hashMap["social_id"] = ""
        hashMap["device_id"] = ""
        hashMap["device_token"] = ""
        hashMap["device_type"] = ""
        RestClient.getInst().userSignUp(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                if (response.body()!!.result) {
                    PrefManager.getInstance(requireContext())!!.userDetail=response.body()!!.data
                    PrefManager.getInstance(requireContext())!!.keyIsLoggedIn = true
                    startActivity(IntentHelper.getDashboardActivity(context))
                    activity!!.finish()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

}