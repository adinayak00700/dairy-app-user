package user.techokissan.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentLoginBinding
import user.techokissan.models.ModelUser
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import user.techokissan.utilities.ValidationHelper
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentLogin.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentLogin : BaseFragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
private lateinit var  binding:FragmentLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view=inflater.inflate(R.layout.fragment_login, container, false)
        binding=DataBindingUtil.bind<FragmentLoginBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonSignIn.setOnClickListener {
           if (isValid()){
               loginApi()
           }
        }

        binding.tvForgotPasswortd.setOnClickListener {
            startActivity(IntentHelper.getForgotPassword(context!!))
        }

    }

    companion object {
        @JvmStatic
        fun newInstance() =
            FragmentLogin().apply {
                arguments = Bundle().apply {

                }
            }
    }


    fun isValid(): Boolean {
        if(ValidationHelper.isNull(binding.etEmail.text.toString())){
            makeToast("Please Enter email")
            return false
        }else
            if(ValidationHelper.isNull(binding.etPassword.text.toString())){
                makeToast("Please Enter Password")
                return false
            }
            return true;
    }


    fun loginApi() {

        val hashMap = HashMap<String, String>()
        hashMap["username"] =binding.etEmail.text.toString()
        hashMap["password"] =binding.etPassword.text.toString()
        hashMap["social_id"] = ""
        hashMap["device_id"] = ""
        hashMap["device_token"] = ""
        hashMap["device_type"] = ""
        RestClient.getInst().userLogin(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {

                if (response.body()!!.result) {
                    PrefManager.getInstance(requireContext())!!.userDetail=response.body()!!.data
                    PrefManager.getInstance(requireContext())!!.keyIsLoggedIn = true
                    startActivity(IntentHelper.getDashboardActivity(context))
                    activity!!.finish()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }
}