package user.techokissan.fragment

import ProductAdapter
import SubCategoryAdapter
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.datingapp.utilities.Constants.USER_TOKEN
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.adapter.BillAdapter
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentBillsBinding
import user.techokissan.demo.CartAdapter
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val CATEGORY_ID = "catId"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentBills.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentBills : BaseFragment(), BillAdapter.Callbackk {
    private  var search=""
    private var categoryId: String? = ""
    private var param2: String? = ""
    private lateinit var binding: FragmentBillsBinding
    private var isViewCartButtonShowing=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind<FragmentBillsBinding>(
            inflater.inflate(
                R.layout.fragment_bills,
                container,
                false
            )
        )!!
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentBills.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(categoryId: String) =
            FragmentBills().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)

                }
            }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    getAllBills()


    }

    private fun getAllBills() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token


        RestClient.getInst().getBills(hashMap).enqueue(object : Callback<ModelMySubscription?> {
            override fun onResponse(
                call: Call<ModelMySubscription?>?,
                response: Response<ModelMySubscription?>
            ) {
                if (response.body()!!.result) {
                    binding.rvBills.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    var adapter = BillAdapter(requireContext(),this@FragmentBills)
                  adapter.addPackages(response.body()!!.data)
                    binding.rvBills.adapter = adapter

Log.d("asdadasd",response.body()!!.data.size.toString())
                /*    if (response.body()!!.data.size==0){
                        binding..visibility=View.VISIBLE
                    }else{
                        binding.rvBills.visibility=View.GONE
                    }*/
                } else {
                }
            }
            override fun onFailure(call: Call<ModelMySubscription?>?, t: Throwable) {
            }
        })
    }

    override fun onClickONBill(bill: SubscriptionData) {
        startActivity(IntentHelper.getPaymentScreen(context!!).putExtra("total_amt",bill.billAmount).putExtra("type","bill").putExtra("mPackageID",bill.packageDetail.id).putExtra("orderId",bill.order_id))
    }


}