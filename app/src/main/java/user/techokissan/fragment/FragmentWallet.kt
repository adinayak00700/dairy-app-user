package user.techokissan.fragment

import TransactionAdapter
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentWalletBinding
import user.techokissan.models.ModelSuccess
import user.techokissan.models.ModelTransaction
import user.techokissan.models.ModelUser
import user.techokissan.networking.RestClient
import user.techokissan.utilities.ValidationHelper
import java.util.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentWallet : BaseFragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentWalletBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind<FragmentWalletBinding>(
            inflater.inflate(
                R.layout.fragment_wallet,
                container,
                false
            )
        )!!
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateUI()
        binding.buttonAddMoney.setOnClickListener {
           if (ValidationHelper.isNull(binding.etAmount.text.toString())){
               makeToast("please Enter amount")
           }else{
               startPayment(binding.etAmount.text.toString().toDouble())
           }
        }

        binding.tvSeemore.setOnClickListener {
            findNavController().navigate(R.id.idFragmentWalletToidFragmentTransactions)
        }
    }



    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentWallet().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun startPayment(amount: Double) {
        val amount = Math.round(amount.toFloat() * 100)


        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        val activity: Activity = requireActivity()

        val co = Checkout()

        // set your id as below
        co.setKeyID("rzp_test_nbeFkzxFdW7hTn");

        // set image
        co.setImage(R.drawable.add);
        try {
            val options = JSONObject()
            options.put("name", "Razorpay Corp")
            options.put("description", "Demoing Charges")
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", amount)
            val preFill = JSONObject()
            preFill.put("email", PrefManager.getInstance(requireActivity())!!.userDetail.email)
            preFill.put("contact", PrefManager.getInstance(requireActivity())!!.userDetail.name)
            options.put("prefill", preFill)
            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_SHORT)
                .show()
            e.printStackTrace()
        }
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */


    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token

        RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {

                if (response.body()!!.result) {
                    PrefManager.getInstance(requireContext())!!.userDetail = response.body()!!.data
                    updateUI()

                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

    fun updateUI() {
        binding.tvAmount.text =
            "Your balance INR " + PrefManager.getInstance(requireContext())!!.userDetail.wallet.toString()

        Glide.with(context!!).load(PrefManager.getInstance(context!!)!!.userDetail.image).circleCrop().into(binding.imageView2)
        getWalletTransactions()
    }


    fun addToWallet(trasactionId: String) {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["transactionID"] = trasactionId
        hashMap["amount"] = binding.etAmount.text.toString()
        RestClient.getInst().addtowallet(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {

                if (response.body()!!.result) {

                    getProfile()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }


    fun getWalletTransactions() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        RestClient.getInst().walletTransaction(hashMap).enqueue(object : Callback<ModelTransaction> {
            override fun onResponse(call: Call<ModelTransaction>, response: Response<ModelTransaction>) {

                if (response.body()!!.result) {

                    var transactionAdapter = TransactionAdapter(context!!)
                    transactionAdapter.addTransactions(response.body()!!.data)
                    binding.rvTransactions.adapter = transactionAdapter
                    binding.rvTransactions.layoutManager =
                        LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelTransaction>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }



}