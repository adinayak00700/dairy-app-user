package user.techokissan.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.canhub.cropper.CropImage
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment

import user.techokissan.databinding.FragmentProfileEditBinding
import user.techokissan.models.ModelSuccess
import user.techokissan.models.ModelUser
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import java.io.File
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentProfileEdit : BaseFragment() {
    private lateinit var binding: FragmentProfileEditBinding
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_profile_edit, container, false)
        binding = DataBindingUtil.bind<FragmentProfileEditBinding>(view)!!
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getProfile()
        binding.ivUserImage.setOnClickListener {
            openCamera()
        }

        binding.buttonSaveChanges.setOnClickListener {
            editProfileApi()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentProfileEdit().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun editProfileApi() {

        RestClient.getInst().etProfileApi(
            PrefManager.getInstance(getContext()!!)!!.userDetail.token,binding.etName.text.toString(),binding.etEmail.text.toString(),
            object : Callback<ModelSuccess> {
                override fun onResponse(
                    call: Call<ModelSuccess>,
                    response: Response<ModelSuccess>
                ) {
                    if (response.body()!!.result) {
                        getProfile()
                    }

                }

                override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                }
            })
    }

    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap["token"] = PrefManager.getInstance(requireContext())!!.userDetail.token

        RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {

                if (response.body()!!.result) {
                    PrefManager.getInstance(requireContext())!!.userDetail = response.body()!!.data
                    updateProfile()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }


    private fun openCamera() {
        val intent: Intent = CropImage.activity()
            .setAspectRatio(1, 1)
            .getIntent(requireContext())
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
    }

    var imageFile: File? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                val result: CropImage.ActivityResult = CropImage.getActivityResult(data)!!
                val resultUri: Uri = result.uriContent!!
                imageFile = File(result.getUriFilePath(requireContext(), true))

                Glide.with(requireContext()).load(resultUri).circleCrop()
                    .into(binding!!.ivUserImage)
                hitEditProfileApiOnlyImage()
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }
    }

    fun hitEditProfileApiOnlyImage() {
        Log.d("a.ksfhdlbasyklcnh;sef","""""""""");

        RestClient.getInst().hitEditProfileApiOnlyPhoto(
            PrefManager.getInstance(requireContext())!!.userDetail.token,
            imageFile,
            object : Callback<ModelSuccess> {
                override fun onResponse(
                    call: Call<ModelSuccess>,
                    response: Response<ModelSuccess>
                ) {
                    if (response.body()!!.result) {
                            getProfile()
                    }

                }

                override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                }
            })
    }

    fun updateProfile() {
        binding.etEmail.setText(PrefManager.getInstance(requireContext())!!.userDetail.email)
        binding.etName.setText(PrefManager.getInstance(requireContext())!!.userDetail.name)
        Glide.with(requireActivity())
            .load(PrefManager.getInstance(requireContext())!!.userDetail.image)
            .into(binding.ivUserImage)
    }

}