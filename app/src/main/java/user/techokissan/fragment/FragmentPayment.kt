package user.techokissan.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.activity.ActivityFinalPayment
import user.techokissan.databinding.FragmentPaymentBinding
import user.techokissan.models.ModelUser
import user.techokissan.networking.RestClient
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentPayment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentPayment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
private lateinit var binding:FragmentPaymentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_payment, container, false)
        binding=DataBindingUtil.bind<FragmentPaymentBinding>(view)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonPayAmount.setOnClickListener {

            if (PrefManager.getInstance(requireActivity())!!.userDetail.wallet.toDouble()>=activity!!.intent.getStringExtra("total_amt").toString().toDouble()){



               if (activity!!.intent.getStringExtra("type")!=null){
                   if (activity!!.intent.getStringExtra("type")=="bill"){
                       (activity as ActivityFinalPayment?)!!.payBillAmount()
                   }else{
                       (activity as ActivityFinalPayment?)!!.placeOrderApi()
                   }
               }


            }else{
                Toast.makeText(requireActivity(),"Insufficient Balance in Wallet. Please add to Wallet First",Toast.LENGTH_SHORT).show()
            }


        }
        binding.buttonAddToWallet.setOnClickListener {
            findNavController().navigate(R.id.idFragmentWallet)
        }
        Glide.with(context!!).load(PrefManager.getInstance(context!!)!!.userDetail.image).circleCrop().into(binding.imageView2)
        binding.tvPayableAmount.text="Payable Amount : INR "+activity!!.intent.getStringExtra("total_amt")
getProfile()
    }


    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token

        RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {

                if (response.body()!!.result) {
                   binding.tvAmount.text="Your Wallet Balance : INR "+response.body()!!.data.wallet
PrefManager.getInstance(requireContext())!!.userDetail=response.body()!!.data
                } else {

                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {

            }
        })
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentPayment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentPayment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }




}