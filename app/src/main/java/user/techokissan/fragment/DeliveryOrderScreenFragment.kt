package user.techokissan.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.adapter.OrderHistoryAdapter
import user.techokissan.databinding.FragmentDeliveryOrderScreenBinding
import user.techokissan.models.OrderHistoryDataRes
import user.techokissan.models.OrderHistoryRes
import user.techokissan.networking.RestClient
import java.util.HashMap




class DeliveryOrderScreenFragment : Fragment() {
    private lateinit var binding: FragmentDeliveryOrderScreenBinding
    private lateinit var bannerAdapter: OrderHistoryAdapter
    private lateinit var mOrderList: ArrayList<OrderHistoryDataRes>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mOrderList = ArrayList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        val mBinding = FragmentDeliveryOrderScreenBinding.inflate(inflater, container, false)
        binding = mBinding
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callApi("delivered")
    }
    @SuppressLint("NotifyDataSetChanged")
    private  fun callApi(key: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["status"] = key
        RestClient.getInst().ORDER_HISTORY(hashMap).enqueue(object :
            Callback<OrderHistoryRes?> {
            override fun onResponse(
                call: Call<OrderHistoryRes?>?,
                response: Response<OrderHistoryRes?>
            ) {
                if (response.body()!!.result) {
                    if (response.body()!!.data.size > 0){
                        binding.mShowWalletHistoryLL.visibility = View.VISIBLE
                        binding.mNoDataFoundTV.visibility = View.GONE
                        binding.WalletRV.apply {
                            visibility = View.VISIBLE
                            bannerAdapter = OrderHistoryAdapter( requireContext(), response.body()!!.data)
                            val popularLayout = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                            layoutManager = popularLayout
                            itemAnimator = DefaultItemAnimator()
                            adapter = bannerAdapter
                        }
                        bannerAdapter.notifyDataSetChanged()
                    }else{
                        binding.mShowWalletHistoryLL.visibility = View.GONE
                        binding.mNoDataFoundTV.visibility = View.VISIBLE
                    }

                } else {
                }
            }

            override fun onFailure(call: Call<OrderHistoryRes?>?, t: Throwable) {

            }
        })
    }

}