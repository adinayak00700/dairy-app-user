package user.techokissan.fragment

import ProductAdapter
import SubCategoryAdapter
import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.datingapp.utilities.Constants.USER_TOKEN
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseFragment
import user.techokissan.databinding.FragmentProductListBinding
import user.techokissan.demo.CartAdapter
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val CATEGORY_ID = "catId"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentProductList.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentProductList : BaseFragment() {
    private  var search=""
    private var categoryId: String? = ""
    private var param2: String? = ""
    private var vendorId: String? = ""
    private lateinit var binding: FragmentProductListBinding
    private var isViewCartButtonShowing=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            categoryId = it.getString(CATEGORY_ID)
            vendorId = it.getString("vendorId")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind<FragmentProductListBinding>(
            inflater.inflate(
                R.layout.fragment_product_list,
                container,
                false
            )
        )!!
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentProductList.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(categoryId: String,vendorId: String) =
            FragmentProductList().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)
                    putString("vendorId", vendorId)

                }
            }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!categoryId.equals("")) {
            getAllSubcategory()
            getCartCount()

        } else {
            getAllProducts("")
            binding.layoutSubcategry.visibility = View.GONE
        }

        binding.layoutViewCart.setOnClickListener {
                startActivity(IntentHelper.getCartScreen(requireActivity()))
        }



    }

    private fun getAllProducts(subCategoryId: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["sub_categoryID"] = subCategoryId
        hashMap["search"] = search
        hashMap["vendorID"] = vendorId!!

        RestClient.getInst().products(hashMap).enqueue(object : Callback<ModelProducts?> {
            override fun onResponse(
                call: Call<ModelProducts?>?,
                response: Response<ModelProducts?>
            ) {
                if (response.body()!!.result) {
                    binding.rvProducts.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    var adapter = ProductAdapter(requireContext(),object :ProductAdapter.Callbackk{
                        override fun onCartValueChanged() {
                            getCartCount()
                        }

                    })
                    adapter.addProducts(response.body()!!.data)
                    binding.rvProducts.adapter = adapter


                    if (response.body()!!.data.size==0){
                        binding.layoutNoData.visibility=View.VISIBLE
                    }else{
                        binding.layoutNoData.visibility=View.GONE
                    }
                } else {
                }
            }
            override fun onFailure(call: Call<ModelProducts?>?, t: Throwable) {
            }
        })
    }





    private fun getAllSubcategory() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token
        hashMap["categoryID"] = this.categoryId!!
        hashMap["vendorID"] = this.vendorId!!


        RestClient.getInst().subcategory(hashMap).enqueue(object : Callback<ModelSubcategory?> {
            override fun onResponse(
                call: Call<ModelSubcategory?>?,
                response: Response<ModelSubcategory?>
            ) {
                if (response.body()!!.result) {


                    var adapter = SubCategoryAdapter(
                        requireContext(),
                        response.body()!!.data,
                        object : SubCategoryAdapter.Callbackk {
                            override fun onClickOnTopCat(catId: String) {
                                getAllProducts(catId)
                            }

                        })
                    binding.rvSubCategory.adapter = adapter
                    binding.rvSubCategory.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    if (response.body()!!.data.size > 0) {
                        getAllProducts(response.body()!!.data[0].id)
                        binding.layoutSubcategry.visibility = View.VISIBLE
                    }else{
                        binding.layoutSubcategry.visibility = View.GONE
                    }
                } else {

                }
            }

            override fun onFailure(call: Call<ModelSubcategory?>?, t: Throwable) {

            }
        })
    }

    fun searchData(dataa:String){
        this.search=dataa
        getAllProducts("")
    }


     fun getCartCount() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireActivity())!!.userDetail.token
        RestClient.getInst().USER_CART(hashMap).enqueue(object :
            Callback<CartListRes?> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CartListRes?>?,
                response: Response<CartListRes?>
            ) {
                if (response.body()!!.result) {

                    binding.tvCartItemCount.text=response.body()!!.data.size.toString()+" Items"
                  binding.tvAmounttt.text=response.body()!!.total_amount+" INR"
                    if (response.body()!!.data.size!=0){
                        showViewCart(true)
                    }else{
                        showViewCart(false)
                    }
                } else {
                }
            }

            override fun onFailure(call: Call<CartListRes?>?, t: Throwable) {

            }
        })
    }
    fun showViewCart(show: Boolean) {

        if (show) {
            isViewCartButtonShowing = true
            binding.layoutViewCart.visibility=View.VISIBLE
        } else {
            isViewCartButtonShowing = false
            binding.layoutViewCart.visibility=View.GONE
        }




    }

}