package user.techokissan.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.adapter.MySubscriptionAdapter
import user.techokissan.adapter.SubscriptionPackageAdapter
import user.techokissan.databinding.FragmentMySubscriptionBinding
import user.techokissan.models.ModelMySubscription
import user.techokissan.models.ModelProducts
import user.techokissan.models.SubscriptionData
import user.techokissan.networking.RestClient
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentMySubscription.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentMySubscription : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
private lateinit var binding:FragmentMySubscriptionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_my_subscription, container, false)
        binding=DataBindingUtil.bind<FragmentMySubscriptionBinding>(view)!!
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getSubscriptions()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentMySubscription.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentMySubscription().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun getSubscriptions() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(requireContext())!!.userDetail.token


        RestClient.getInst().getMySubscriptions(hashMap).enqueue(object : Callback<ModelMySubscription?> {
            override fun onResponse(
                call: Call<ModelMySubscription?>?,
                response: Response<ModelMySubscription?>
            ) {
                if (response.body()!!.result) {
                    binding.tvSubscription.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    var adapter = MySubscriptionAdapter(requireContext())
                    adapter.addPackages(response.body()!!.data)
                    binding.tvSubscription.adapter = adapter

                } else {
                }
            }

            override fun onFailure(call: Call<ModelMySubscription?>?, t: Throwable) {

            }
        })
    }


}