package user.techokissan.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import user.techokissan.R
import user.techokissan.databinding.ActivityProductListBinding
import user.techokissan.fragment.FragmentProductList
import user.techokissan.fragment.FragmentProductSearch
import java.lang.Exception

class ActivityProductList : AppCompatActivity() {
    private lateinit var binding:ActivityProductListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_product_list)
        binding.toolbar.tvhead.text="Search Products"
        binding.toolbar.topAppBar.setNavigationOnClickListener {
                finish()
        }

        val bundle = intent.extras
        if (bundle!=null){
            Log.d("asjdkasd","alsdjalsd")
            var vendorId=""
        if (bundle!!.getString("vendorId")!=null){
            vendorId=bundle!!.getString("vendorId")!!
        }
           var fragmentProductList=FragmentProductList.newInstance(bundle!!.getString("catId")!!,vendorId)
            binding.toolbar.tvhead.text="Products"
            loadFragment(fragmentProductList)
        }else{
            loadFragment(FragmentProductSearch())
        }


    }


    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        var fragmentTemp: Fragment? = mFragmentManager.findFragmentByTag(backStateName)
        if (fragmentTemp == null) {
            fragmentTemp = fragment
            fragmentTransaction.add(binding!!.frame.id, (fragmentTemp)!!, backStateName)
        } else {
            fragmentTransaction.show(fragmentTemp)
        }
        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.commitNowAllowingStateLoss()
    }

    override fun onResume() {
        super.onResume()

    try {
        var fragmnt=supportFragmentManager.findFragmentById(R.id.frame) as FragmentProductList
        fragmnt.getCartCount()
    }catch (e:Exception){

    }
    }

}