package user.techokissan.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import user.techokissan.R
import user.techokissan.databinding.ActivityOrderScreenBinding
import user.techokissan.fragment.CancelledOrderScreenFragment
import user.techokissan.fragment.DeliveryOrderScreenFragment
import user.techokissan.fragment.OngoingOrderScreenFragment

class OrderScreenActivity : AppCompatActivity() , View.OnClickListener{
    private lateinit var binding : ActivityOrderScreenBinding
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        binding = ActivityOrderScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val personalInfoFragment = OngoingOrderScreenFragment()
        replaceFragment(personalInfoFragment)
        binding.mPersonalLL.background = ContextCompat.getDrawable(this ,R.drawable.item_order_bg_unselected )
        binding.mBusinessProfileLL.background = ContextCompat.getDrawable(this ,R.drawable.item_list_order_bg_selected )
        binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(this ,R.color.white ))
        binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(this ,R.color.black ))

        binding.mPersonalLL.setOnClickListener(this)
        binding.mBusinessProfileLL.setOnClickListener(this)
        binding.mCancelledLL.setOnClickListener(this)
        binding.toolbar.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.mPersonalLL ->{
                binding.mPersonalLL.background = ContextCompat.getDrawable(this ,R.drawable.item_list_order_bg_selected )
                binding.mBusinessProfileLL.background = ContextCompat.getDrawable(this ,R.drawable.item_order_bg_unselected )
                binding.mCancelledLL.background = ContextCompat.getDrawable(this ,R.drawable.item_order_bg_unselected )
                binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(this ,R.color.black ))
                binding.mCancelledTV.setTextColor(ContextCompat.getColor(this ,R.color.black ))
                binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(this ,R.color.white ))
                val personalInfoFragment = DeliveryOrderScreenFragment()
                replaceFragment(personalInfoFragment)
            }
            R.id.mBusinessProfileLL ->{
                binding.mPersonalLL.background = ContextCompat.getDrawable(this ,R.drawable.item_order_bg_unselected )
                binding.mCancelledLL.background = ContextCompat.getDrawable(this ,R.drawable.item_order_bg_unselected )
                binding.mBusinessProfileLL.background = ContextCompat.getDrawable(this ,R.drawable.item_list_order_bg_selected )
                binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(this ,R.color.white ))
                binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(this ,R.color.black))
                binding.mCancelledTV.setTextColor(ContextCompat.getColor(this ,R.color.black ))
                val businessProfile = OngoingOrderScreenFragment()
                replaceFragment(businessProfile)
            }
            R.id.mCancelledLL->{
                binding.mBusinessProfileLL.background = ContextCompat.getDrawable(this ,R.drawable.item_order_bg_unselected )
                binding.mPersonalLL.background = ContextCompat.getDrawable(this ,R.drawable.item_order_bg_unselected )
                binding.mCancelledLL.background = ContextCompat.getDrawable(this ,R.drawable.item_list_order_bg_selected )
                binding.mCancelledTV.setTextColor(ContextCompat.getColor(this ,R.color.white))
                binding.mBusinessTextTV.setTextColor(ContextCompat.getColor(this ,R.color.black))
                binding.mPersonalTextTV.setTextColor(ContextCompat.getColor(this ,R.color.black))
                val personalInfoFragment = CancelledOrderScreenFragment()
                replaceFragment(personalInfoFragment)

            }
            R.id.toolbar ->{
                onBackPressed()
            }

        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.mContainerViewContainer, fragment)
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}