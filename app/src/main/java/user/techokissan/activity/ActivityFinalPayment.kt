package user.techokissan.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.gson.Gson
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseActivity
import user.techokissan.databinding.ActivityFinalPaymentBinding
import user.techokissan.demo.CartAdapter
import user.techokissan.fragment.FragmentWallet
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import java.util.HashMap

class ActivityFinalPayment : BaseActivity(),PaymentResultWithDataListener {

    private var data: String? = null
    private var order_amt: String? = null
    private var total_amt: String? = null
    private var addressId: String? = null
    private lateinit var binding: ActivityFinalPaymentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_final_payment)
        findNavController(R.id.nav_host_fragment)
        getDataFromPrevScreen()
    }

    override fun onClick(viewId: Int, view: View?) {

    }


    fun getDataFromPrevScreen() {


        addressId = intent.getStringExtra("addressID")
        order_amt = intent.getStringExtra("order_amt")
        total_amt = intent.getStringExtra("total_amt")

        data = intent.getStringExtra("items")


    }

    fun payBillAmount() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["billAmount"] = total_amt.toString()
        hashMap["package_id"] = intent.getStringExtra("mPackageID").toString()
        hashMap["order_id"] = intent.getStringExtra("orderId").toString()





        RestClient.getInst().billpayment(hashMap).enqueue(object :
            Callback<ModelSuccess?> {
            override fun onResponse(
                call: Call<ModelSuccess?>?,
                response: Response<ModelSuccess?>
            ) {
                if (response.body()!!.result) {
                    Toast.makeText(
                        ActivityFinalPayment@this@ActivityFinalPayment,
                        "Payment Successful",
                        Toast.LENGTH_SHORT
                    ).show()
                    val intent =
                        Intent(this@ActivityFinalPayment, ActivityDashboard::class.java)
                    startActivity(intent)
                    finish()

                } else {
                    Toast.makeText(
                        this@ActivityFinalPayment,
                        "Failed",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }
        })
    }


    fun placeOrderApi() {
        if (intent.getStringExtra("type").equals("subscription", true)) {
            val hashMap: HashMap<String, String> = HashMap()
            hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
            hashMap["addressID"] = addressId!!
            hashMap["order_amt"] = order_amt.toString()
            hashMap["total_amt"] = order_amt.toString()
            hashMap["total_amount"] = order_amt.toString()
            hashMap["items"] = data!!
            hashMap["package_qty"] = "1"
            hashMap["dayCount"] = "0"



            hashMap["delivery_date"] = intent.getStringExtra("mSlotDate").toString()
            hashMap["subscription_type"] = intent.getStringExtra("mSubscriptionType").toString()
            hashMap["package_id"] = intent.getStringExtra("mPackageID").toString()






            RestClient.getInst().PLACE_ORDER_Package(hashMap).enqueue(object :
                Callback<ModelSuccess?> {
                override fun onResponse(
                    call: Call<ModelSuccess?>?,
                    response: Response<ModelSuccess?>
                ) {
                    if (response.body()!!.result) {
                        Toast.makeText(
                            ActivityFinalPayment@this@ActivityFinalPayment,
                            "Subscribed Successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent =
                            Intent(this@ActivityFinalPayment, ActivityDashboard::class.java)
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(
                            this@ActivityFinalPayment,
                            "Failed",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

                }
            })
        } else {
            val hashMap: HashMap<String, String> = HashMap()
            hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
            hashMap["addressID"] = addressId!!
            hashMap["order_amt"] = total_amt.toString()
            hashMap["total_amt"] = total_amt.toString()
            hashMap["total_amount"] = total_amt.toString()
            hashMap["items"] = data!!
            hashMap["delivery_date"] = ""
            hashMap["city_id"] = "1"
            hashMap["package_qty"] = "0"
            hashMap["dayCount"] = "0"
            hashMap["subscription_type"] = ""
            hashMap["package_id"] = "0"
            RestClient.getInst().PLACE_ORDER(hashMap).enqueue(object :
                Callback<OrderPlacesbean?> {
                override fun onResponse(
                    call: Call<OrderPlacesbean?>?,
                    response: Response<OrderPlacesbean?>
                ) {
                    if (response.body()!!.result) {
                        Toast.makeText(
                            this@ActivityFinalPayment,
                            "Order Placed Successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent =
                            Intent(this@ActivityFinalPayment, ActivityDashboard::class.java)
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(
                            this@ActivityFinalPayment,
                            "Order Failed",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<OrderPlacesbean?>?, t: Throwable) {

                }
            })
        }

    }

    override fun onPaymentSuccess(p0: String?, p1: PaymentData?) {
        try {
            showSnackbar("Payment Successful")

            var fragment=getForegroundFragment() as FragmentWallet
            fragment.addToWallet(p1!!.paymentId!!)
        } catch (e: Exception) {
            Log.d("asdasdad",e.message!!)
        }
    }

    override fun onPaymentError(p0: Int, p1: String?, p2: PaymentData?) {

    }

    fun getForegroundFragment(): Fragment? {
        val navHostFragment: Fragment? =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        return if (navHostFragment == null) null else navHostFragment.getChildFragmentManager()
            .getFragments().get(0)
    }


}