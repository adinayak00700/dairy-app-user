package user.techokissan.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.cabuser.utilities.DeviceLocationManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.razorpay.PaymentResultListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.adapter.DrawerAdapter
import user.techokissan.base.BaseActivity
import user.techokissan.databinding.ActivityDashboardBinding
import user.techokissan.databinding.HeaderProfileBinding
import user.techokissan.fragment.FlagmentHome
import user.techokissan.fragment.FragmentWallet
import user.techokissan.models.ModelProducts
import user.techokissan.models.ModelSuccess
import user.techokissan.models.ModelUser
import user.techokissan.networking.RestClient
import user.techokissan.utilities.AddressManager
import user.techokissan.utilities.IntentHelper
import java.util.HashMap
import kotlin.math.ln


class ActivityDashboard : BaseActivity(), PaymentResultListener, DrawerAdapter.Callback {
    var headerProfileBinding: HeaderProfileBinding? = null
    lateinit var navController: NavController
    lateinit var binding: ActivityDashboardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        binding.toolbar.tvhead.text = "Techo Kissan"
        setUpNavigation()
        //  getPIPmode()
        setupDrawer()

        init()
        setupProfileData()
        getProfile()

    }


    fun init() {
        val header_layout = findViewById<ConstraintLayout>(R.id.header_profile)
        val headerView: View =
            LayoutInflater.from(this).inflate(R.layout.header_profile, header_layout, false)

        headerProfileBinding = DataBindingUtil.bind(headerView)
        binding!!.lvDrawer.addHeaderView(headerView)

  /*      binding.toolbar.tvhead.setOnClickListener {

            val intet = Intent(this, ActivityUpdateLocation::class.java)
            startActivity(intet)


        }*/
        var drawerAdapter = DrawerAdapter(this, this)
        binding.lvDrawer.adapter = drawerAdapter

        binding.toolbar.topAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.search -> {
                    startActivity(IntentHelper.getProductsLIstScreen(this))
                    true
                }
                R.id.cart -> {
                    val intet = Intent(this, CartScreenActivity::class.java)
                    startActivity(intet)
                    // navController.navigate(R.id.idFragmentCart)
                    true
                }
                /*         R.id.notification -> {
                             navController.navigate(R.id.idFragmentNotification)
                             true
                         }*/
                else -> false
            }
        }

        binding.toolbar.topAppBar.setNavigationOnClickListener {
            binding.drawerLayout.open()
        }

        /*  binding.toolbar.navigationView.setNavigationItemSelectedListener { menuItem ->
              // Handle menu item selected
              menuItem.isChecked=true
              binding.drawerLayout.close()
              true
          }*/

        deviceLocationManager =
            DeviceLocationManager(this@ActivityDashboard,
                object : DeviceLocationManager.Callbackk {
                    override fun onGettingCurrentLocation(
                        latitude: Double,
                        longitude: Double
                    ) {
                        var addressManager=AddressManager()
                        addressManager.findAddress(LatLng(latitude, longitude),true)
                        addressManager.setCallbacks(object :AddressManager.AddressCallbacks{
                            override fun onPickUpAddressFound(pickAddress: String?) {
                                if (pickAddress != null) {
                                    updateLocationApi(latitude,longitude,pickAddress)
                                }
                            }

                            override fun onDropAddressFound(dropAddress: String?) {

                            }

                        })
                    }
                })

    }

    private fun toggle() {
        if (binding.drawerLayout.isDrawerVisible(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    override fun onClick(viewId: Int, view: View?) {

    }

    private fun setupDrawer() {
        binding.drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                //   binding.menuu.progress= slideOffset
            }

            override fun onDrawerOpened(drawerView: View) {

            }

            override fun onDrawerClosed(drawerView: View) {

            }

            override fun onDrawerStateChanged(newState: Int) {

            }
        })
    }
    lateinit var deviceLocationManager: DeviceLocationManager
    fun setUpNavigation() {
        navController = findNavController(R.id.nav_host_fragment)
        setupWithNavController(
            binding.navView,
            navController
        )


        navController.addOnDestinationChangedListener(object :
            NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {

                when (destination.id) {
                    R.id.idFragmentHome -> {

                        if (PrefManager.getInstance(this@ActivityDashboard)!!.userDetail.address!=""){
                            binding.toolbar.tvhead.text = PrefManager.getInstance(this@ActivityDashboard)!!.userDetail.address
                        }else{
                            binding.toolbar.tvhead.text="Update Address"
                        }
                        binding.toolbar.tvhead.setOnClickListener {
                            binding.toolbar.tvhead.text = "Fetching Location..."

                            deviceLocationManager.getCurrentLocationn()
                        }
                    }

                    R.id.idFragmentWallet -> {
                        binding.toolbar.tvhead.text = "Wallet"
                        binding.toolbar.tvhead.setOnClickListener(null)
                    }

                    R.id.idFragmentOrders -> {
                        binding.toolbar.tvhead.text = "Orders"
                        binding.toolbar.tvhead.setOnClickListener(null)
                    }

                    R.id.idFragmentProfileView -> {
                        binding.toolbar.tvhead.text = "Profile"
                        binding.toolbar.tvhead.setOnClickListener(null)
                    }
                }

                Log.d("asdasdasd", destination.displayName.toString())
            }
        })


    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        deviceLocationManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override
    fun onPaymentSuccess(razorpayPaymentID: String) {
        try {
            showSnackbar("Payment Successful")

            var fragment = getForegroundFragment() as FragmentWallet
            fragment.addToWallet(razorpayPaymentID)
        } catch (e: Exception) {
            Log.d("asdasdad", e.message!!)
        }
    }

    fun getForegroundFragment(): Fragment? {
        val navHostFragment: Fragment? =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        return if (navHostFragment == null) null else navHostFragment.getChildFragmentManager()
            .getFragments().get(0)
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    override
    fun onPaymentError(code: Int, response: String) {
        try {
            showSnackbar("Payment Successful")
        } catch (e: Exception) {
        }
    }


    private fun setupProfileData() {
        try {
            headerProfileBinding!!.tvUserName.text =
                PrefManager.getInstance(this)!!.userDetail.name
            Glide.with(this@ActivityDashboard)
                .load(PrefManager.getInstance(this)!!.userDetail.image).circleCrop()
                .into(headerProfileBinding!!.ivUserImage)
            headerProfileBinding!!.root.setOnClickListener {
                binding.navView.selectedItemId = R.id.idFragmentProfileView
                toggle()
            }
        } catch (e: Exception) {

        }
    }

    override fun onClickonDraweerItem(position: Int) {
        toggle()
        when (position) {
            0 -> {
                binding.navView.selectedItemId = R.id.idFragmentProfileView
            }

            1 -> {
                binding.navView.selectedItemId = R.id.idFragmentProfileView
            }


            2 -> {
                binding.navView.selectedItemId = R.id.idFragmentWallet
            }


            3 -> {

                startActivity(IntentHelper.getMySubscriptionScreen(this))
            }

            4 -> {
                binding.navView.selectedItemId = R.id.idFragmentOrders
            }
            5 -> {
                startActivity(IntentHelper.getBillActivity(this))
            }

            8 -> {
                startActivity(IntentHelper.getLoginScreen(this))
                PrefManager.getInstance(this)!!.clearPrefs()
                finishAffinity()
            }


        }
    }


    private fun updateLocationApi(lat: Double,lng:Double,address:String) {
        val hashMap: java.util.HashMap<String, String> = java.util.HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["lat"] = lat.toString()
        hashMap["lng"] = lng.toString()
        hashMap["address"] = address.toString()

        RestClient.getInst().updateCoordinates(hashMap).enqueue(object : Callback<ModelSuccess?> {
            override fun onResponse(
                call: Call<ModelSuccess?>?,
                response: Response<ModelSuccess?>
            ) {
                if (response.body()!!.result) {

                    try {

                        var fragment = getForegroundFragment() as FlagmentHome
                        fragment.getProducts()
                    } catch (e: Exception) {
                        Log.d("asdasdad", e.message!!)
                    }

                getProfile()
                } else {
                }
            }
            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {
            }
        })
    }

    fun getProfile() {

        val hashMap = HashMap<String, String>()
        hashMap[Constants.USER_TOKEN] =PrefManager.getInstance(this)!!.userDetail.token

        RestClient.getInst().getProfile(hashMap).enqueue(object : Callback<ModelUser> {
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {

                if (response.body()!!.result) {

                    PrefManager.getInstance(this@ActivityDashboard)!!.userDetail=response.body()!!.data

                       if (response.body()!!.data.address!=null || response.body()!!.data.address!=""){
                           binding.toolbar.tvhead.text=response.body()!!.data.address
                       }else{
                           binding.toolbar.tvhead.text="Update Address"
                       }



                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

}