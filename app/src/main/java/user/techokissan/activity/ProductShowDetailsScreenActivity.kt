package user.techokissan.activity

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import user.techokissan.demo.DayAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.adapter.SubscriptionPackageAdapter
import user.techokissan.databinding.ActivityProductShowDetailsScreenBinding
import user.techokissan.models.ProductDetailsRes
import user.techokissan.networking.RestClient
import java.util.HashMap

class ProductShowDetailsScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProductShowDetailsScreenBinding
    private lateinit var notificationAdapter : SubscriptionPackageAdapter
    private lateinit var dayAdapter  : DayAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  ActivityProductShowDetailsScreenBinding.inflate(layoutInflater)
        val view  =binding.root
        setContentView(view)
        PRODUCTS_DETAILS(intent.getStringExtra("productID")!!)


        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
        binding.toolbar.tvhead.text="Subscribe"

    }



    private fun PRODUCTS_DETAILS(productID: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["productID"] = productID
        RestClient.getInst().PRODUCTS_DETAILS(hashMap).enqueue(object :
            Callback<ProductDetailsRes?> {
            @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
            override fun onResponse(
                call: Call<ProductDetailsRes?>?,
                response: Response<ProductDetailsRes?>){
                if (response.body()!!.result) {
                    Glide.with(this@ProductShowDetailsScreenActivity).load(response.body()!!.data.product_image).into(binding.restroImage)
                    binding.title.text =response.body()!!.data.product_name



                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        binding.tvDexriptionValiue.setText(Html.fromHtml(response.body()!!.data.description, Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        binding.tvDexriptionValiue.setText(Html.fromHtml(response.body()!!.data.description));
                    }





                    binding.price.text ="₹ "+response.body()!!.data.price
                    binding.mrp.text ="Mrp ₹ "+ response.body()!!.data.mrp
                    binding.mrp.setPaintFlags(binding.mrp.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
                    binding.WalletRV.visibility = View.VISIBLE
                  //  binding.mNoDataFoundTV.visibility = View.GONE
                    binding.WalletRV.apply {
                        visibility = View.VISIBLE
                        notificationAdapter = SubscriptionPackageAdapter(this@ProductShowDetailsScreenActivity , response.body()!!.data.mPackage , intent.getStringExtra("productID")!!)
                        val popularLayout = LinearLayoutManager(this@ProductShowDetailsScreenActivity, LinearLayoutManager.VERTICAL, false)
                        layoutManager = popularLayout
                        itemAnimator = DefaultItemAnimator()
                        adapter = notificationAdapter
                    }
                    notificationAdapter.notifyDataSetChanged()

                } else {
                }
            }

            override fun onFailure(call: Call<ProductDetailsRes?>?, t: Throwable) {

            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}