package user.techokissan.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.databinding.ActivityProductDetailsScreenBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.Product
import user.techokissan.models.ProductDetailsRes
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper

class ProductDetailsScreenActivity : AppCompatActivity() {
   private lateinit var productsList : ArrayList<Product>
    private lateinit var binding: ActivityProductDetailsScreenBinding
    private var mCartCount : Int =0
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =  ActivityProductDetailsScreenBinding.inflate(layoutInflater)

        val view  =binding.root

        setContentView(view)
        binding.toolbar.setOnClickListener{
            onBackPressed()
        }
        productsList = intent.getSerializableExtra("product") as ArrayList<Product>
        PRODUCTS_DETAILS(productsList[intent.getIntExtra("position" , -1)].id)


        binding.mBtnNext.setOnClickListener{
            val intent = Intent(this , CartScreenActivity::class.java)
            startActivity(intent)
        }
        binding.tvViewReviews.setOnClickListener {
           startActivity(IntentHelper.getReviewScreen(this).putExtra("productId",productsList[intent.getIntExtra("position" , -1)].id).putExtra("type","product"))
        }
       /* Glide.with(this).load(productsList[intent.getIntExtra("position" , -1)].product_image).into(binding.mImgIV)
        binding.mNameTV.text = productsList[intent.getIntExtra("position" , -1)].product_name
        binding.price.text = "₹ "+productsList[intent.getIntExtra("position" , -1)].price
        binding.mrp.text ="MRP ₹ "+ productsList[intent.getIntExtra("position" , -1)].mrp
        binding.mUnitTV.text = productsList[intent.getIntExtra("position" , -1)].unit_value+" "+productsList[intent.getIntExtra("position" , -1)].unit


        if (productsList[intent.getIntExtra("position" , -1)].cartCount > 0){
            binding.mCountLL.visibility = View.VISIBLE
            binding.mAddLL.visibility = View.GONE
            binding.mCountTV.text = productsList[intent.getIntExtra("position" , -1)].cartCount.toString()
        }else{
            binding.mCountLL.visibility = View.GONE
            binding.mAddLL.visibility = View.VISIBLE
        }
*/



    }


    private fun PRODUCTS_DETAILS(productID: String) {
        val hashMap: java.util.HashMap<String, String> = java.util.HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["productID"] = productID
        RestClient.getInst().PRODUCTS_DETAILS(hashMap).enqueue(object :
            Callback<ProductDetailsRes?> {
            @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
            override fun onResponse(
                call: Call<ProductDetailsRes?>?,
                response: Response<ProductDetailsRes?>){
                if (response.body()!!.result) {
                    Glide.with(this@ProductDetailsScreenActivity).load(response.body()!!.data.product_image).into(binding.mImgIV)
                    binding.mNameTV.text = response.body()!!.data.product_name
                    binding.price.text = "₹ "+response.body()!!.data.price
                    binding.mrp.text ="MRP ₹ "+ response.body()!!.data.mrp
                    binding.mrp.paintFlags = binding.mrp.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    binding.mUnitTV.text = response.body()!!.data.unit_value+" "+response.body()!!.data.unit


                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        binding.mDescTV.setText(Html.fromHtml(response.body()!!.data.description,Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        binding.mDescTV.setText(Html.fromHtml( response.body()!!.data.description))
                    }


                    if (response.body()!!.data.cartCount.toInt() > 0){
                        binding.mCountLL.visibility = View.VISIBLE
                        binding.mAddLL.visibility = View.GONE
                        binding.mCountTV.text = response.body()!!.data.cartCount
                    }else{
                        binding.mCountLL.visibility = View.GONE
                        binding.mAddLL.visibility = View.VISIBLE
                    }

                    binding.mAddLL.setOnClickListener {
                        mCartCount = response.body()!!.data.cartCount.toInt()
                        mCartCount++
                        response.body()!!.data.cartCount =mCartCount.toString()
                        binding.mCountLL.visibility = View.VISIBLE
                        binding.mAddLL.visibility = View.GONE
                        binding.mCountTV.text =  response.body()!!.data.cartCount.toString()
                        addToCart(response.body()!!.data.cartCount, response.body()!!.data.id , "0")


                    }

                    binding.mPlusLL.setOnClickListener {
                       // productsList[intent.getIntExtra("position" , -1)].cartCount++
                        mCartCount = response.body()!!.data.cartCount.toInt()
                        mCartCount++
                        response.body()!!.data.cartCount =mCartCount.toString()
                        binding.mCountLL.visibility = View.VISIBLE
                        binding.mAddLL.visibility = View.GONE
                        binding.mCountTV.text = response.body()!!.data.cartCount
                        addToCart(response.body()!!.data.cartCount.toString() ,response.body()!!.data.id , "0")
                    }

                    binding.mMinusLL.setOnClickListener {
                       // productsList[intent.getIntExtra("position" , -1)].cartCount--
                        mCartCount = response.body()!!.data.cartCount.toInt()
                        mCartCount--
                        response.body()!!.data.cartCount =mCartCount.toString()
                        if (response.body()!!.data.cartCount.toInt() > 0){
                            binding.mCountTV.text = response.body()!!.data.cartCount.toString()
                            binding.mCountLL.visibility = View.VISIBLE
                            binding.mAddLL.visibility = View.GONE
                            addToCart(response.body()!!.data.cartCount.toString() ,response.body()!!.data.id , "0")
                        }else{
                            binding.mCountLL.visibility = View.GONE
                            binding.mAddLL.visibility = View.VISIBLE
                            addToCart(response.body()!!.data.cartCount ,response.body()!!.data.id, "0")
                        }
                    }

                } else {
                }
            }

            override fun onFailure(call: Call<ProductDetailsRes?>?, t: Throwable) {

            }
        })
    }

    private fun addToCart(qty: String, productID: String ,  package_id : String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["productID"] = productID
        hashMap["qty"] = qty
        hashMap["package_id"] = package_id
        RestClient.getInst().ADD_PRODUCT(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {

                } else {
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}