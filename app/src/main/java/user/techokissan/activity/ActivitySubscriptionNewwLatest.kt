package user.techokissan.activity

import QuantityAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.fitfarm.demo.DateAdapter
import com.michalsvec.singlerowcalendar.calendar.CalendarChangesObserver
import com.michalsvec.singlerowcalendar.calendar.CalendarViewManager
import com.michalsvec.singlerowcalendar.calendar.SingleRowCalendarAdapter
import com.michalsvec.singlerowcalendar.selection.CalendarSelectionManager
import com.michalsvec.singlerowcalendar.utils.DateUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.adapter.CustomeDateAdapter
import user.techokissan.databinding.ActivitySubscriptionNewwLatestBinding
import user.techokissan.demo.DayAdapter
import user.techokissan.demo.DayAdapterNew
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class ActivitySubscriptionNewwLatest : AppCompatActivity(), DateAdapter.OnSubscriptionListener,
    DayAdapter.OnWeeklyCountListener, CustomeDateAdapter.OnDateListener, QuantityAdapter.Callbackk {

    private lateinit var binding: ActivitySubscriptionNewwLatestBinding
    private lateinit var notificationAdapter: DateAdapter
    private lateinit var mCustomDateAdapter: CustomeDateAdapter
    private lateinit var dayAdapter: DayAdapterNew

    private lateinit var subscriptionTypeModel: ArrayList<SubscriptionTypeModel>
    private lateinit var weeklyQuantityModel: ArrayList<ModelQuantity>
    private lateinit var everydayQuantityModel: ArrayList<ModelQuantity>
    private lateinit var alternateQuantityModel: ArrayList<ModelQuantity>
    private var subscriptionType = ""
    private var isAddressSelected = false
    private var isSubscritionTypeSelected = false
    private var isTrialPack = false
    private lateinit var address: UserAddressDataListRes
    private lateinit var quantityAdapterForEveryday: QuantityAdapter
    private lateinit var quantityAdapterForAlternate: QuantityAdapter
    private lateinit var quantityAdapterForWeekly: QuantityAdapter
    private var availableQuantity = 0
    private var totalQuantity = 0
    var isStartDateSelected = false
    private var daysCountForExpiry = 0
    private var filledQuantity = 0
    private val calendar = Calendar.getInstance()
    private var currentMonth = 0
    private var selectedStartDate = Date()
    private lateinit var expiryDate: Date
    private lateinit var packageDetails: ProductPackageRes

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubscriptionNewwLatestBinding.inflate(layoutInflater)

        if (intent
                .getStringExtra("isTrial") == "Y"
        ) {
            isTrialPack = true


        }






        val view = binding.root
        setContentView(view)

        // set current date to calendar and current month to currentMonth variable
        calendar.time = Date()
        currentMonth = calendar[Calendar.MONTH]

        subscriptionTypeModel = ArrayList()
        weeklyQuantityModel = ArrayList()
        alternateQuantityModel = ArrayList()
        everydayQuantityModel = ArrayList()



        subscriptionTypeModel.add(SubscriptionTypeModel(1, "everyday"))
        subscriptionTypeModel.add(SubscriptionTypeModel(2, "weekly"))
        subscriptionTypeModel.add(SubscriptionTypeModel(3, "alternate"))


        weeklyQuantityModel.add(ModelQuantity("Monday", 0))
        weeklyQuantityModel.add(ModelQuantity("Tuesday", 0))
        weeklyQuantityModel.add(ModelQuantity("Wednesday", 0))
        weeklyQuantityModel.add(ModelQuantity("Thursday", 0))
        weeklyQuantityModel.add(ModelQuantity("Friday", 0))
        weeklyQuantityModel.add(ModelQuantity("Saturday", 0))
        weeklyQuantityModel.add(ModelQuantity("Sunday", 0))



        everydayQuantityModel.add(ModelQuantity("Quantity", 0))


        alternateQuantityModel.add(ModelQuantity("Day1", 0))
        alternateQuantityModel.add(ModelQuantity("Day2", 0))




        quantityAdapterForEveryday = QuantityAdapter(this, this)
        quantityAdapterForEveryday.addToList(everydayQuantityModel)
        quantityAdapterForAlternate = QuantityAdapter(this, this)
        quantityAdapterForAlternate.addToList(alternateQuantityModel)
        quantityAdapterForWeekly = QuantityAdapter(this, this)
        quantityAdapterForWeekly.addToList(weeklyQuantityModel)
        binding.mainRV.layoutManager = LinearLayoutManager(this)
        var mMorTime: String
        val sdf = SimpleDateFormat("EEEE yyyy-MM-dd")



        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
        binding.toolbar.tvhead.text="Subscribe"

        //  binding.mNoDataFoundTV.visibility = View.GONE
        binding.weeklyRV.apply {
            visibility = View.VISIBLE
            notificationAdapter =
                DateAdapter(this@ActivitySubscriptionNewwLatest, subscriptionTypeModel)
            val popularLayout = LinearLayoutManager(
                this@ActivitySubscriptionNewwLatest,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            layoutManager = popularLayout
            itemAnimator = DefaultItemAnimator()
            adapter = notificationAdapter
        }
        notificationAdapter.notifyDataSetChanged()




        PRODUCTS_DETAILS(intent.getStringExtra("productID")!!)


        initCalender()

        binding.mBtnNext.setOnClickListener {
            if (isStartDateSelected) {
                if (isAddressSelected) {
                    if (PrefManager.getInstance(this)!!.userDetail.wallet >= 1000) {
                      if (isSubscritionTypeSelected){
                          placeOrderApi()
                      }else{
                          Toast.makeText(
                              this,
                              "Please Select Subscription Type",
                              Toast.LENGTH_SHORT
                          ).show()
                      }
                    } else {
                        Toast.makeText(
                            this,
                            "Please add 1000 minimum in wallet as security",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    val intent = Intent(this, ShowAddressListScreenActivity::class.java)

                    startActivityForResult(intent, 110)

                }
            } else {
                Toast.makeText(this, "Select start Date", Toast.LENGTH_SHORT).show()
            }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 110 && resultCode == Activity.RESULT_OK) {

            isAddressSelected = true
            address = UserAddressDataListRes(
                data!!.getStringExtra("id").toString(),
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                data!!.getStringExtra("contact").toString(),
                "",
                "",
                data!!.getStringExtra("phone").toString(),
                "",
                "",
                data!!.getStringExtra("full_address").toString()
            )
            Log.d("asdasdasdasdasd", address.id)
            binding.tvContactNumber.text = address.phone
            binding.tvaddressFull.text = address.full_address
            binding.selectedAddressLayout.visibility = View.VISIBLE
        }
    }

    fun initCalender() {
        // set current date to calendar and current month to currentMonth variable
        calendar.time = Date()
        currentMonth = calendar[Calendar.MONTH]

        // enable white status bar with black icons
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = Color.WHITE
        }

        // calendar view manager is responsible for our displaying logic
        val myCalendarViewManager = object :
            CalendarViewManager {
            override fun setCalendarViewResourceId(
                position: Int,
                date: Date,
                isSelected: Boolean
            ): Int {
                // set date to calendar according to position where we are
                val cal = Calendar.getInstance()
                cal.time = date
                // if item is selected we return this layout items
                // in this example. monday, wednesday and friday will have special item views and other days
                // will be using basic item view
                return if (isSelected)
                    when (cal[Calendar.DAY_OF_WEEK]) {
                        /*            Calendar.MONDAY -> R.layout.first_special_selected_calendar_item
                                    Calendar.WEDNESDAY -> R.layout.second_special_selected_calendar_item
                                    Calendar.FRIDAY -> R.layout.third_special_selected_calendar_item*/
                        else -> R.layout.selected_calendar_item
                    }
                else
                // here we return items which are not selected
                    when (cal[Calendar.DAY_OF_WEEK]) {
                        /*      Calendar.MONDAY -> R.layout.first_special_calendar_item
                              Calendar.WEDNESDAY -> R.layout.second_special_calendar_item
                              Calendar.FRIDAY -> R.layout.third_special_calendar_item*/
                        else -> R.layout.calendar_item
                    }

                // NOTE: if we don't want to do it this way, we can simply change color of background
                // in bindDataToCalendarView method
            }

            override fun bindDataToCalendarView(
                holder: SingleRowCalendarAdapter.CalendarViewHolder,
                date: Date,
                position: Int,
                isSelected: Boolean
            ) {

                holder.itemView.findViewById<TextView>(R.id.tv_date_calendar_item).text =
                    DateUtils.getDayNumber(date)
                holder.itemView.findViewById<TextView>(R.id.tv_day_calendar_item).text =
                    DateUtils.getDay3LettersName(date)
                /*     // using this method we can bind data to calendar view
                     // good practice is if all views in layout have same IDs in all item views
                   holder.itemView.tv_date_calendar_item.text =
                     holder.itemView.tv_day_calendar_item.text = DateUtils.getDay3LettersName(date)*/

            }
        }

        // using calendar changes observer we can track changes in calendar
        val myCalendarChangesObserver = object :
            CalendarChangesObserver {
            // you can override more methods, in this example we need only this one
            override fun whenSelectionChanged(isSelected: Boolean, position: Int, date: Date) {
/*              tvDate.text = "${DateUtils.getMonthName(date)}, ${DateUtils.getDayNumber(date)} "
                tvDay.text = DateUtils.getDayName(date)*/
                selectedStartDate = date
                expiryDate = getExpiryDateCounter()
                isStartDateSelected = true
                Log.d("asdadassd", getExpiryDateCounter().toString())
                val dfLocal = SimpleDateFormat("dd MMM yyyy", Locale.US)
                val time = dfLocal.format(expiryDate)
                binding.tvExpiryDate.text = time
                super.whenSelectionChanged(isSelected, position, date)
            }
        }


        // selection manager is responsible for managing selection
        val mySelectionManager = object : CalendarSelectionManager {
            override fun canBeItemSelected(position: Int, date: Date): Boolean {
                // set date to calendar according to position
                val cal = Calendar.getInstance()
                cal.time = date
                // in this example sunday and saturday can't be selected, others can
                return when (cal[Calendar.DAY_OF_WEEK]) {
                    /*    Calendar.SATURDAY -> false
                        Calendar.SUNDAY -> false*/
                    else -> true
                }
            }
        }

        // here we init our calendar, also you can set more properties if you haven't specified in XML layout
        val singleRowCalendar = binding.mainSingleRowCalendar.apply {
            calendarViewManager = myCalendarViewManager
            calendarChangesObserver = myCalendarChangesObserver
            calendarSelectionManager = mySelectionManager
            setDates(getFutureDatesOfCurrentMonth())
            init()
        }

        /*   btnRight.setOnClickListener {
               singleRowCalendar.setDates(getDatesOfNextMonth())
           }

           btnLeft.setOnClickListener {
               singleRowCalendar.setDates(getDatesOfPreviousMonth())
           }*/
    }

   fun  trialPackPayment(){

   }

    private fun PRODUCTS_DETAILS(productID: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["productID"] = productID
        RestClient.getInst().PRODUCTS_DETAILS(hashMap).enqueue(object :
            Callback<ProductDetailsRes?> {
            @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
            override fun onResponse(
                call: Call<ProductDetailsRes?>?,
                response: Response<ProductDetailsRes?>
            ) {
                if (response.body()!!.result) {
                    Glide.with(this@ActivitySubscriptionNewwLatest)
                        .load(response.body()!!.data.product_image).into(binding.restroImage)
                    binding.title.text = response.body()!!.data.product_name

                    binding.price.text = "₹ " + response.body()!!.data.price
                    binding.mrp.text = "Mrp ₹ " + response.body()!!.data.mrp

                    binding.mrp.paintFlags = binding.mrp.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    // binding.mrp.text ="Mrp ₹ "+ response.body()!!.data.mrp
                    //   binding.WalletRV.visibility = View.VISIBLE


                    packageDetails =
                        response.body()!!.data.mPackage[intent.getStringExtra("packageIndex")!!
                            .toInt()]
                    binding.tvSubscriptionTitle.text = packageDetails.name
                    totalQuantity = packageDetails.qty.toInt()
                    availableQuantity = packageDetails.qty.toInt()



                    quantityAdapterForAlternate.setTotalAvailableQuantityy(availableQuantity)
                    quantityAdapterForEveryday.setTotalAvailableQuantityy(availableQuantity)
                    quantityAdapterForWeekly.setTotalAvailableQuantityy(availableQuantity)


                    val a =
                        Math.round(packageDetails.amount.toDouble() / packageDetails.qty.toInt())
                            .toInt()
                    binding.tvSubscriptiponPageSubHead.text =
                        packageDetails.qty + " packs @ ₹ " + a + "/pk"
                    /*    mPriceID =packageDetails.amount
                        mPackageID = packageDetails.id*/

                   if (isTrialPack){
                       binding.layoutTopWalletInsuffi.visibility=View.VISIBLE
                       if(PrefManager.getInstance(this@ActivitySubscriptionNewwLatest)!!.userDetail.wallet<packageDetails.amount.toString().toDouble()){

                           var remainingAmount=packageDetails.amount.toString().toDouble()-(PrefManager.getInstance(this@ActivitySubscriptionNewwLatest)!!.userDetail.wallet)
                           binding.tvAddAmountTowallet.text="Total Amount to Pay : INR"+packageDetails.amount+" \nYou need to add INR "+remainingAmount+" in your wallet to purchase this trial pack. PLease Add to wallet first."
                           binding.mBtnNext.setOnClickListener{
                               Toast.makeText(this@ActivitySubscriptionNewwLatest,"Add to wallet First",Toast.LENGTH_SHORT).show()
                           }
                       }else{
                           binding.tvAddAmountTowallet.text="Total Amount to Pay : INR"+packageDetails.amount+" \nwill be automatically debited from your wallet when you subscribe"

                       }
                   }else{
                       binding.layoutTopWalletInsuffi.visibility=View.VISIBLE
                       binding.tvTrialPack.visibility=View.GONE


                       binding.tvAddAmountTowallet.text="Total Subscription Amount : INR "+packageDetails.amount

                   }

                    //  binding.mNoDataFoundTV.visibility = View.GONE
                } else {
                }
            }

            override fun onFailure(call: Call<ProductDetailsRes?>?, t: Throwable) {

            }
        })
    }


    fun placeOrderApi() {

        val dfLocal = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        val time = dfLocal.format(selectedStartDate)


        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["addressID"] = address.id
        hashMap["total_amount"] = packageDetails.amount
        hashMap["subscription_type"] = subscriptionType
        hashMap["package_id"] = packageDetails.id
        hashMap["start_date"] = time
        hashMap["trail"] = ""
        if (subscriptionType == "everyday") {
            hashMap["subscription_value"] = quantityAdapterForEveryday.getSelectedQuantity()
        } else
            if (subscriptionType == "alternate") {
                hashMap["subscription_value"] = quantityAdapterForAlternate.getSelectedQuantity()
            } else if (subscriptionType == "weekly") {
                hashMap["subscription_value"] = quantityAdapterForWeekly.getSelectedQuantity()
            }




        RestClient.getInst().PLACE_ORDER_Package(hashMap).enqueue(object :
            Callback<ModelSuccess?> {
            override fun onResponse(
                call: Call<ModelSuccess?>?,
                response: Response<ModelSuccess?>
            ) {
                if (response.body()!!.result) {
                    Toast.makeText(
                        this@ActivitySubscriptionNewwLatest,
                        "Subscribed Successfully",
                        Toast.LENGTH_SHORT
                    ).show()
                    val intent =
                        Intent(this@ActivitySubscriptionNewwLatest, ActivityDashboard::class.java)
                    startActivity(intent)
                    finish()

                } else {
                    Toast.makeText(
                        this@ActivitySubscriptionNewwLatest,
                        "Failed",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }
        })

    }

    fun calculateTotalSubscritionAmount() {
        //   totalSubscriptionAmoiunt=(mPriceID.toString().toDouble()*mQty.toString().toDouble()).toString()
    }

    override fun onDateClick(position: Int, data: ArrayList<CustomDate>) {
        TODO("Not yet implemented")
    }

    override fun onSubscriptionClick(
        position: Int,
        subscriptionTypeModel: ArrayList<SubscriptionTypeModel>
    ) {
        Log.d("asdasda", subscriptionTypeModel[position].type)
        isSubscritionTypeSelected=true
        subscriptionType = subscriptionTypeModel[position].type
        if (subscriptionType == "everyday") {

            binding.mainRV.adapter = quantityAdapterForEveryday
        } else if (subscriptionType == "alternate") {
            binding.mainRV.adapter = quantityAdapterForAlternate
        } else if (subscriptionType == "weekly") {
            binding.mainRV.adapter = quantityAdapterForWeekly
        }
    }

    override fun onWeekClick(position: Int, data: ArrayList<SubscriptionDaysTypeModel>) {
        TODO("Not yet implemented")
    }

    private fun getFutureDatesOfCurrentMonth(): List<Date> {
        // get all next dates of current month
        currentMonth = calendar[Calendar.MONTH]
        return getDates(mutableListOf())
    }

    private fun getDates(list: MutableList<Date>): List<Date> {
        // load dates of whole month
        calendar.set(Calendar.MONTH, currentMonth)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        list.add(calendar.time)
        while (currentMonth == calendar[Calendar.MONTH]) {
            calendar.add(Calendar.DATE, +1)
            if (calendar[Calendar.MONTH] == currentMonth)
                list.add(calendar.time)
        }
        calendar.add(Calendar.DATE, -1)
        return list
    }


    fun getExpiryDateCounter(): Date {


        if (subscriptionType == "everyday") {
            val c = Calendar.getInstance()
            c.time = selectedStartDate
            availableQuantity =
                totalQuantity - quantityAdapterForEveryday.getTotalSelectedQuantityCount()
            Log.d("askdjkasa", availableQuantity.toString())
            Log.d(
                "askdjkasagetTotal",
                quantityAdapterForEveryday.getTotalSelectedQuantityCount().toString()
            )
            Log.d("askdjkasaTotalQuan", totalQuantity.toString())


            if (quantityAdapterForEveryday.getTotalSelectedQuantityCount() == 0) {//user not added anything
                c.add(Calendar.DATE, totalQuantity)
            } else {
                c.add(
                    Calendar.DATE,
                    totalQuantity / quantityAdapterForEveryday.getTotalSelectedQuantityCount()
                )
            }
            expiryDate = c.time
            if (filledQuantity == 0) {
                daysCountForExpiry = totalQuantity
            } else {
                daysCountForExpiry = totalQuantity / filledQuantity
            }
            return expiryDate
        } else
            if (subscriptionType == "alternate") {
                availableQuantity =
                    totalQuantity - quantityAdapterForAlternate.getTotalSelectedQuantityCount()
                val c = Calendar.getInstance()
                c.time = selectedStartDate



                if (quantityAdapterForAlternate.getTotalSelectedQuantityCount() == 0) {
                    c.add(Calendar.DATE, totalQuantity) // number of days to add
                } else {
                    c.add(
                        Calendar.DATE,
                        totalQuantity / quantityAdapterForAlternate.getTotalSelectedQuantityCount() * 2
                    ) // number of days to add
                }

                expiryDate = c.time






                if (filledQuantity == 0) {
                    daysCountForExpiry = totalQuantity
                } else {
                    daysCountForExpiry = totalQuantity * 2 / filledQuantity
                }


                return expiryDate
            } else if (subscriptionType == "weekly") {
                availableQuantity =
                    totalQuantity - quantityAdapterForWeekly.getTotalSelectedQuantityCount()
                val c = Calendar.getInstance()
                c.time = selectedStartDate

                if (quantityAdapterForWeekly.getTotalSelectedQuantityCount() == 0) {
                    c.add(Calendar.DATE, totalQuantity) // number of days to add
                } else {
                    c.add(
                        Calendar.DATE,
                        totalQuantity / quantityAdapterForWeekly.getTotalSelectedQuantityCount() * 7
                    ) // number of days to add
                }

                expiryDate = c.time
                if (filledQuantity == 0) {
                    daysCountForExpiry = totalQuantity
                } else {
                    daysCountForExpiry = totalQuantity * 7 / filledQuantity
                }


                return expiryDate
            }
        return Date()
    }
    override fun onQuantityChange() {
    }

}