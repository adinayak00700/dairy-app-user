package user.techokissan.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import user.techokissan.R
import user.techokissan.databinding.ActivityBillBinding
import user.techokissan.fragment.FragmentBills

class ActivityBill : AppCompatActivity() {
    private lateinit var binding:ActivityBillBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil. setContentView(this,R.layout.activity_bill)
        loadFragment(FragmentBills())
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }
        binding.toolbar.tvhead.text="Bills"
    }


    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        var fragmentTemp: Fragment? = mFragmentManager.findFragmentByTag(backStateName)
        if (fragmentTemp == null) {
            fragmentTemp = fragment
            fragmentTransaction.add(binding!!.frame.id, (fragmentTemp)!!, backStateName)
        } else {
            fragmentTransaction.show(fragmentTemp)
        }
        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.commitNowAllowingStateLoss()
    }
}