package com.wedguruphotographer

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sun.bob.mcalendarview.MarkStyle
import sun.bob.mcalendarview.listeners.OnDateClickListener
import sun.bob.mcalendarview.listeners.OnMonthScrollListener
import sun.bob.mcalendarview.views.ExpCalendarView
import sun.bob.mcalendarview.vo.DateData
import user.techokissan.R
import user.techokissan.base.BaseActivity
import user.techokissan.databinding.ActivityNoAvailabilityBinding
import user.techokissan.models.ModelDays
import user.techokissan.models.ModelSuccess
import user.techokissan.models.SubscriptionData
import user.techokissan.networking.RestClient
import user.techokissan.utilities.PriceFormater
import java.util.*

class ActivityNoAvailability : BaseActivity(), DatePickerDialog.OnDateSetListener {
    lateinit var binding: ActivityNoAvailabilityBinding
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    var selectedDate: String? = null
    var staffId: String? = null
    var totalHolidayInAMonth = 0
    var calender: ExpCalendarView? = null
    var currentPage = 0
    var holiDayDates = ArrayList<DateData>()

    @SuppressLint("NonConstantResourceId")
    override fun onClick(viewId: Int, view: View) {
        when (viewId) {
            R.id.buttonAddHoliday -> {
            }

            R.id.nextMonth -> calender!!.currentItem = ++currentPage
            R.id.prevMonth -> calender!!.currentItem = --currentPage
        }
    }

    private fun goBack() {
        if (bottomSheetBehavior!!.state == BottomSheetBehavior.STATE_EXPANDED) {
            collapse()
        } else {
            calender!!.markedDates.all.clear()
            calender!!.setOnDateClickListener(null)
            super.onBackPressed()
        }
    }

    override fun onBackPressed() {
        goBack()
    }

    fun expand() {
        Handler(Looper.getMainLooper()).postDelayed({
            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
        }, 250)
    }

    fun collapse() {
        Handler(Looper.getMainLooper()).postDelayed({
            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
        }, 250)
    }

        lateinit var subscriptionData:SubscriptionData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_no_availability)
        subscriptionData=intent.getSerializableExtra("data") as SubscriptionData
        calender = findViewById<View>(R.id.calendarPayroll) as ExpCalendarView
        calender!!.markedDates.removeAdd()
        calender!!.markedDates.all.clear()
        getAllDaysApi()
        calender!!.setOnDateClickListener(object : OnDateClickListener() {
            override fun onDateClick(view: View, date: DateData) {

              var month=  String.format("%02d", date.month);


                var day=  String.format("%02d", date.day);

              for (i in 0 until calender!!.markedDates.all.size){

                if (calender!!.markedDates.all[i].day==date.day && calender!!.markedDates.all[i].month==date.month && calender!!.markedDates.all[i].year==date.year){


                    PriceFormater.isTimeValieForClick(date.year.toString()+"-"+month+"-"+day)
                    noAvailabilityApi(date.year.toString()+"-"+month+"-"+day)
                }
              }





         /*       if (!holiDayDates.contains(date)) { // all  non  holiday dates
                    if (calender!!.markedDates.all.contains(date)) {
                        for (i in calender!!.markedDates.all.indices) {
                            if (date.year == calender!!.markedDates.all[i].year && date.month == calender!!.markedDates.all[i].month && date.day == calender!!.markedDates.all[i].day) {
                                calender!!.unMarkDate(calender!!.markedDates.all[i])
                              //  hitAddHolidayApi(date.year.toString() + "-" + date.month + "-" + date.day)
                            }
                        }
                    } else {
                        calender!!.markDate(
                            date.setMarkStyle(
                                MarkStyle(
                                    MarkStyle.BACKGROUND,
                                    ContextCompat.getColor(this@ActivityNoAvailability,R.color.colorBlue)
                                )
                            )
                        )
                     //   hitAddHolidayApi(date.year.toString() + "-" + date.month + "-" + date.day)
                    }
                }*/
            }
        })




        binding.prevMonth.setOnClickListener(this)
        binding.nextMonth.setOnClickListener(this)
        currentPage = calender!!.currentItem
        binding.CalenderDays.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBuffalomilk))
        binding.tvMonth.setText("June") // temporary -->
        binding.tvYear.setText("2022")
        calender!!.setOnMonthScrollListener(object : OnMonthScrollListener() {
            override fun onMonthChange(year: Int, month: Int) {
                binding.tvMonth.setText(PriceFormater.monthFormat(month.toString()))
                binding.tvYear.setText(year.toString())
            }

            override fun onMonthScroll(positionOffset: Float) {}
        })
        binding.buttonSeePayroll.setOnClickListener(this)
        binding.buttonAddHoliday.setOnClickListener(this)

        binding.toolbar.tvhead.setText("Details")
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }

    }

/*    fun getSelectedDate(): String {
        val markedDates = calender!!.markedDates
        val arrayList = ArrayList<String?>()
        for (i in markedDates.all.indices) {
            arrayList.add(
                markedDates.all[i].year.toString() + "-" + String.format(
                    "%02d",
                    markedDates.all[i].month
                ) + "-" + String.format("%02d", markedDates.all[i].day)
            )
        }
        return TextUtils.join(",", arrayList)
    }*/

  /*  fun hitAddHolidayApi(date: String?) {
        val hashMap = HashMap<String, String?>()
        hashMap[Constants.kToken] =
            PrefManager.getInstance(this).getUserDetail().getData().getJwtToken()
        hashMap["date"] = date
        hashMap[Constants.staff_id] = staffId
        RestClient.getInst().add_holidays_api(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body().isStatus()) {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }*/



    fun getAllDaysApi() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["package_id"] = subscriptionData.packageDetail.id
        hashMap["subscription_type"] = subscriptionData.subscription
        RestClient.getInst().getAllDays(hashMap).enqueue(object : Callback<ModelDays> {
            override fun onResponse(call: Call<ModelDays>, response: Response<ModelDays>) {
              if (response.body()!!.result) {
                    if (response.body()!!.data.size>0) {
                        for (i in 0 until response.body()!!.data.size) {
                            val pattern = "yyyy-MM-dd"
                            val year: Int =
                                response.body()!!.data[i].delivery_date.substring(0, 4).toInt()
                            val month: Int =
                                response.body()!!.data[i].delivery_date.substring(5, 7).toInt()
                            val date: Int =
                                response.body()!!.data[i].delivery_date.substring(8, 10).toInt()
                            val dateData = DateData(year, month, date)
                            Log.d(
                                "newwDate",
                                dateData.day.toString() + " " + dateData.month + " " + dateData.year
                            )
                            if(response.body()!!.data[i].status=="PLACED"){
                                calender!!.markDate(
                                    dateData.setMarkStyle(
                                        MarkStyle(
                                            MarkStyle.BACKGROUND,
                                            ContextCompat.getColor(this@ActivityNoAvailability,R.color.colorEgg)

                                        )
                                    )
                                )
                            }else if (response.body()!!.data[i].status=="CANCEL"){

                                calender!!.markDate(
                                    dateData.setMarkStyle(
                                        MarkStyle(
                                            MarkStyle.BACKGROUND,
                                            ContextCompat.getColor(this@ActivityNoAvailability,R.color.colorRed)

                                        )
                                    )
                                )
                            }




                        }
                    }
                }
            }

            override fun onFailure(call: Call<ModelDays>, t: Throwable) {}
        })
    }

/*
    fun hit_extra_pament_data() {
        val hashMap = HashMap<String, String?>()
        hashMap[Constants.kToken] =
            PrefManager.getInstance(this).getUserDetail().getData().getJwtToken()
        hashMap["data_g"] = Gson().toJson(staffListAdapter.getDataForPayroll())
        Log.d("asdasd", Gson().toJson(staffListAdapter.getDataForPayroll()))
        hashMap["year_month_pay"] = selectedDate
        RestClient.getInst().extra_pament_data(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {
                if (response.body().isStatus()) {
                    hitApiRunPayroll()
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {}
        })
    }

    fun hitApiRunPayroll() {
        val hashMap = HashMap<String, String?>()
        hashMap[Constants.kToken] =
            PrefManager.getInstance(this).getUserDetail().getData().getJwtToken()
        hashMap["year_month_pay"] = selectedDate
        hashMap[Constants.staff_id] = staffId
        hashMap["ex"] = holiDayDates.size.toString()
        Log.d("fhfghh", holiDayDates.size.toString())
        RestClient.getInst().run_pay_role(hashMap).enqueue(object : Callback<ModelSuccess?> {
            override fun onResponse(call: Call<ModelSuccess?>, response: Response<ModelSuccess?>) {
                if (response.body() != null) {
                    if (response.body().isStatus()) {
                        finish()
                    }
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess?>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }
*/


    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        expand()
        selectedDate = year.toString() + "-" + String.format("%02d", month)
        totalHolidayInAMonth = getTotalMonthHolidays(month, year)
        Log.d("asdasd", totalHolidayInAMonth.toString())
    }

    fun getTotalMonthHolidays(month: Int, year: Int): Int {
        var count = 0
        for (i in holiDayDates.indices) {
            if (holiDayDates[i].month == month && holiDayDates[i].year == year) {
                ++count
            }
        }
        return count
    }

    // This takes a 1-based month, e.g. January=1. If you want to use a 0-based
    // month, remove the "- 1" later on.
    fun getAllWeekendHolidays(year: Int, month: Int, holidays: String): Int {
        val holidaysOFStaffinDays = Arrays.asList(*holidays.split("\\s*,\\s*").toTypedArray())
        val calendar = Calendar.getInstance()
        calendar[year, month - 1] = 1
        val daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        var count = 0
        for (day in 1..daysInMonth) {
            calendar[year, month - 1] = day
            val dayOfWeek = calendar[Calendar.DAY_OF_WEEK]
            for (i in holidaysOFStaffinDays.indices) {
                if (dayOfWeek == holidaysOFStaffinDays[i].toInt()) {
                    Log.d("vrhy", dayOfWeek.toString())
                    Log.d(
                        "qqqqqq",
                        calendar[Calendar.YEAR].toString() + "::" + month + "::" + calendar[Calendar.DATE]
                    )
                    val dateData = DateData(
                        calendar[Calendar.YEAR], month,
                        calendar[Calendar.DATE]
                    )
                    holiDayDates.add(dateData)
                    calender!!.markDate(
                        dateData.setMarkStyle(
                            MarkStyle(
                                MarkStyle.BACKGROUND,
                                ContextCompat.getColor(this@ActivityNoAvailability,R.color.colorEgg)

                            )
                        )
                    )
                    count++
                    // Or do whatever you need to with the result.
                    break
                }
            }
        }
        return count
    }



    private fun noAvailabilityApi(date: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["cancel_date"] = date
        hashMap["package_id"] = subscriptionData.packageDetail.id
        hashMap["subscription_type"] = subscriptionData.subscription

        RestClient.getInst().noAVailabilityApi(hashMap).enqueue(object : Callback<ModelSuccess?> {
            override fun onResponse(
                call: Call<ModelSuccess?>?,
                response: Response<ModelSuccess?>
            ) {
                if (response.body()!!.result) {
               makeToast("Cancelled on Date "+date)

                    val year: Int =
                        date.substring(0, 4).toInt()
                    val month: Int =
                        date.substring(5, 7).toInt()
                    val date: Int =
                        date.substring(8, 10).toInt()
                    val dateData = DateData(year, month, date)
                    calender!!.unMarkDate(dateData)
                    calender!!.markDate(

                        dateData.setMarkStyle(
                            MarkStyle(
                                MarkStyle.BACKGROUND,
                                ContextCompat.getColor(this@ActivityNoAvailability,R.color.colorRed)

                            )
                        )
                    )
                } else {
                }
            }
            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {
            }
        })
    }
}