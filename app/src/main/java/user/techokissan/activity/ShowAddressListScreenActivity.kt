package user.techokissan.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.adapter.AddressListAdapter
import user.techokissan.databinding.ActivityShowAddressListScreenBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.OrderPlacesbean
import user.techokissan.models.UserAddressDataListRes
import user.techokissan.models.UserAddressListRes
import user.techokissan.networking.RestClient
import java.util.HashMap

class ShowAddressListScreenActivity : AppCompatActivity()  , AddressListAdapter.OnRemoveAddressListener{
    private lateinit var binding: ActivityShowAddressListScreenBinding
    private lateinit var notificationAdapter : AddressListAdapter
    private lateinit var dialog : Dialog
    private lateinit var removeList: ArrayList<UserAddressDataListRes>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  ActivityShowAddressListScreenBinding.inflate(layoutInflater)
        val view  =binding.root
        setContentView(view)
        dialog = Dialog(this)
        removeList = ArrayList()
        binding.toolbar.setOnClickListener{
            onBackPressed()
        }

        println("==>"+intent.getStringExtra("itemData"))

        binding.buttonSignIn.setOnClickListener{
            val intet = Intent(this , AddAddressScreenActivity::class.java)
            intet.putExtra("itemData" , intent.getStringExtra("itemData"))
            intet.putExtra("order_amt" , intent.getStringExtra("order_amt"))
            intet.putExtra("total_amt" , intent.getStringExtra("total_amt"))
             startActivity(intet)

        }



    }

    override fun onResume() {
        super.onResume()
        LIST_ADDRESS()
    }

    private fun LIST_ADDRESS() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        RestClient.getInst().LIST_ADDRESS(hashMap).enqueue(object :
            Callback<UserAddressListRes?> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<UserAddressListRes?>?,
                response: Response<UserAddressListRes?>
            ) {
                if (response.body()!!.result) {
                    removeList.clear()
                    removeList.addAll(response.body()!!.data)
                    if (removeList.size > 0 ){
                        binding.recyclerview.visibility = View.VISIBLE
                        binding.mNoFoundTV.visibility = View.GONE
                        binding.recyclerview.apply {
                            visibility = View.VISIBLE
                            notificationAdapter = AddressListAdapter(
                                this@ShowAddressListScreenActivity, removeList
                            )
                            val popularLayout = LinearLayoutManager(
                                this@ShowAddressListScreenActivity,
                                LinearLayoutManager.VERTICAL,
                                false
                            )
                            layoutManager = popularLayout
                            itemAnimator = DefaultItemAnimator()
                            adapter = notificationAdapter
                        }
                        notificationAdapter.notifyDataSetChanged()
                    }else{
                        binding.recyclerview.visibility = View.GONE
                        binding.mNoFoundTV.visibility = View.VISIBLE
                    }

                } else {
                }
            }

            override fun onFailure(call: Call<UserAddressListRes?>?, t: Throwable) {

            }
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onRemoveClick(data: ArrayList<UserAddressDataListRes>, position: Int) {
        dialog.setContentView(R.layout.item_list_remove_address)
        val ok = dialog.findViewById<TextView>(R.id.ok)
        val cancel = dialog.findViewById<TextView>(R.id.cancel)
        ok.setOnClickListener {
            callRemoveAddressApi(data , position)
        }
        cancel.setOnClickListener {
            dialog.dismiss()

        }
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
    }

    override fun onCheckClick(data: ArrayList<UserAddressDataListRes>, position: Int) {
        var intent=Intent()
        Log.d("asdasdasd",data[position].id)
        intent.putExtra("id",data[position].id)
        intent.putExtra("contact",data[position].contact_person_mobile)
        intent.putExtra("full_address",data[position].full_address)
        intent.putExtra("phone",data[position].phone)
        setResult(Activity.RESULT_OK,intent)
        finish()

    }


    private fun callRemoveAddressApi(data: java.util.ArrayList<UserAddressDataListRes>, position: Int) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["address_id"] = data[position].id
        RestClient.getInst().DELETE_ADDRESS(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {
                    removeList.removeAt(position)
                    notificationAdapter.notifyItemRemoved(position)
                    notificationAdapter.notifyItemRangeChanged(position, notificationAdapter.itemCount)
                    dialog.dismiss()
                    Toast.makeText(this@ShowAddressListScreenActivity , response.body()!!.message , Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@ShowAddressListScreenActivity , response.body()!!.message , Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }
}