package user.techokissan.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.fitfarm.demo.DateAdapter
import com.google.gson.Gson
import user.techokissan.demo.DayAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.adapter.CustomeDateAdapter
import user.techokissan.base.ApiUtils
import user.techokissan.databinding.ActivitySubscriptionScreenBinding
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SubscriptionScreenActivity : AppCompatActivity(), DateAdapter.OnSubscriptionListener  , DayAdapter.OnWeeklyCountListener , CustomeDateAdapter.OnDateListener{
    private lateinit var binding: ActivitySubscriptionScreenBinding
    private lateinit var notificationAdapter: DateAdapter
    private lateinit var mCustomDateAdapter : CustomeDateAdapter
    private lateinit var dayAdapter: DayAdapter
    private lateinit var subscriptionTypeModel: ArrayList<SubscriptionTypeModel>
    private lateinit var subscriptionDaysTypeModel: ArrayList<SubscriptionDaysTypeModel>
    private var mSubscriptionType =""
    private var mSlotDate =""
    private var mPackageID =""
    private var mProductID =""
    private var mPriceID =""
    private var mQty =""
    private var totalSubscriptionAmoiunt=""
    private var data : String? = null
    private var isAddressSelected=false
    lateinit var packageDetails:ProductPackageRes
    private lateinit var customData: ArrayList<CustomData>
    lateinit var address:UserAddressDataListRes
    private lateinit var mCustomDateList: ArrayList<CustomDate>

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubscriptionScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        subscriptionTypeModel = ArrayList()
        subscriptionDaysTypeModel = ArrayList()
        mCustomDateList = ArrayList()
        customData = ArrayList()




        subscriptionTypeModel.add(SubscriptionTypeModel(1, "EveryDay"))
        subscriptionTypeModel.add(SubscriptionTypeModel(2, "Weekly"))
        subscriptionTypeModel.add(SubscriptionTypeModel(3, "Alternate Day"))


        subscriptionDaysTypeModel.add(SubscriptionDaysTypeModel(1, "Monday", 0))
        subscriptionDaysTypeModel.add(SubscriptionDaysTypeModel(2, "Tuesday", 0))
        subscriptionDaysTypeModel.add(SubscriptionDaysTypeModel(3, "Wednesday", 0))
        subscriptionDaysTypeModel.add(SubscriptionDaysTypeModel(4, "Thursday", 0))
        subscriptionDaysTypeModel.add(SubscriptionDaysTypeModel(5, "Friday", 0))
        subscriptionDaysTypeModel.add(SubscriptionDaysTypeModel(6, "Saturday", 0))
        subscriptionDaysTypeModel.add(SubscriptionDaysTypeModel(7, "Sunday", 0))

        var mMorTime: String
        val sdf = SimpleDateFormat("EEEE yyyy-MM-dd")
        mCustomDateList.clear()
        for (i in 0..6) {
            val calendar: Calendar = GregorianCalendar()
            calendar.add(Calendar.DATE, i)
            val day: String = sdf.format(calendar.time)
            mMorTime = day
            val mSplitsKey = " "
            val list = mMorTime.split(mSplitsKey)
            mCustomDateList.add(CustomDate(list[0], list[1]))
        }
        dialogPopUp(mCustomDateList)




        binding.WalletRV.visibility = View.VISIBLE
        //  binding.mNoDataFoundTV.visibility = View.GONE
        binding.WalletRV.apply {
            visibility = View.VISIBLE
            notificationAdapter =
                DateAdapter(this@SubscriptionScreenActivity, subscriptionTypeModel)
            val popularLayout = LinearLayoutManager(
                this@SubscriptionScreenActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            layoutManager = popularLayout
            itemAnimator = DefaultItemAnimator()
            adapter = notificationAdapter
        }
        notificationAdapter.notifyDataSetChanged()




        PRODUCTS_DETAILS(intent.getStringExtra("productID")!!)
        binding.toolbar.setOnClickListener {
            onBackPressed()
        }


        binding.mBtnCV.setOnClickListener{
            if (TextUtils.isEmpty(mSubscriptionType)){
                ApiUtils.showToast(this , "Choose Subscription Type")
            }else  if (TextUtils.isEmpty(mQty)){
                ApiUtils.showToast(this , "Choose Any quantity")
            }
            else  if (TextUtils.isEmpty(mSlotDate)){
                ApiUtils.showToast(this , "Choose Any Date")
            }else{



                if (isAddressSelected) {


                    customData.add(CustomData(intent.getStringExtra("productID").toString(),mPackageID ,mQty))
                    customData.add(CustomData(intent.getStringExtra("productID").toString(),mPackageID ,"1"))
                    data  = Gson().toJson(customData)
                    val intent = Intent(this ,ActivityFinalPayment::class.java)
                    intent.putExtra("type" , "subscription")
                    intent.putExtra("mSlotDate" , mSlotDate)
                    intent.putExtra("mSubscriptionType" , mSubscriptionType)
                    intent.putExtra("mQty" , mQty)
                    intent.putExtra("mPackageID" , mPackageID)
                    intent.putExtra("order_amt" , totalSubscriptionAmoiunt)
                    intent.putExtra("total_amt" , totalSubscriptionAmoiunt)


                    intent.putExtra("items" , data)
                    intent.putExtra("addressID",address.id)
                    startActivity(intent)

                }else {

                    val intent = Intent(this ,ShowAddressListScreenActivity::class.java)

                    startActivityForResult(intent,110)

                }

            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==110 && resultCode== Activity.RESULT_OK){

            isAddressSelected=true
            address= UserAddressDataListRes(data!!.getStringExtra("id").toString(),"","","","","","","","","",data!!.getStringExtra("contact").toString(),"","",data!!.getStringExtra("phone").toString(),"","",data!!.getStringExtra("full_address").toString())
            Log.d("asdasdasdasdasd",address.id)
            binding.tvContactNumber.text=address.phone
            binding.tvaddressFull.text=address.full_address
            binding.selectedAddressLayout.visibility=View.VISIBLE
        }
    }

    private fun dialogPopUp(mCustomDateList: java.util.ArrayList<CustomDate>) {
        binding.mDateRV.apply {
            mCustomDateAdapter = CustomeDateAdapter(this@SubscriptionScreenActivity, mCustomDateList)
            val popularLayout = LinearLayoutManager(
               this@SubscriptionScreenActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            layoutManager = popularLayout
            itemAnimator = DefaultItemAnimator()
            adapter = mCustomDateAdapter
        }
        mCustomDateAdapter.notifyDataSetChanged()
    }


    private fun PRODUCTS_DETAILS(productID: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["productID"] = productID
        RestClient.getInst().PRODUCTS_DETAILS(hashMap).enqueue(object :
            Callback<ProductDetailsRes?> {
            @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
            override fun onResponse(
                call: Call<ProductDetailsRes?>?,
                response: Response<ProductDetailsRes?>
            ) {
                if (response.body()!!.result) {
                    Glide.with(this@SubscriptionScreenActivity)
                        .load(response.body()!!.data.product_image).into(binding.restroImage)
                    binding.title.text = response.body()!!.data.product_name
                    mPackageID = response.body()!!.data.id

                    binding.price.text = "₹ " + response.body()!!.data.price
                    binding.mrp.text =  "Mrp ₹ " + response.body()!!.data.mrp
                    
                    binding.mrp.paintFlags = binding.mrp.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    // binding.mrp.text ="Mrp ₹ "+ response.body()!!.data.mrp
                    binding.WalletRV.visibility = View.VISIBLE

                    
                    
                    
               packageDetails=     response.body()!!.data.mPackage[intent.getStringExtra("packageIndex")!!.toInt()]
                 binding.tvSubscriptionTitle.text=packageDetails.name

                    val a = Math.round(packageDetails.amount.toDouble()/packageDetails.qty.toInt()).toInt()
                    binding.tvSubscriptiponPageSubHead.text =packageDetails.qty+" packs @ ₹ "+a+"/pk"
                    mPriceID =packageDetails.amount


                    //  binding.mNoDataFoundTV.visibility = View.GONE
                } else {
                }
            }

            override fun onFailure(call: Call<ProductDetailsRes?>?, t: Throwable) {

            }
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onSubscriptionClick(
        position: Int,
        subscriptionTypeModel: ArrayList<SubscriptionTypeModel>
    ) {
         mSubscriptionType = subscriptionTypeModel[position].type
        if(subscriptionTypeModel[position].type.equals("EveryDay", true)) {
            binding.mQtyPerDayLL.visibility = View.GONE
            binding.mDateRV.visibility = View.VISIBLE
            subscriptionDaysTypeModel[position].cartCount = 0
            binding.mCountLL.visibility = View.GONE
            binding.mAddLL.visibility = View.VISIBLE
            binding.mAddLL.setOnClickListener {
                subscriptionDaysTypeModel[position].cartCount++
                mQty = subscriptionDaysTypeModel[position].cartCount.toString()
                calculateTotalSubscritionAmount()


                binding.mCountLL.visibility = View.VISIBLE
                binding.mAddLL.visibility = View.GONE
                binding.mCountTV.text = subscriptionDaysTypeModel[position].cartCount.toString()
                //  addToCart(subscriptionDaysTypeModel[position].cartCount.toString() ,intent.getStringExtra("productID")!! , subscriptionDaysTypeModel[position].id )

            }

            binding.mPlusLL.setOnClickListener {
                subscriptionDaysTypeModel[position].cartCount++
                mQty = subscriptionDaysTypeModel[position].cartCount.toString()
                binding.mCountLL.visibility = View.VISIBLE
                binding.mAddLL.visibility = View.GONE
                binding.mCountTV.text = subscriptionDaysTypeModel[position].cartCount.toString()
                calculateTotalSubscritionAmount()
                // addToCart(subscriptionTypeModel[position].cartCount.toString() ,intent.getStringExtra("productID")!!, subscriptionDaysTypeModel[position].id)
            }

            binding.mMinusLL.setOnClickListener {
                subscriptionDaysTypeModel[position].cartCount--
                mQty = subscriptionDaysTypeModel[position].cartCount.toString()
                calculateTotalSubscritionAmount()
                if (subscriptionDaysTypeModel[position].cartCount > 0) {
                    binding.mCountTV.text = subscriptionDaysTypeModel[position].cartCount.toString()
                    binding.mCountLL.visibility = View.VISIBLE
                    binding.mAddLL.visibility = View.GONE
                    //  addToCart(subscriptionTypeModel[position].cartCount.toString() ,intent.getStringExtra("productID")!! , subscriptionTypeModel[position].id)
                } else {
                    binding.mCountLL.visibility = View.GONE
                    binding.mAddLL.visibility = View.VISIBLE
                    // addToCart(packageDetails.cartCount.toString() ,intent.getStringExtra("productID")!! , subscriptionTypeModel[position].id)
                }
            }
        } else if (subscriptionTypeModel[position].type.equals("Weekly", true)) {
            binding.mQtyPerDayLL.visibility = View.GONE
            binding.mDateRV.visibility = View.VISIBLE
            binding.WalletRV.visibility = View.VISIBLE

            binding.WalletRV.apply {
                visibility = View.VISIBLE
                dayAdapter = DayAdapter(this@SubscriptionScreenActivity, subscriptionDaysTypeModel)
                val popularLayout = LinearLayoutManager(
                    this@SubscriptionScreenActivity,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                layoutManager = popularLayout
                itemAnimator = DefaultItemAnimator()
                adapter = dayAdapter
            }
            dayAdapter.notifyDataSetChanged()
        } else{
            binding.mQtyPerDayLL.visibility = View.GONE
            binding.DysTime.text = "Alternate Days"
            binding.mDateRV.visibility = View.VISIBLE
            binding.WalletRV.visibility = View.VISIBLE
            subscriptionDaysTypeModel[position].cartCount = 0
            binding.mCountLL.visibility = View.GONE
            binding.mAddLL.visibility = View.VISIBLE
            binding.mAddLL.setOnClickListener {
                subscriptionDaysTypeModel[position].cartCount++
                binding.mCountLL.visibility = View.VISIBLE
                binding.mAddLL.visibility = View.GONE
                binding.mCountTV.text = subscriptionDaysTypeModel[position].cartCount.toString()
                //  addToCart(subscriptionDaysTypeModel[position].cartCount.toString() ,intent.getStringExtra("productID")!! , subscriptionDaysTypeModel[position].id )

            }

            binding.mPlusLL.setOnClickListener {
                subscriptionDaysTypeModel[position].cartCount++
                binding.mCountLL.visibility = View.VISIBLE
                binding.mAddLL.visibility = View.GONE
                binding.mCountTV.text = subscriptionDaysTypeModel[position].cartCount.toString()
                // addToCart(subscriptionTypeModel[position].cartCount.toString() ,intent.getStringExtra("productID")!!, subscriptionDaysTypeModel[position].id)
            }

            binding.mMinusLL.setOnClickListener {
                subscriptionDaysTypeModel[position].cartCount--
                if (subscriptionDaysTypeModel[position].cartCount > 0) {
                    binding.mCountTV.text = subscriptionDaysTypeModel[position].cartCount.toString()
                    binding.mCountLL.visibility = View.VISIBLE
                    binding.mAddLL.visibility = View.GONE
                    //  addToCart(subscriptionTypeModel[position].cartCount.toString() ,intent.getStringExtra("productID")!! , subscriptionTypeModel[position].id)
                } else {
                    binding.mCountLL.visibility = View.GONE
                    binding.mAddLL.visibility = View.VISIBLE
                    // addToCart(packageDetails.cartCount.toString() ,intent.getStringExtra("productID")!! , subscriptionTypeModel[position].id)
                }
            }
        }

    }

    override fun onWeekClick(
        position: Int,
        data: ArrayList<SubscriptionDaysTypeModel>
    ) {
        println("==>"+data[position].cartCount)
        mQty = data[position].cartCount.toString()
        calculateTotalSubscritionAmount()
    }

    override fun onDateClick(position: Int, data: ArrayList<CustomDate>) {
        mSlotDate = data[position].date
    }









    fun placeOrderApi() {
       
            val hashMap: HashMap<String, String> = HashMap()
            hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
            hashMap["addressID"] = address.id
            hashMap["order_amt"] = mPriceID
            hashMap["total_amt"] = mPriceID
            hashMap["total_amount"] = mPriceID
            hashMap["items"] = data!!
            hashMap["delivery_date"] = mSlotDate
            hashMap["subscription_type"] = mSubscriptionType
            hashMap["package_id"] = mPackageID




      /*  intent.putExtra("mQty" , mQty)*/


            RestClient.getInst().PLACE_ORDER(hashMap).enqueue(object :
                Callback<OrderPlacesbean?> {
                override fun onResponse(
                    call: Call<OrderPlacesbean?>?,
                    response: Response<OrderPlacesbean?>
                ) {
                    if (response.body()!!.result) {
                        Toast.makeText(this@SubscriptionScreenActivity , "Subscribed Successfully" , Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@SubscriptionScreenActivity , ActivityDashboard::class.java)
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(this@SubscriptionScreenActivity , "Failed" , Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<OrderPlacesbean?>?, t: Throwable) {

                }
            })
    
        }

    fun calculateTotalSubscritionAmount(){
        totalSubscriptionAmoiunt=(mPriceID.toString().toDouble()*mQty.toString().toDouble()).toString()
    }

    }
