package user.techokissan.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import com.google.gson.Gson
import user.techokissan.demo.CartAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseActivity
import user.techokissan.databinding.ActivityCartScreenBinding
import user.techokissan.models.*
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import java.util.HashMap

class CartScreenActivity : BaseActivity(), CartAdapter.OnRemoveCartListener {
    private var isAddressSelected = false
    lateinit var address: UserAddressDataListRes
    private lateinit var binding: ActivityCartScreenBinding
    private lateinit var notificationAdapter: CartAdapter
    private lateinit var dialog: Dialog
    private lateinit var removeList: ArrayList<CartListDataRes>
    private lateinit var customData: ArrayList<CustomData>
    private var data: String? = null
    private var orderType = ""
    private var order_amt: String? = null
    private var total_amt: String? = null

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCartScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        dialog = Dialog(this)
        removeList = ArrayList()
        customData = ArrayList()
        cartList(true)

        binding.toolbar.setOnClickListener {
            onBackPressed()
        }

        binding.tvChangeAddress.setOnClickListener {
            val intet = Intent(this, ShowAddressListScreenActivity::class.java)
            startActivityForResult(intet, 110)

        }

        binding.mBtnNext.setOnClickListener {
            if (removeList.size > 0) {
                if (isAddressSelected) {
                    //placeOrderApi()


                    startActivity(
                        IntentHelper.getPaymentScreen(this).putExtra("addressID", address.id)
                            .putExtra("order_amt", order_amt.toString())
                            .putExtra("total_amt", total_amt.toString())
                            .putExtra("total_amount", total_amt.toString())
                            .putExtra("items", data!!)
                            .putExtra("type", "orderPlace")
                    )
             /*       if (orderType.equals("a")) {
                        startActivity(
                            IntentHelper.getPaymentScreen(this).putExtra("addressID", address.id)
                                .putExtra("order_amt", order_amt.toString())
                                .putExtra("total_amt", total_amt.toString())
                                .putExtra("total_amount", total_amt.toString())
                                .putExtra("items", data!!)
                        )

                    } else {
                        startActivity(
                            IntentHelper.getPaymentScreen(this).putExtra("addressID", address.id)
                                .putExtra("order_amt", order_amt.toString())
                                .putExtra("total_amt", total_amt.toString())
                                .putExtra("total_amount", total_amt.toString())
                                .putExtra("items", data!!)

                                .putExtra(
                                    "mSlotDate",
                                    intent.getStringExtra("mSlotDate").toString()
                                )
                                .putExtra(
                                    "mSubscriptionType",
                                    intent.getStringExtra("mSubscriptionType").toString()
                                )
                                .putExtra(
                                    "package_id",
                                    intent.getStringExtra("package_id").toString()
                                )
                        )

                    }*/


                } else {
                    val intet = Intent(this, ShowAddressListScreenActivity::class.java)
                    startActivityForResult(intet, 110)
                }
            } else {
                makeToast("Please Add Items to Proceed")
            }
        }
    }

    private fun cartList(freshApi: Boolean) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        RestClient.getInst().USER_CART(hashMap).enqueue(object :
            Callback<CartListRes?> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CartListRes?>?,
                response: Response<CartListRes?>
            ) {
                if (response.body()!!.result) {
                    if (freshApi) {
                        removeList.clear()
                        removeList.addAll(response.body()!!.data)
                        total_amt = response.body()!!.total_amount
                        order_amt = response.body()!!.order_amt
                        if (removeList.size > 0) {
                            binding.mShowWalletHistoryLL.visibility = View.VISIBLE
                            binding.totalAmount.text = "₹ " + response.body()!!.total_amount
                            binding.mNoDataFoundTV.visibility = View.GONE
                            for (i in 0 until removeList.size) {
                                customData.add(
                                    CustomData(
                                        removeList[i].id,
                                        "",
                                        removeList[i].cart_qty
                                    )
                                )
                            }
                            data = Gson().toJson(customData)
                            println("==>" + Gson().toJson(customData))
                            println("==>" + data)
                            /*val mError: CustomData = Gson().fromJson(customData.toString(), CustomData::class.java)
                            println("==>$mError")*/


                            binding.WalletRV.apply {
                                visibility = View.VISIBLE
                                notificationAdapter =
                                    CartAdapter(this@CartScreenActivity, removeList)
                                val popularLayout = LinearLayoutManager(
                                    this@CartScreenActivity,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                                layoutManager = popularLayout
                                itemAnimator = DefaultItemAnimator()
                                adapter = notificationAdapter
                            }
                            notificationAdapter.notifyDataSetChanged()
                        } else {
                            binding.mShowWalletHistoryLL.visibility = View.GONE
                            binding.mNoDataFoundTV.visibility = View.VISIBLE
                        }
                    } else {


                        total_amt = response.body()!!.total_amount
                        order_amt = response.body()!!.order_amt
                        binding.totalAmount.text = "₹ " + response.body()!!.total_amount

                        if (response.body()!!.data.size != 0) {
                            binding.mShowWalletHistoryLL.visibility = View.VISIBLE
                            binding.mNoDataFoundTV.visibility = View.GONE
                        } else {
                            binding.mShowWalletHistoryLL.visibility = View.GONE
                            binding.mNoDataFoundTV.visibility = View.VISIBLE
                        }


                    }
                } else {
                }
            }

            override fun onFailure(call: Call<CartListRes?>?, t: Throwable) {

            }
        })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 110 && resultCode == Activity.RESULT_OK) {

            isAddressSelected = true
            address = UserAddressDataListRes(
                data!!.getStringExtra("id").toString(),
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                data!!.getStringExtra("contact").toString(),
                "",
                "",
                data!!.getStringExtra("phone").toString(),
                "",
                "",
                data!!.getStringExtra("full_address").toString()
            )
            Log.d("asdasdasdasdasd", address.id)
            binding.tvContactNumber.text = address.phone
            binding.tvaddressFull.text = address.full_address
            binding.selectedAddressLayout.visibility = View.VISIBLE
        }
    }

    override fun onClick(viewId: Int, view: View?) {

    }


    override fun onRemoveClick(data: ArrayList<CartListDataRes>, position: Int) {
        /*  if (data.size > 0 ){
              binding.mShowWalletHistoryLL.visibility = View.VISIBLE
              binding.mNoDataFoundTV.visibility = View.GONE*/
        dialog.setContentView(R.layout.item_list_remove_address)
        val ok = dialog.findViewById<TextView>(R.id.ok)
        val cancel = dialog.findViewById<TextView>(R.id.cancel)
        ok.setOnClickListener {
            addToCart("0", data, position)
        }
        cancel.setOnClickListener {
            dialog.dismiss()

        }
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        /*}else{
            binding.mShowWalletHistoryLL.visibility = View.GONE
            binding.mNoDataFoundTV.visibility = View.VISIBLE
        }*/

    }

    private fun addToCart(qty: String, productID: ArrayList<CartListDataRes>, position: Int) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["productID"] = productID[position].id
        hashMap["qty"] = qty
        RestClient.getInst().ADD_PRODUCT(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {
                    removeList.removeAt(position)
                    notificationAdapter.notifyItemRemoved(position)
                    notificationAdapter.notifyItemRangeChanged(
                        position,
                        notificationAdapter.itemCount
                    )
                    dialog.dismiss()
                    Toast.makeText(
                        this@CartScreenActivity,
                        response.body()!!.message,
                        Toast.LENGTH_SHORT
                    ).show()
                    cartList(false)
                } else {
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }


    fun placeOrderApi() {
        if (intent.getStringExtra("type").equals("subscription", true)) {
            val hashMap: HashMap<String, String> = HashMap()
            hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
            hashMap["addressID"] = address.id
            hashMap["order_amt"] = order_amt.toString()
            hashMap["total_amt"] = total_amt.toString()
            hashMap["total_amount"] = total_amt.toString()
            hashMap["items"] = data!!
            hashMap["delivery_date"] = intent.getStringExtra("mSlotDate").toString()
            hashMap["subscription_type"] = intent.getStringExtra("mSubscriptionType").toString()
            hashMap["package_id"] = intent.getStringExtra("mPackageID").toString()







            RestClient.getInst().PLACE_ORDER(hashMap).enqueue(object :
                Callback<OrderPlacesbean?> {
                override fun onResponse(
                    call: Call<OrderPlacesbean?>?,
                    response: Response<OrderPlacesbean?>
                ) {
                    if (response.body()!!.result) {
                        Toast.makeText(
                            this@CartScreenActivity,
                            "Subscribed Successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent(this@CartScreenActivity, ActivityDashboard::class.java)
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(this@CartScreenActivity, "Failed", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<OrderPlacesbean?>?, t: Throwable) {

                }
            })
        } else {
            val hashMap: HashMap<String, String> = HashMap()
            hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
            hashMap["addressID"] = address.id
            hashMap["order_amt"] = total_amt.toString()
            hashMap["total_amt"] = total_amt.toString()
            hashMap["total_amount"] = total_amt.toString()
            hashMap["items"] = data!!
            hashMap["delivery_date"] = ""
            hashMap["city_id"] = "1"

            hashMap["subscription_type"] = ""
            hashMap["package_id"] = "0"
            RestClient.getInst().PLACE_ORDER(hashMap).enqueue(object :
                Callback<OrderPlacesbean?> {
                override fun onResponse(
                    call: Call<OrderPlacesbean?>?,
                    response: Response<OrderPlacesbean?>
                ) {
                    if (response.body()!!.result) {
                        Toast.makeText(this@CartScreenActivity, "Order Places", Toast.LENGTH_SHORT)
                            .show()
                        val intent = Intent(this@CartScreenActivity, ActivityDashboard::class.java)
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(this@CartScreenActivity, "Order Failed", Toast.LENGTH_SHORT)
                            .show()
                    }
                }

                override fun onFailure(call: Call<OrderPlacesbean?>?, t: Throwable) {

                }
            })
        }

    }
}