package user.techokissan.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.adapter.OrderDetailsShowAdapter
import user.techokissan.databinding.ActivityOrderDetailsScreenBinding
import user.techokissan.models.ModelSuccess
import user.techokissan.models.OrderHistoryDataRes
import user.techokissan.models.OrderItemsList
import user.techokissan.networking.RestClient

class
OrderDetailsScreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOrderDetailsScreenBinding

    private  var position = 0
    private lateinit var data: ArrayList<OrderHistoryDataRes>
    private lateinit var orderDetailsShowAdapter   : OrderDetailsShowAdapter
  /*  //FOR INTERNET
    private val networkMonitor = NetworkMonitorUtil(this)*/
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  ActivityOrderDetailsScreenBinding.inflate(layoutInflater)
        val view  =binding.root
        setContentView(view)


        position = intent.getIntExtra("position" , -1)
        data = intent.getSerializableExtra("data") as ArrayList<OrderHistoryDataRes>


        callApi()


        if (data[position].status=="PLACED"){
            binding.tvOrderCancel.visibility=View.VISIBLE
        }else{
            binding.tvOrderCancel.visibility=View.GONE
        }
      binding.tvOrderCancel.setOnClickListener {
cancelOrder()
      }





        binding.mBackIV.setOnClickListener{
            onBackPressed()
        }


    }
    override fun onBackPressed() {
        finish()
    }


    @SuppressLint("NotifyDataSetChanged")
    private  fun callApi() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["orderID"] = data[position].id
        RestClient.getInst().ORDER_ITEMS(hashMap).enqueue(object :
            Callback<OrderItemsList?> {
            override fun onResponse(
                call: Call<OrderItemsList?>?,
                response: Response<OrderItemsList?>
            ) {
                if (response.body()!!.result) {
                    if (response.body()!!.data.size > 0){
                            binding.recyclerview.visibility = View.VISIBLE
                            binding.recyclerview.apply {
                                visibility = View.VISIBLE
                                orderDetailsShowAdapter = OrderDetailsShowAdapter(this@OrderDetailsScreenActivity, response.body()!!.data)
                                val popularLayout = LinearLayoutManager(this@OrderDetailsScreenActivity , LinearLayoutManager.VERTICAL, false)
                                layoutManager = popularLayout
                                itemAnimator = DefaultItemAnimator()
                                adapter = orderDetailsShowAdapter
                            }
                            orderDetailsShowAdapter.notifyDataSetChanged()



                    }else{
                        binding.recyclerview.visibility = View.GONE
                    }

                } else {
                }
            }

            override fun onFailure(call: Call<OrderItemsList?>?, t: Throwable) {

            }
        })
    }




    @SuppressLint("NotifyDataSetChanged")
    private  fun cancelOrder() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["order_id"] = data[position].id
        RestClient.getInst().ORDER_CANCEL(hashMap).enqueue(object :
            Callback<ModelSuccess?> {
            override fun onResponse(
                call: Call<ModelSuccess?>?,
                response: Response<ModelSuccess?>
            ) {
                if (response.body()!!.result) {
                    Toast.makeText(this@OrderDetailsScreenActivity,"Order Cancelled",Toast.LENGTH_SHORT).show()
                  finish()
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }
        })
    }




}