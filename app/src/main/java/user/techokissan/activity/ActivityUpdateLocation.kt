package user.techokissan.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.adapter.CityAdapter
import user.techokissan.adapter.LocalityAdapter
import user.techokissan.adapter.StateAdapter
import user.techokissan.databinding.ActivityUpdateLocationBinding
import user.techokissan.models.*
import user.techokissan.networking.RestClient

class ActivityUpdateLocation : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    lateinit var binding:ActivityUpdateLocationBinding

    private lateinit var cityAdapter: CityAdapter
    private lateinit var stateAdapter: StateAdapter
    private lateinit var localityAdapter: LocalityAdapter

    private lateinit var mStateList: ArrayList<StateListDataRes>
    private lateinit var mCitiesList: ArrayList<CityListDataRes>
    private lateinit var mLocalityList: ArrayList<LocalityListDataRes>

    private var mStateID = ""
    private var mCityID = ""
    private var mStateName = ""
    private var mCityName = ""
    private var mLocalityID = ""
    private var mLocalityName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil. setContentView(this,R.layout.activity_update_location)

        binding.toolbar.tvhead.text="Location"
        binding.toolbar.topAppBar.setNavigationOnClickListener {

        }

        binding.buttonOk.setOnClickListener {
            updateLocationApi()
        }



        mStateList = ArrayList()
        mCitiesList = ArrayList()
        mLocalityList = ArrayList()

        binding.mStateSP.onItemSelectedListener = this
        binding.mCitySP.onItemSelectedListener = this
        binding.mLocalitySP.onItemSelectedListener = this
        callStateApi()
    }


    private fun callStateApi() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        RestClient.getInst().stateList(hashMap).enqueue(object :
            Callback<StateListRes?> {
            override fun onResponse(call: Call<StateListRes?>?, response: Response<StateListRes?>) {
                if (response.body()!!.result) {
                    if (response.body()!!.data.size > 0){
                        mStateList.clear()
                        mStateList.addAll(response.body()!!.data)
                        stateAdapter = StateAdapter(this@ActivityUpdateLocation, mStateList)
                        binding.mStateSP.adapter = stateAdapter
                        stateAdapter.notifyDataSetChanged()
                    }

                    /* for (i in 0 until mCitiesList.size) {
                         if (mCitiesList[i].id.equals(mCityID, true)) {
                             binding.mCitySP.setSelection(i)
                         }
                     }*/
                } else {
                }
            }

            override fun onFailure(call: Call<StateListRes?>?, t: Throwable) {

            }
        })
    }
    private fun callLocalityApi(mStateID: String, mCityID: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["state_id"] = mStateID
        hashMap["city_id"] = mCityID
        RestClient.getInst().LOCALITY(hashMap).enqueue(object :
            Callback<LocalityListRes?> {
            override fun onResponse(call: Call<LocalityListRes?>?, response: Response<LocalityListRes?>) {
                if (response.body()!!.result) {
                    mLocalityList.clear()
                    mLocalityList.addAll(response.body()!!.data)
                    if (mLocalityList.size > 0){
                        localityAdapter = LocalityAdapter(this@ActivityUpdateLocation, mLocalityList)
                        binding.mLocalitySP.adapter = localityAdapter
                        localityAdapter.notifyDataSetChanged()
                    }else{
                        localityAdapter = LocalityAdapter(this@ActivityUpdateLocation, mLocalityList)
                        binding.mLocalitySP.adapter = localityAdapter
                        localityAdapter.notifyDataSetChanged()
                    }

                    /* for (i in 0 until mCitiesList.size) {
                         if (mCitiesList[i].id.equals(mCityID, true)) {
                             binding.mCitySP.setSelection(i)
                         }
                     }*/



                } else {
                }
            }

            override fun onFailure(call: Call<LocalityListRes?>?, t: Throwable) {

            }
        })
    }
    private fun callCityApi(mStateID: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["state_id"] = mStateID
        RestClient.getInst().CITY_LIST(hashMap).enqueue(object :
            Callback<CityListRes?> {
            override fun onResponse(call: Call<CityListRes?>?, response: Response<CityListRes?>) {
                if (response.body()!!.result) {
                    mCitiesList.clear()
                    mCitiesList.addAll(response.body()!!.data)
                    if (mCitiesList.size > 0){
                        cityAdapter = CityAdapter(this@ActivityUpdateLocation, mCitiesList)
                        binding.mCitySP.adapter = cityAdapter
                        cityAdapter.notifyDataSetChanged()
                    }else {
                        cityAdapter = CityAdapter(this@ActivityUpdateLocation, mCitiesList)
                        binding.mCitySP.adapter = cityAdapter
                        cityAdapter.notifyDataSetChanged()
                    }
                } else {
                }
            }

            override fun onFailure(call: Call<CityListRes?>?, t: Throwable) {

            }
        })
    }

    private fun updateLocationApi() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["state_id"] = mStateID
        hashMap["city_id"] = mCityID
        hashMap["locality_id"] = mLocalityID
        RestClient.getInst().updateLocation(hashMap).enqueue(object :
            Callback<ModelSuccess?> {
            override fun onResponse(call: Call<ModelSuccess?>?, response: Response<ModelSuccess?>) {
                if (response.body()!!.result) {
                    finishAtivityWithResult()
                } else {
                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }
        })
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent) {
            binding.mStateSP -> {
                mStateID = mStateList[position].id
                mStateName = mStateList[position].name
                println("==>$mStateID")
                callCityApi(mStateID)
            }
            binding.mCitySP -> {
                mCityID = mCitiesList[position].id
                mCityName = mCitiesList[position].name
                println("==>$mStateID")
                println("==>$mCityID")
                callLocalityApi(mStateID , mCityID)

            }
            binding.mLocalitySP -> {
                mLocalityID = mLocalityList[position].id
                mLocalityName = mLocalityList[position].name
                println("==>$mStateID")
                println("==>$mCityID")
                println("==>$mLocalityID")
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    fun  finishAtivityWithResult(){
        var intent=Intent()
        intent.putExtra("state",mStateName)
        intent.putExtra("city",mCityName)
        intent.putExtra("locality",mLocalityName)
        setResult(Activity.RESULT_OK,intent)
        finish()
    }

}