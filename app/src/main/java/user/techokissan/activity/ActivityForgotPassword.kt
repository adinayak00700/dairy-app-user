package user.techokissan.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.base.BaseActivity
import user.techokissan.databinding.ActivityForgotPasswordBinding
import user.techokissan.models.ModelSuccess
import user.techokissan.models.ModelUser
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper
import user.techokissan.utilities.ValidationHelper
import java.util.HashMap

class ActivityForgotPassword : BaseActivity() {
    var isChangePassword=false
    private lateinit var binding:ActivityForgotPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil. setContentView(this,R.layout.activity_forgot_password)
if (intent.getStringExtra("asd")!=null){
    isChangePassword=true
}

        binding.buttonForgotPassword.setOnClickListener {
           if (isChangePassword){
               changePasswordApi()
           }else{
               if (isValid()){
                   forgotPasswordApi()
               }
           }
        }

        if (isChangePassword){
            binding.buttonForgotPassword.text="Change Password"
            binding.layoutEmail.visibility=View.GONE
            binding.layoutpassword.visibility=View.VISIBLE
        }
    }

    override fun onClick(viewId: Int, view: View?) {

    }


    fun isValid(): Boolean {
        if(ValidationHelper.isNull(binding.etEmail.text.toString())){
            makeToast("Please Enter email")
            return false
        }/*else
            if(ValidationHelper.isNull(binding.etPassword.text.toString())){
                makeToast("Please Enter Password")
                return false
            }*/
        return true;
    }


    fun forgotPasswordApi() {

        val hashMap = HashMap<String, String>()
        hashMap["emailID"] =binding.etEmail.text.toString()
        RestClient.getInst().forgotPassword(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {

                if (response.body()!!.result) {
         /*           PrefManager.getInstance(this@ActivityForgotPassword)!!.userDetail=response.body()!!.data
                    PrefManager.getInstance(this@ActivityForgotPassword)!!.keyIsLoggedIn = true
                    startActivity(IntentHelper.getDashboardActivity(context))*/
                    makeToast("A new password has been sent to you email address")
                    finish()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }

    fun changePasswordApi() {

        val hashMap = HashMap<String, String>()
        hashMap["new_password"] =binding.etPassword.text.toString()
        RestClient.getInst().changePaassword(hashMap).enqueue(object : Callback<ModelSuccess> {
            override fun onResponse(call: Call<ModelSuccess>, response: Response<ModelSuccess>) {

                if (response.body()!!.result) {
                    /*           PrefManager.getInstance(this@ActivityForgotPassword)!!.userDetail=response.body()!!.data
                               PrefManager.getInstance(this@ActivityForgotPassword)!!.keyIsLoggedIn = true
                               startActivity(IntentHelper.getDashboardActivity(context))*/
                    finish()
                } else {
                    makeToast(response.body()!!.message)
                }
            }

            override fun onFailure(call: Call<ModelSuccess>, t: Throwable) {
                makeToast(t.message)
            }
        })
    }
}