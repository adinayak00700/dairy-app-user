package user.techokissan.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View


import com.example.mechanicforyoubusiness.utilities.PrefManager
import user.techokissan.R
import user.techokissan.base.BaseActivity
import user.techokissan.utilities.IntentHelper

class ActivitySplash : BaseActivity() {

    lateinit var context: ActivitySplash
    lateinit var prefManager: PrefManager
    val SPLASH_DURATION: Long = 2000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        context = this
        prefManager = PrefManager(context)
        Handler(Looper.getMainLooper()).postDelayed({
            if (prefManager.keyIsLoggedIn) {
                startActivity(IntentHelper.getDashboardActivity(context))
            } else {
                startActivity(IntentHelper.getLoginScreen(context))
            }
            finish()
        }, SPLASH_DURATION)
    }

    override fun onClick(viewId: Int, view: View?) {

    }
}