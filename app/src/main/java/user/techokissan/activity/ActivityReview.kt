package user.techokissan.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import user.techokissan.R
import user.techokissan.databinding.ActivityReviewBinding
import user.techokissan.fragment.FragmentProductList
import user.techokissan.fragment.FragmentReviews
import java.lang.Exception

class ActivityReview : AppCompatActivity() {
    private lateinit var binding:ActivityReviewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_review)
        binding.toolbar.tvhead.text="Reviews"
        binding.toolbar.topAppBar.setNavigationOnClickListener {
            finish()
        }

        loadFragment(FragmentReviews.newInstance(
            intent.getStringExtra("productId").toString(), intent.getStringExtra("type").toString()
        ))


    }


    var backStateName=""
    fun loadFragment(fragment: Fragment) {
        backStateName = fragment.javaClass.simpleName
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.getPrimaryNavigationFragment()
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        var fragmentTemp: Fragment? = mFragmentManager.findFragmentByTag(backStateName)
        if (fragmentTemp == null) {
            fragmentTemp = fragment
            fragmentTransaction.add(binding!!.frame.id, (fragmentTemp)!!, backStateName)
        } else {
            fragmentTransaction.show(fragmentTemp)
        }
        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.commitNowAllowingStateLoss()
    }

    override fun onResume() {
        super.onResume()

        try {
            var fragmnt=supportFragmentManager.findFragmentById(R.id.frame) as FragmentProductList
            fragmnt.getCartCount()
        }catch (e:Exception){

        }
    }

}