package user.techokissan.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.adapter.CityAdapter
import user.techokissan.adapter.LocalityAdapter
import user.techokissan.adapter.StateAdapter
import user.techokissan.databinding.ActivityAddAddressScreenBinding
import user.techokissan.models.*
import user.techokissan.networking.RestClient

class AddAddressScreenActivity : AppCompatActivity(), View.OnClickListener , AdapterView.OnItemSelectedListener {
    private lateinit var binding: ActivityAddAddressScreenBinding
    private var mAddressType =""

    // adapter
    private lateinit var cityAdapter: CityAdapter
    private lateinit var stateAdapter: StateAdapter
    private lateinit var localityAdapter: LocalityAdapter

    private lateinit var mStateList: ArrayList<StateListDataRes>
    private lateinit var mCitiesList: ArrayList<CityListDataRes>
    private lateinit var mLocalityList: ArrayList<LocalityListDataRes>

    private var mStateID = ""
    private var mCityID = ""
    private var mStateName = ""
    private var mCityName = ""
    private var mLocalityID = ""
    private var mLocalityName = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddAddressScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.buttonSignIn.setOnClickListener(this)
        binding.mHomeLL.setOnClickListener(this)
        binding.mOtherLL.setOnClickListener(this)
        binding.mWorkLL.setOnClickListener(this)
        mStateList = ArrayList()
        mCitiesList = ArrayList()
        mLocalityList = ArrayList()
        binding.mStateSP.onItemSelectedListener = this
        binding.mCitySP.onItemSelectedListener = this
        binding.mLocalitySP.onItemSelectedListener = this
        binding.toolbar.setOnClickListener{
            onBackPressed()
        }

        callStateApi()
    }



    override fun onClick(p0: View?) {
        when (p0) {
            binding.buttonSignIn -> {
                if(TextUtils.isEmpty(binding.mNameET.text.toString())) {
                    binding.mNameET.requestFocus()
                    binding.mNameET.error = "Please Customer Name"
                } else if (TextUtils.isEmpty(binding.mPincodeET.text.toString())) {
                    binding.mPincodeET.requestFocus()
                    binding.mPincodeET.error = "PinCode Here"
                } else if (TextUtils.isEmpty(mAddressType)) {
                    Toast.makeText(this , "Choose Address Type" , Toast.LENGTH_SHORT).show()
                } else if (TextUtils.isEmpty(binding.mPhoneET.text.toString())) {
                    binding.mPhoneET.requestFocus()
                    binding.mPhoneET.error = "Please Phone No."
                } else {

                    callApi()
                }

            }
            binding.mHomeLL -> {
                mAddressType = "Home"
                binding.mHomeLL.isSelected = true
                binding.mOtherLL.isSelected = false
                binding.mWorkLL.isSelected = false
                binding.mSingleTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.white
                    )
                )
                binding.mWorkTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )
                binding.mOtherTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )


                // binding.mHomeLL.setBackgroundDrawable(R.drawable.address_type_bg)
            }
            binding.mOtherLL -> {
                mAddressType = "Other"
                binding.mOtherLL.isSelected = false
                binding.mOtherLL.isSelected = true
                binding.mWorkLL.isSelected = false

                binding.mSingleTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )
                binding.mWorkTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )
                binding.mOtherTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.white
                    )
                )
            }
            binding.mWorkLL -> {
                mAddressType = "Work"
                binding.mHomeLL.isSelected = false
                binding.mOtherLL.isSelected = false
                binding.mWorkLL.isSelected = true

                binding.mSingleTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )
                binding.mWorkTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.white
                    )
                )
                binding.mOtherTV.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )
            }
        }
    }

    private fun callApi() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["flat_no"] = binding.mFlatNoET.text.toString()
        hashMap["building_name"] = binding.mBuildingNameET.text.toString()
        hashMap["landmark"] = binding.mLandMarkET.text.toString()
        hashMap["state_id"] = ""
        hashMap["city_id"] = ""
        hashMap["locality_id"] = ""
        hashMap["pin_code"] = binding.mPincodeET.text.toString()
        hashMap["full_address"] = binding.etFullAddress.text.toString()
        hashMap["phone"] = binding.mPhoneET.text.toString()
        hashMap["type"] = mAddressType

        RestClient.getInst().ADD_ADDRESS(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {
            finish()


              /*       binding.mFlatNoET.setText("")
                     binding.mBuildingNameET.setText("")
                     binding.mLandMarkET.setText("")
                     binding.mPincodeET.setText("")
                     binding.mPhoneET.setText("")
                     binding.mNameET.setText("")
                    binding.mOtherLL.isSelected = false
                    binding.mHomeLL.isSelected = false
                    binding.mWorkLL.isSelected = false
                    binding.mSingleTV.setTextColor(
                        ContextCompat.getColor(
                            this@AddAddressScreenActivity,
                            R.color.black
                        )
                    )
                    binding.mWorkTV.setTextColor(
                        ContextCompat.getColor(
                            this@AddAddressScreenActivity,
                            R.color.black
                        )
                    )
                    binding.mOtherTV.setTextColor(
                        ContextCompat.getColor(
                            this@AddAddressScreenActivity,
                            R.color.black
                        )
                    )*/
                     Toast.makeText(this@AddAddressScreenActivity , response.body()!!.message , Toast.LENGTH_SHORT).show()
                } else {
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent) {
            binding.mStateSP -> {
                  mStateID = mStateList[position].id
                  mStateName = mStateList[position].name
                  println("==>$mStateID")
                  callCityApi(mStateID)
            }
            binding.mCitySP -> {
                mCityID = mCitiesList[position].id
                mCityName = mCitiesList[position].name
                println("==>$mStateID")
                println("==>$mCityID")
                callLocalityApi(mStateID , mCityID)

            }
            binding.mLocalitySP -> {
                mLocalityID = mLocalityList[position].id
                mLocalityName = mLocalityList[position].name
                println("==>$mStateID")
                println("==>$mCityID")
                println("==>$mLocalityID")
            }
        }

    }



    override fun onNothingSelected(p0: AdapterView<*>?) {

    }
    private fun callStateApi() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        RestClient.getInst().stateList(hashMap).enqueue(object :
            Callback<StateListRes?> {
            override fun onResponse(call: Call<StateListRes?>?, response: Response<StateListRes?>) {
                if (response.body()!!.result) {
                    if (response.body()!!.data.size > 0){
                        mStateList.clear()
                        mStateList.addAll(response.body()!!.data)
                        stateAdapter = StateAdapter(this@AddAddressScreenActivity, mStateList)
                        binding.mStateSP.adapter = stateAdapter
                        stateAdapter.notifyDataSetChanged()
                    }

                    /* for (i in 0 until mCitiesList.size) {
                         if (mCitiesList[i].id.equals(mCityID, true)) {
                             binding.mCitySP.setSelection(i)
                         }
                     }*/
                } else {
                }
            }

            override fun onFailure(call: Call<StateListRes?>?, t: Throwable) {

            }
        })
    }
    private fun callLocalityApi(mStateID: String, mCityID: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["state_id"] = mStateID
        hashMap["city_id"] = mCityID
        RestClient.getInst().LOCALITY(hashMap).enqueue(object :
            Callback<LocalityListRes?> {
            override fun onResponse(call: Call<LocalityListRes?>?, response: Response<LocalityListRes?>) {
                if (response.body()!!.result) {
                    mLocalityList.clear()
                    mLocalityList.addAll(response.body()!!.data)
                    if (mLocalityList.size > 0){
                        localityAdapter = LocalityAdapter(this@AddAddressScreenActivity, mLocalityList)
                        binding.mLocalitySP.adapter = localityAdapter
                        localityAdapter.notifyDataSetChanged()
                    }else{
                        localityAdapter = LocalityAdapter(this@AddAddressScreenActivity, mLocalityList)
                        binding.mLocalitySP.adapter = localityAdapter
                        localityAdapter.notifyDataSetChanged()
                    }

                    /* for (i in 0 until mCitiesList.size) {
                         if (mCitiesList[i].id.equals(mCityID, true)) {
                             binding.mCitySP.setSelection(i)
                         }
                     }*/



                } else {
                }
            }

            override fun onFailure(call: Call<LocalityListRes?>?, t: Throwable) {

            }
        })
    }
    private fun callCityApi(mStateID: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(this)!!.userDetail.token
        hashMap["state_id"] = mStateID
        RestClient.getInst().CITY_LIST(hashMap).enqueue(object :
            Callback<CityListRes?> {
            override fun onResponse(call: Call<CityListRes?>?, response: Response<CityListRes?>) {
                if (response.body()!!.result) {
                    mCitiesList.clear()
                    mCitiesList.addAll(response.body()!!.data)
                    if (mCitiesList.size > 0){
                        cityAdapter = CityAdapter(this@AddAddressScreenActivity, mCitiesList)
                        binding.mCitySP.adapter = cityAdapter
                        cityAdapter.notifyDataSetChanged()
                    }else {
                        cityAdapter = CityAdapter(this@AddAddressScreenActivity, mCitiesList)
                        binding.mCitySP.adapter = cityAdapter
                        cityAdapter.notifyDataSetChanged()
                    }
                } else {
                }
            }

            override fun onFailure(call: Call<CityListRes?>?, t: Throwable) {

            }
        })
    }
}