package user.techokissan.utilities;

import java.util.HashMap;
import java.util.TimeZone;


public class UrlFactory {
    private static final String URL_DEV ="https://errorfix.tech/test/api/";     // Development
    private static final String URL_PROD = "https://errorfix.tech/test/api/";   // Live


    public static final String URL_TEST = "https://storypay.appmantra.live/admin/api/";   // Live


    //private static final String URL_DEV ="https://storypay.appmantra.live/admin/api/";     // Development
    private static final String API_VERSION = "api/v0/";
    private static final boolean MODE_DEVELOPMENT = true;

    public static boolean isModeDevelopment() {
        return MODE_DEVELOPMENT;
    }

    public static String getBaseUrl(){

        if(isModeDevelopment()){
            return  URL_DEV;
        }else {
            return URL_PROD;
        }

        //return isModeDevelopment() ? URL_DEV : URL_PROD;
    }

    public static String getBaseUrlWithApiVersioning(){
        return getBaseUrl().concat(API_VERSION);
    }

    private static final String kHeaderDevice = "deviceType";
    private static final String kHeaderAccessToken = "SessionToken";
    private static final String vHeaderDevice = "android";
    private static final String kTimezone = "Timezone";
    private static final String KDeviceToken = "deviceToken";


    public static String generateUrl(String urlSuffix){
        return getBaseUrl().concat(urlSuffix);
    }

    public static String generateUrlWithVersion(String urlSuffix){
        return getBaseUrl().concat(API_VERSION).concat(urlSuffix);
    }



    public static HashMap<String, String> getDefaultHeaders(){
        HashMap<String, String> defaultHeaders = new HashMap<>();
        defaultHeaders.put("Content-Type", "application/json");
        defaultHeaders.put("Accept","application/json");
        defaultHeaders.put(kHeaderDevice, vHeaderDevice);
        defaultHeaders.put(kTimezone, getTimezone());
        defaultHeaders.put("language", "english");
        defaultHeaders.put("AppKey", "Poru13feqwopto2qMpt43");

 /*       defaultHeaders.put("GuestToken", GuestPrefrenceManager.getInstance(App.getInstance()).getGuestToken());
        defaultHeaders.put("currentVersion", BuildConfig.VERSION_NAME);*/
        return defaultHeaders;
    }

    public static HashMap<String, String> getGuestHeaders()   {
        HashMap<String, String> defaultHeaders = new HashMap<>();
        defaultHeaders.put("Content-Type", "application/json");
        defaultHeaders.put(kHeaderDevice, vHeaderDevice);
        defaultHeaders.put(kTimezone, getTimezone());
        defaultHeaders.put("language", "english");
        defaultHeaders.put("AppKey", "Poru13feqwopto2qMpt43");
        return defaultHeaders;
    }

    public static HashMap<String, String> getAppHeaders(){
        HashMap<String, String> appHeaders = getDefaultHeaders();
      //  appHeaders.put(kHeaderAccessToken, App.getInstance().getAccessToken());
        return appHeaders;
    }



    private static String getTimezone() {
        TimeZone tz = TimeZone.getDefault();
        return tz.getID() + "";
    }

    /*public static String getLanguage() {
        if (new LanguagePref(App.getInstance()).getLanguage()!=null && new LanguagePref(App.getInstance()).getLanguage().equalsIgnoreCase("ar")) {
            return new LanguagePref(App.getInstance()).getLanguage();
        } else {
            return "en";
        }
    }*/

/*    public static String getDeviceToken(){
        return  FirebaseInstanceId.getInstance().getToken();
    }*/
}
