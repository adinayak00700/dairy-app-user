package user.techokissan.utilities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.StrictMode;


import com.wedguruphotographer.ActivityNoAvailability;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import user.techokissan.R;
import user.techokissan.activity.ActivityBill;
import user.techokissan.activity.ActivityDashboard;
import user.techokissan.activity.ActivityFinalPayment;
import user.techokissan.activity.ActivityForgotPassword;
import user.techokissan.activity.ActivityLogin;
import user.techokissan.activity.ActivityMySubscriptions;
import user.techokissan.activity.ActivityProductList;
import user.techokissan.activity.ActivityReview;
import user.techokissan.activity.ActivityVendor;
import user.techokissan.activity.CartScreenActivity;

public class IntentHelper {

  public static Intent getDashboardActivity(Context context){
        return new Intent(context, ActivityDashboard.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }



   public static Intent getLoginScreen(Context context){
        return new Intent(context, ActivityLogin.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }


    public static Intent getForgotPassword(Context context){
        return new Intent(context, ActivityForgotPassword.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getPaymentScreen(Context context){
        return new Intent(context, ActivityFinalPayment.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getMySubscriptionScreen(Context context){
        return new Intent(context, ActivityMySubscriptions.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getBillActivity(Context context){
        return new Intent(context, ActivityBill.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getProductsLIstScreen(Context context){
        return new Intent(context, ActivityProductList.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getVendorListScreen(Context context){
        return new Intent(context, ActivityVendor.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getReviewScreen(Context context){
        return new Intent(context, ActivityReview.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getNoAvailabilityScreen(Context context){
        return new Intent(context, ActivityNoAvailability.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }


    public static Intent getCartScreen(Context context){
        return new Intent(context, CartScreenActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }


    public static void shareAPP(Context context, String description) {  // it will share this app
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
            String shareMessage = description+"\n\n";
            //   shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            //  shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + "com.agricoaching.app" + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(shareIntent, "Share Via"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void chatWhatsApp(Context context){ // will redirect to WhatsApp to particular contact
        String url = "https://api.whatsapp.com/send?phone="+"+910000000000";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static void sharePostWithPhoto(Context context, String postId, Bitmap bitmap) {  // will send a photo to any other application
        File file=new File(context.getExternalCacheDir()+"/"+context.getResources().getString(R.string.app_name)+".png");
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            FileOutputStream fileOutputStream=new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG,100,fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            Intent intent=new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/*");
            context.startActivity(Intent.createChooser(intent,"share"));
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }




}
