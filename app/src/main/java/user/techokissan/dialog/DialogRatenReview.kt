package user.techokissan.dialog;

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.mechanicforyoubusiness.utilities.PrefManager

import retrofit2.Call
import retrofit2.Response
import user.techokissan.R
import user.techokissan.databinding.DialogRateNReviewBinding
import user.techokissan.models.ModelSuccess
import user.techokissan.networking.RestClient


class DialogRatenReview(private val context: Context,var productName:String,var vendorImage:String,var productId:String) {
    private var dialog: Dialog? = null
    private lateinit var binding: DialogRateNReviewBinding
    var isDialogShowing = false
        private set

    fun show() {
        dialog = Dialog(context, R.style.MyCustomDialogTheme)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_rate_n_review, null)
        binding= DataBindingUtil.bind(view)!!;
        dialog!!.setContentView(binding.root);
        dialog!!.setCancelable(true)
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(null)

            dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.setLayout(getWidth(context) / 100 * 90, LinearLayout.LayoutParams.WRAP_CONTENT)
            binding!!.tvTitle.text="Rate & Review"
            dialog!!.window!!.setDimAmount(0.5f)
            //dialog.getWindow().setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        binding.buttonCancel.setOnClickListener {
            dismiss()
        }
        binding.buttonSubmit.setOnClickListener {
            Log.d("alishdashlda",binding!!.ratingBar.numStars.toString())
            ratenReviewApi(binding!!.ratingBar.rating.toString(),binding.editText.text.toString())
        }
        binding.tvvendorNae.text=productName

        dialog!!.show()
        isDialogShowing = true
    }

    fun dismiss() {
        isDialogShowing = false
        dialog!!.dismiss()
    }



    companion object {
        fun getWidth(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }
    }

    private fun ratenReviewApi(rate:String,review:String) {
        val hashMap: HashMap<String, String> = HashMap()
      hashMap["token"] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["product_id"] = productId
        hashMap["rate"] = rate
        hashMap["review"] = review
        

        RestClient.getInst().ADD_RATING(hashMap).enqueue(object : retrofit2.Callback<ModelSuccess?> {
            override fun onResponse(call: Call<ModelSuccess?>?, response: Response<ModelSuccess?>) {
                if (response.body()!!.result) {
                    dismiss()
                  Toast.makeText(context,"Feedback Added Successfully",Toast.LENGTH_SHORT).show()
                } else {

                }
            }

            override fun onFailure(call: Call<ModelSuccess?>?, t: Throwable) {

            }



        })



    }
}