package user.techokissan.base

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import java.text.ParseException
import java.text.SimpleDateFormat

object ApiUtils {

    @SuppressLint("SimpleDateFormat")
    @Throws(ParseException::class)
    fun dateFormat(date: String?): String? {
//        var date = "2020-02-25 13:16:54"
        var spf = SimpleDateFormat("yyyy-MM-dd")
        val newDate = spf.parse(date)
        spf = SimpleDateFormat("dd-MM-yyyy")
        return spf.format(newDate)
    }

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}