package user.techokissan.base;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import user.techokissan.callbacks.BaseCallBacks;




public class BaseFragment extends Fragment
        implements BaseCallBacks {

    private BaseCallBacks callBacks;

    @Override
    public void onAttach(Context context) {
        Log.d("fjha","ppp");
        super.onAttach(context);
        if(context instanceof BaseCallBacks){
            Log.d("fjha","pppuuu");
            callBacks = (BaseCallBacks)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callBacks = null;
    }


    @Override
    public void showLoader() {
        if(callBacks!=null){
            callBacks.showLoader();

        }

    }



    @Override
    public void dismissLoader() {
        if(callBacks!=null)callBacks.dismissLoader();
    }

    @Override
    public void onFragmentDetach(String fragmentTag) {
        if(callBacks!=null)callBacks.onFragmentDetach(fragmentTag);
    }

    public void makeToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }



/*    protected void showSnackbar(@NonNull String message) {
        View view = getActivity().findViewById(android.R.id.content);
        if (view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }*/


    protected void showSnackbar(String message) {



        Snackbar snack = Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        snack.show();
    }



}
