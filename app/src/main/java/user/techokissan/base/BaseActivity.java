package user.techokissan.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import user.techokissan.callbacks.BaseCallBacks;

public abstract class BaseActivity extends AppCompatActivity
        implements View.OnClickListener, BaseCallBacks
{


    public static final int READ_WRITE_STORAGE = 52;
    public static final int MULTIPLE_PERMISSIONS = 10;
    boolean isGPSAvailable = false;
    boolean isInternetAvailable = false;
    private user.techokissan.dialog.ProgressDialog progressDialog;
    private ProgressDialog mProgressDialog;
    private static List<AlertDialog> sessionExpireDialogList = new ArrayList<>();
    private static List<AlertDialog> errorDialogList = new ArrayList<>();

    private boolean isInBAckground = false;
    private Context context;
    public abstract void onClick(int viewId, View view);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new user.techokissan.dialog.ProgressDialog(this);
        context = this;
    }




    public void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void makeLongToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // case R.id.toolbar_back: onBackPressed(); break;
            default:
                onClick(v.getId(), v);
                break;
        }
    }

    @Override
    public void showLoader() {
        try {
            if (!isFinishing() && !progressDialog.isShowing()) progressDialog.showDialog(user.techokissan.dialog.ProgressDialog.DIALOG_CENTERED);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void dismissLoader() {
        if (!isFinishing() && progressDialog.isShowing()) progressDialog.dismiss();
    }

    protected void showLoading(@NonNull String message) {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(android.app.ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void hideLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    protected void showSnackbar(@NonNull String message) {
        View view = findViewById(android.R.id.content);
        if (view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onFragmentDetach(String fragmentTag) {
    }



    public boolean requestPermission(String permission) {
        boolean isGranted = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{permission}, READ_WRITE_STORAGE);
        }
        return isGranted;
    }


    public boolean isPermissionGranted(String[] permissions) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRITE_STORAGE:
                isPermissionGranted(grantResults[0] == PackageManager.PERMISSION_GRANTED, permissions[0]);
                break;
        }
    }

    public void isPermissionGranted(boolean isGranted, String permission) {

    }




}

