import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import user.techokissan.R
import user.techokissan.models.SubCategory


class SubCategoryAdapter(
    var context: Context,
    var list: List<SubCategory>,
    var callback: Callbackk
) :
    RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder>() {
    var row_index = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.sub_cat_rows, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {

            row_index = position
            notifyDataSetChanged()
            callback.onClickOnTopCat(list.get(position).id)

        }
       holder.catName.text = list.get(position).cat_name



        if (row_index == position) {
            holder.backgroundLayout.setBackground(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.small_tertiary_round_corner
                )
            )


            if (list.get(position).id=="11"){
                holder.backgroundLayout.setBackground(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.small_egg_color_round_corner
                    )
                )
            }



            holder.catName.setTextColor(ContextCompat.getColor(context, R.color.white))
        } else {
            holder.backgroundLayout.setBackground(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.small_tertialry_round_corner_border
                )
            )

            if (list.get(position).id=="11"){//egg
                holder.backgroundLayout.setBackground(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.small_egg_color_round_corner_border
                    )
                )
            }

            holder.catName.setTextColor(ContextCompat.getColor(context, R.color.black))
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }
    fun setCurrentCat(position: Int) {
        row_index=position
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var catName = itemView.findViewById<TextView>(R.id.tvStartTime)
        var backgroundLayout = itemView.findViewById<ConstraintLayout>(R.id.mainLayout)
    }


    interface Callbackk {
        fun onClickOnTopCat(catId: String);
    }
}