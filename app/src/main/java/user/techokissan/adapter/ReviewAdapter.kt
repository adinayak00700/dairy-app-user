import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.activity.ProductDetailsScreenActivity
import user.techokissan.activity.ProductShowDetailsScreenActivity
import user.techokissan.databinding.ProductRowsBinding
import user.techokissan.databinding.ReviewRowsBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.Product
import user.techokissan.models.ReviewData
import user.techokissan.networking.RestClient


class ReviewAdapter(var context: Context, var callback: Callbackk) :
    RecyclerView.Adapter<ReviewAdapter.MyViewHolder>() {
    var reviewList = ArrayList<ReviewData>();

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.review_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<ReviewRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(reviewList[position])


    }

    override fun getItemCount(): Int {
        return reviewList.size
    }

    public interface Callbackk {
        fun onCartValueChanged()
    }

    inner class MyViewHolder(var binding: ReviewRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: ReviewData) {
            binding.tvSubTitle.text = product.review
            binding.tvTitle.text = product.user_name
            binding.ratingBarrr.rating=product.rate.toString().toFloat()
            Glide.with(context).load(product.product_image).into(binding.ivProductImage)
        }
    }

    fun addReviews(data: List<ReviewData>) {
        this.reviewList = data as ArrayList<ReviewData>
    }

    private fun addToCart(qty: String, productID: String, package_id: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["productID"] = productID
        hashMap["qty"] = qty
        hashMap["package_id"] = package_id
        RestClient.getInst().ADD_PRODUCT(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {
                    callback.onCartValueChanged()
                } else {
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }


    private fun makeToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}