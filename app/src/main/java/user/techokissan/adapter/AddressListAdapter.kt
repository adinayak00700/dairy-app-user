package user.techokissan.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import user.techokissan.R
import user.techokissan.databinding.ItemListUserAddressBinding
import user.techokissan.models.UserAddressDataListRes

class AddressListAdapter(private  var context: Context, private var data: ArrayList<UserAddressDataListRes>) : RecyclerView.Adapter<AddressListAdapter.ViewHolder>() {

    class ViewHolder(var viewBinding: ItemListUserAddressBinding) : RecyclerView.ViewHolder(viewBinding.root)

  //  private  var viewModel: AddressVM? = null
   private var  onItemListener : OnRemoveAddressListener
    private var selectedPosition = -1


    init {
        onItemListener  = context as OnRemoveAddressListener
    }

 /*   override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        if (viewModel == null) {
            viewModel = ViewModelProvider((recyclerView.context as ViewModelStoreOwner))[AddressVM::class.java]
            viewModel!!.ctx = context
        }
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListUserAddressBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

       /* if(selectedPosition == position){
            holder.viewBinding.mCircleCheckIV.load(R.drawable.fill_radio)
        }else{
            holder.viewBinding.mCircleCheckIV.load(R.drawable.not_fill_radio)
        }*/


        holder.viewBinding.mLocalityTV.text = data[position].pin_code
        holder.viewBinding.mMobileTV.text = "+91 " +data[position].phone
        holder.viewBinding.mUserNameTV.text = data[position].building_name
        holder.viewBinding.mWorkTV.text = data[position].type

        when {
            data[position].type.equals("Home" , true) -> {
                Picasso.get().load(R.drawable.ic_home).placeholder(R.drawable.ic_home).into(holder.viewBinding.addressIcon)
            }
            data[position].type.equals("Work" , true) -> {
                Picasso.get().load(R.drawable.ic_office).placeholder(R.drawable.ic_office).into(holder.viewBinding.addressIcon)
            }
            else -> {
                Picasso.get().load(R.drawable.ic_othher).placeholder(R.drawable.ic_othher).into(holder.viewBinding.addressIcon)

            }
        }

      /*  holder.viewBinding.mCircleCheckIV.setOnClickListener{
            selectedPosition= position
            //onItemListener.onUserAddressList(data[position])
            notifyDataSetChanged()
        }
*/
        holder.viewBinding.mHomeIV.setOnClickListener{
            notifyItemChanged(position)
            onItemListener.onRemoveClick(data , position)
        }


        holder.viewBinding.mCircleCheckIV.setOnClickListener{
            onItemListener.onCheckClick(data , position)
        }

    }

    override fun getItemCount(): Int {
        return data.size

    }


    interface OnRemoveAddressListener{
        fun onRemoveClick(data: ArrayList<UserAddressDataListRes>, position: Int)
        fun onCheckClick(data: ArrayList<UserAddressDataListRes>, position: Int)
    }
}