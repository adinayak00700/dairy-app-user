package user.techokissan.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import user.techokissan.activity.OrderDetailsScreenActivity
import user.techokissan.databinding.ItemListBookingHistoryBinding
import user.techokissan.models.OrderHistoryDataRes

class OrderHistoryAdapter(
    private var context: Context,
    private var mBannerList: ArrayList<OrderHistoryDataRes>
) : RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder>() {


    class ViewHolder(var viewBinding: ItemListBookingHistoryBinding) : RecyclerView.ViewHolder(viewBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListBookingHistoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false))
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.viewBinding.mOIDTV.text = "Order ID: "+mBannerList[position].id
        holder.viewBinding.mItemCountTV.text = "Name : "+mBannerList[position].customer_name
        holder.viewBinding.mInstruction.text = "Address :"+mBannerList[position].address_type
        holder.viewBinding.mProcessTV.text =  mBannerList[position].status
        holder.viewBinding.mAmtTV.text = "Total Amt : "+mBannerList[position].total_amount
        holder.viewBinding.mPaymentTV.text = mBannerList[position].delivery_date + " " + mBannerList[position].delivery_slot


        holder.viewBinding.mViewMoreDetails.setOnClickListener {
            val intent = Intent(context, OrderDetailsScreenActivity::class.java)
            intent.putExtra("position", position)
            intent.putExtra("data", mBannerList)
            context.startActivity(intent)

        }






    }

    override fun getItemCount(): Int {
        return mBannerList.size
    }

}