package user.techokissan.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.activity.ActivitySubscriptionNewwLatest
import user.techokissan.activity.SubscriptionScreenActivity
import user.techokissan.activity.SubscriptionScreenActivityNew
import user.techokissan.databinding.ItemListSubscriptionPackageBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.ProductPackageRes
import user.techokissan.networking.RestClient

class SubscriptionPackageAdapter(
    private var context: Context,
    private var mPackage: ArrayList<ProductPackageRes>,
    private var productID: String
) : RecyclerView.Adapter<SubscriptionPackageAdapter.ViewHolder>() {

    class ViewHolder(var viewBinding: ItemListSubscriptionPackageBinding) : RecyclerView.ViewHolder(viewBinding.root)


    /*  @SuppressLint("UseCompatLoadingForDrawables")
      private val images = intArrayOf(
          R.drawable.ic_rectangle_first ,
          R.drawable.ic_rectangle_second  , R.drawable.ic_rectangle_third , R.drawable.ic_rectangle_fourth)
  */

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListSubscriptionPackageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //Glide.with(context).load(mPackage[position].image).into(holder.viewBinding.restroImage)
         holder.viewBinding.title.text =mPackage[position].name
        val a = Math.round(mPackage[position].amount.toDouble()/mPackage[position].qty.toInt()).toInt()
         holder.viewBinding.tvSubscriptiponPageSubHead.text =mPackage[position].qty+" packs @ ₹ "+a+"/pk"


        if (mPackage[position].trial=="Y"){
            Log.d("asdasdasdad",mPackage[position].trial)
          holder.viewBinding.tvTrial.text="Trial"
            holder.viewBinding.tvTrial.visibility= View.VISIBLE
        }else{
            holder.viewBinding.tvTrial.text=""
            holder.viewBinding.tvTrial.visibility= View.GONE
        }
        holder.viewBinding.mAddLL.setOnClickListener {
            val intent = Intent(context , ActivitySubscriptionNewwLatest::class.java)
            intent.putExtra("productID" , productID)
            intent.putExtra("pacamount" , productID)
            intent.putExtra("packageIndex" , position.toString())
            intent.putExtra("isTrial" , mPackage[position].trial)
            context.startActivity(intent)
       }

       // holder.viewBinding.mBgLL.setBackgroundResource(images[position % 3])

      /* holder.viewBinding.mAddLL.setOnClickListener {
           mPackage[position].cartCount++
               holder.viewBinding.mCountLL.visibility = View.VISIBLE
               holder.viewBinding.mAddLL.visibility = View.GONE
               holder.viewBinding.mCountTV.text = mPackage[position].cartCount.toString()
               addToCart(mPackage[position].cartCount.toString() ,mPackage[position].product_id , mPackage[position].id )

       }

       holder.viewBinding.mPlusLL.setOnClickListener {
           mPackage[position].cartCount++
           holder.viewBinding.mCountLL.visibility = View.VISIBLE
           holder.viewBinding.mAddLL.visibility = View.GONE
           holder.viewBinding.mCountTV.text = mPackage[position].cartCount.toString()
           addToCart(mPackage[position].cartCount.toString() ,mPackage[position].product_id , mPackage[position].id)
       }

       holder.viewBinding.mMinusLL.setOnClickListener {
           mPackage[position].cartCount--
           if (mPackage[position].cartCount > 0){
               holder.viewBinding.mCountTV.text = mPackage[position].cartCount.toString()
               holder.viewBinding.mCountLL.visibility = View.VISIBLE
               holder.viewBinding.mAddLL.visibility = View.GONE
               addToCart(mPackage[position].cartCount.toString() ,mPackage[position].product_id , mPackage[position].id)
           }else{
               holder.viewBinding.mCountLL.visibility = View.GONE
               holder.viewBinding.mAddLL.visibility = View.VISIBLE
               addToCart(mPackage[position].cartCount.toString() ,mPackage[position].product_id , mPackage[position].id)
           }
       }*/

   }

   override fun getItemCount(): Int {
       return  mPackage.size

   }
   private fun addToCart(qty: String, productID: String ,  package_id : String) {
       val hashMap: HashMap<String, String> = HashMap()
       hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(context)!!.userDetail.token
       hashMap["productID"] = productID
       hashMap["qty"] = qty
       hashMap["package_id"] = package_id
       RestClient.getInst().ADD_PRODUCT(hashMap).enqueue(object :
           Callback<AddProductModelRes?> {
           override fun onResponse(
               call: Call<AddProductModelRes?>?,
               response: Response<AddProductModelRes?>
           ) {
               if (response.body()!!.result) {
                   Toast.makeText(context , response.body()!!.message , Toast.LENGTH_SHORT).show()

               } else {
               }
           }

           override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

           }
       })
   }
}