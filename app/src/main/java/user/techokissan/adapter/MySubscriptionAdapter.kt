package user.techokissan.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.activity.SubscriptionScreenActivity
import user.techokissan.databinding.ItemListSubscriptionPackageBinding
import user.techokissan.databinding.MySubsRowsBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.ProductPackageRes
import user.techokissan.models.SubscriptionData
import user.techokissan.networking.RestClient
import user.techokissan.utilities.IntentHelper

class MySubscriptionAdapter(
    private var context: Context,
) : RecyclerView.Adapter<MySubscriptionAdapter.ViewHolder>() {


    class ViewHolder(var viewBinding: MySubsRowsBinding) : RecyclerView.ViewHolder(viewBinding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            MySubsRowsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.viewBinding.title.text = list.get(position).packageDetail.name
        holder.viewBinding.tvSubscriptiponPageSubHead.text =
            list[position].packageDetail.qty + " packs @ ₹ " + list[position].packageDetail.amount.toDouble() / list[position].packageDetail.qty.toInt() + "/pk"
        holder.viewBinding.tvSubType.text = list.get(position).subscription
        holder.viewBinding.tvExpiry.text = "Expires on " + list.get(position).exp_date

        if (list.get(position).packageDetail.trial==("N")) {
            holder.viewBinding.tvTrial.text = ""
            holder.viewBinding.tvTrial.visibility = View.GONE
        } else {
            holder.viewBinding.tvTrial.text = "Trial Pack"
            holder.viewBinding.tvTrial.visibility = View.VISIBLE
        }


        holder.viewBinding.root.setOnClickListener {
            context.startActivity(IntentHelper.getNoAvailabilityScreen(context).putExtra("data",list[position]))
        }

        //Glide.with(context).load(list[position].image).into(holder.viewBinding.restroImage)
/*
        holder.viewBinding.title.text =list[position].name
        holder.viewBinding.tvSubscriptiponPageSubHead.text =list[position].qty+" packs @ ₹ "+list[position].amount.toDouble()/list[position].qty.toInt()+"/pk"
*/
/*
        holder.viewBinding.mAddLL.setOnClickListener {
            val intent = Intent(context , SubscriptionScreenActivity::class.java)
            intent.putExtra("productID" , productID)
            intent.putExtra("pacamount" , productID)
            intent.putExtra("productID" , productID)
            context.startActivity(intent)
        }*/


    }

    override fun getItemCount(): Int {
        return list.size

    }

    private fun addToCart(qty: String, productID: String, package_id: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["productID"] = productID
        hashMap["qty"] = qty
        hashMap["package_id"] = package_id
        RestClient.getInst().ADD_PRODUCT(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {
                    Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()

                } else {
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }

    var list = ArrayList<SubscriptionData>()
    fun addPackages(list: List<SubscriptionData>) {
        this.list = list as ArrayList<SubscriptionData>
    }

}