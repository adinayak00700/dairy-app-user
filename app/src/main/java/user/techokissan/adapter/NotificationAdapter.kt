import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import user.techokissan.R


class NotificationAdapter(var context: Context) :
    RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.notification_rows, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            //  callbackk.onClickOnAddress(list.get(position))
        }

    }

    override fun getItemCount(): Int {
        return 2
    }

    public interface Callbackk{
        //  fun onClickOnAddress(addressId:ModelAddress.DataBean)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }



    private fun makeToast(message: String?) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }


}