package user.techokissan.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import user.techokissan.R
import user.techokissan.base.ApiUtils
import user.techokissan.databinding.ItemListCustomDaysBinding
import user.techokissan.databinding.ItemListDateLayoutBinding
import user.techokissan.models.CustomDate

class CustomeDateAdapter (
    private var context: Context,
    private var data: ArrayList<CustomDate>,
) : RecyclerView.Adapter<CustomeDateAdapter.ViewHolder>() {

    private var selectedPosition = -1
    class ViewHolder(var viewBinding: ItemListCustomDaysBinding) : RecyclerView.ViewHolder(viewBinding.root)

    private var   onDateListener : OnDateListener

    init {
        onDateListener  = context as OnDateListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemListCustomDaysBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false))
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        println("==>" + Gson().toJson(data))

        // val ischecked = if (position === checkedposition) true else false
        holder.viewBinding.mDateLL.isActivated = data[position].isSelected

        if (selectedPosition == position ){
            holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.viewBinding.mDayTV.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.coustom_button)
        }else{

            holder.viewBinding.mDateTV.setTextColor(ContextCompat.getColor(context, R.color.black))
            holder.viewBinding.mDayTV.setTextColor(ContextCompat.getColor(context, R.color.black))
            holder.viewBinding.mDateLL.background = ContextCompat.getDrawable(context, R.drawable.unselected_bg)
        }


       // holder.viewBinding.mDateTV.text = ApiUtils.dateFormat(data[position].date) + " ( " + data[position].days + " ) "
        holder.viewBinding.mDateTV.text = ApiUtils.dateFormat(data[position].date)
        holder.viewBinding.mDayTV.text = data[position].days

        holder.viewBinding.mDateLL.setOnClickListener {
            selectedPosition = position
            onDateListener.onDateClick(position , data )
            notifyDataSetChanged()

        }
    }

    override fun getItemCount(): Int {
        return data.size

    }


    interface OnDateListener{
        fun onDateClick(position: Int, data: ArrayList<CustomDate>)
    }
}