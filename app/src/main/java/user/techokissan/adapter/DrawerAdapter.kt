package user.techokissan.adapter;

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import user.techokissan.R
import user.techokissan.databinding.LeftMenuRowsBinding


class DrawerAdapter(private val context: Context, private val callback: Callback) :
    BaseAdapter() {
    var binding: LeftMenuRowsBinding? = null
    private val icons: ArrayList<Drawable?> = ArrayList<Drawable?>()
    private val title = ArrayList<String>()
    override fun getCount(): Int {
       return icons.size
    }

    override fun getItem(position: Int): Any {
        TODO("Not yet implemented")
    }




    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = LayoutInflater.from(context).inflate(R.layout.left_menu_rows, parent, false)
        binding = DataBindingUtil.bind(view)
        binding!!.tvTitle.setText(title[position])
        binding!!.ivIcon.setImageDrawable(icons[position])
        binding!!.getRoot()
                .setOnClickListener(View.OnClickListener { callback.onClickonDraweerItem(position) })

        if(position==1){
            binding!!.root!!.layoutParams = AbsListView.LayoutParams(-1, 1)
            binding!!.root!!.visibility = View.GONE
        }
        return view
    }


    interface Callback {
        fun onClickonDraweerItem(position: Int)
    }

    init {
        title.add(context.resources.getString(R.string.my_profile))
        title.add(context.resources.getString(R.string.calender))
/*        title.add(context.resources.getString(R.string.go_app_money))
        title.add(context.resources.getString(R.string.rc_challan))*/
        title.add(context.resources.getString(R.string.wallet))
/*        title.add(context.resources.getString(R.string.payment))*/
        title.add(context.resources.getString(R.string.subscription))
        title.add(context.resources.getString(R.string.order_history))
        title.add(context.resources.getString(R.string.bills))
        title.add(context.resources.getString(R.string.contact_us))
        title.add(context.resources.getString(R.string.terms_of_use))
        title.add(context.resources.getString(R.string.logout))



        icons.add(context.resources.getDrawable(R.drawable.user_p))
        icons.add(context.resources.getDrawable(R.drawable.calendar_p))
        icons.add(context.resources.getDrawable(R.drawable.wallet_p))
        icons.add(context.resources.getDrawable(R.drawable.subscription))
        icons.add(context.resources.getDrawable(R.drawable.history))
        icons.add(context.resources.getDrawable(R.drawable.history))
        icons.add(context.resources.getDrawable(R.drawable.telephone_call_new) )
        icons.add(context.resources.getDrawable(R.drawable.accept))
        icons.add(context.resources.getDrawable(R.drawable.logout))
    }

}