import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.databinding.QuantityRowsBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.ModelQuantity
import user.techokissan.networking.RestClient


class QuantityAdapter(var context: Context,var callback:Callbackk) :
    RecyclerView.Adapter<QuantityAdapter.MyViewHolder>() {
    var quantityList = ArrayList<ModelQuantity>();

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.quantity_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<QuantityRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(quantityList[position])



    }
    public fun setTotalAvailableQuantityy(quantityyy:Int){
       totalAvailableQuantity =quantityyy
    }

    private var totalAvailableQuantity=0


    public interface Callbackk{
        fun onQuantityChange();
    }

    override fun getItemCount(): Int {
        return quantityList.size
    }


    inner class MyViewHolder(var binding: QuantityRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(quantitymodel: ModelQuantity) {
            binding.tvTitle.text=quantitymodel.title
            binding.tvQuantity.text=quantitymodel.quantity.toString()
            binding.ivAdd.setOnClickListener {


               if (getTotalSelectedQuantityCount()<totalAvailableQuantity){
                   quantityList[adapterPosition].quantity++
                   notifyItemChanged(adapterPosition)
                   callback.onQuantityChange()
               }else{
                   Toast.makeText(context,"Limit Reached",Toast.LENGTH_SHORT).show()
               }
            }

            binding.idMinus.setOnClickListener {

                if (quantityList[adapterPosition].quantity!=0){
                    quantityList[adapterPosition].quantity--
                    notifyItemChanged(adapterPosition)
                    callback.onQuantityChange()
                }

            }


        }
    }

    fun getSelectedQuantity() : String{
        var stringArray=ArrayList<String>()
        for (i in 0 until quantityList.size){
            stringArray.add(quantityList[i].quantity.toString())

        }
        val s: String = TextUtils.join(", ", stringArray)
        var x="["+s+"]"
        return x
    }


    fun getTotalSelectedQuantityCount():Int{
        var totalQuantity=0
        var stringArray=ArrayList<String>()
        for (i in 0 until quantityList.size){
            totalQuantity += quantityList[i].quantity

        }

        return totalQuantity;
    }




 fun addToList(listt:ArrayList<ModelQuantity>){
     this.quantityList=listt
 }

    private fun makeToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}