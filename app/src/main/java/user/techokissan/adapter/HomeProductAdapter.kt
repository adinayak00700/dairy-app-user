import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.activity.ProductShowDetailsScreenActivity
import user.techokissan.databinding.DailyNeedRowsBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.Product
import user.techokissan.networking.RestClient


class HomeProductAdapter(var context: Context) :
    RecyclerView.Adapter<HomeProductAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.daily_need_rows, parent, false)

        return MyViewHolder(DataBindingUtil.bind<DailyNeedRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            //  callbackk.onClickOnAddress(list.get(position))
        }

        holder.bind(productsList[position])

        holder.bind(productsList[position])

        if (productsList[position].`package`.size>0 ){
            holder.binding.mAddTV.text = "Buy"
      //      holder.binding.mCardCV.isClickable = false
        }else{
            holder.binding.mAddTV.text = "Add To Cart"
           // holder.binding.mCardCV.isClickable = true
            if (productsList[position].cartCount > 0){
                holder.binding.mCountLL.visibility = View.VISIBLE
                holder.binding.mAddLL.visibility = View.GONE
                holder.binding.mCountTV.text = productsList[position].cartCount.toString()
            }else{
                holder.binding.mCountLL.visibility = View.GONE
                holder.binding.mAddLL.visibility = View.VISIBLE
            }

        }

        if (productsList[position].`package`.isNotEmpty()){
            holder.binding.mCartItemCountLL.visibility+View.GONE
            //holder.binding.mCardCV.isClickable = false
        }else{
            holder.binding.mCartItemCountLL.visibility+View.VISIBLE
            // holder.binding.mCardCV.isClickable = true
        }
  /*      holder.binding.mCardCV.setOnClickListener{
            if (!productsList[position].`package`.isNotEmpty()){
                val intent = Intent(context , ProductDetailsScreenActivity::class.java)
                intent.putExtra("product" , productsList)
                intent.putExtra("position" , position)
                context.startActivity(intent)
            }

        }
*/





        holder.binding.mAddLL.setOnClickListener {
            if(holder.binding.mAddTV.text.toString().equals("Buy", true)){
                val intent = Intent(context , ProductShowDetailsScreenActivity::class.java)
                intent.putExtra("productID" , productsList[position].id)
                context.startActivity(intent)
            }else{
                productsList[position].cartCount++
                holder.binding.mCountLL.visibility = View.VISIBLE
                holder.binding.mAddLL.visibility = View.GONE
                holder.binding.mCountTV.text = productsList[position].cartCount.toString()
                addToCart(productsList[position].cartCount.toString() ,productsList[position].id , "0")
            }

        }

        holder.binding.mPlusLL.setOnClickListener {
            productsList[position].cartCount++
            holder.binding.mCountLL.visibility = View.VISIBLE
            holder.binding.mAddLL.visibility = View.GONE
            holder.binding.mCountTV.text = productsList[position].cartCount.toString()
            addToCart(productsList[position].cartCount.toString() ,productsList[position].id , "0")
        }

        holder.binding.mMinusLL.setOnClickListener {
            productsList[position].cartCount--
            if (productsList[position].cartCount > 0){
                holder.binding.mCountTV.text = productsList[position].cartCount.toString()
                holder.binding.mCountLL.visibility = View.VISIBLE
                holder.binding.mAddLL.visibility = View.GONE
                addToCart(productsList[position].cartCount.toString() ,productsList[position].id , "0")
            }else{
                holder.binding.mCountLL.visibility = View.GONE
                holder.binding.mAddLL.visibility = View.VISIBLE
                addToCart(productsList[position].cartCount.toString() ,productsList[position].id, "0")
            }
        }

    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    public interface Callbackk {
        //  fun onClickOnAddress(addressId:ModelAddress.DataBean)
    }

    inner class MyViewHolder(var binding: DailyNeedRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product:Product) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (adapterPosition % 2 == 0) {
                    binding.cardView.setCardBackgroundColor(context.getColor(R.color.colorVeryLightPrimary))
                } else {
                    binding.cardView.setCardBackgroundColor(context.getColor(R.color.colorVeryLightSecondary))
                }
            }
            Glide.with(context).load(product.product_image).into(binding.ivProductImage)
            binding.tvProductName.text=product.product_name
            binding.tvWeight.text=product.unit_value+" "+product.unit.toString()
            binding.tvPrice.text="₹ "+product.price
            binding.tvPriceWithoutDiscount.text="₹ "+product.mrp
            binding.tvPriceWithoutDiscount.setPaintFlags(binding.tvPriceWithoutDiscount.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
            var off=(100-(product.price.toDouble()*100/product.mrp.toDouble())).toInt()
            binding.tvDiscount.setText(off.toString()+" %Off")
            binding.tvButtonBuyNow.text=product.unit_value+" "+product.unit.toString()
        }
    }


    var productsList = ArrayList<Product>();


    fun addProducts(data: List<Product>) {
        this.productsList = data as ArrayList<Product>
    }



    private fun addToCart(qty: String, productID: String ,  package_id : String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["productID"] = productID
        hashMap["qty"] = qty
        hashMap["package_id"] = package_id
        RestClient.getInst().ADD_PRODUCT(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {

                } else {
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }


    private fun makeToast(message: String?) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }


}