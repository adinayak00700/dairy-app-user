import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import user.techokissan.R
import user.techokissan.databinding.DailyNeedRowsBinding
import user.techokissan.databinding.TransactionRowsBinding
import user.techokissan.models.Transactionn


class TransactionAdapter(var context: Context) :
    RecyclerView.Adapter<TransactionAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.transaction_rows, parent, false)

        return MyViewHolder(DataBindingUtil.bind<TransactionRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            //  callbackk.onClickOnAddress(list.get(position))
        }

        holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    public interface Callbackk{
        //  fun onClickOnAddress(addressId:ModelAddress.DataBean)
    }

    inner class MyViewHolder(var binding: TransactionRowsBinding) : RecyclerView.ViewHolder(binding.root) {
fun bind(transaction:Transactionn){
    binding.tvAmount.text="INR "+transaction.amount

    if (transaction.against_for.equals("wallet")){
        binding.tvTitle.text="Added to wallet"
        binding.tvSubTitle.visibility=View.INVISIBLE
    }else if (transaction.against_for.equals("order")){
        binding.tvTitle.text="Paid for Order"
        binding.tvSubTitle.text="Order Id : "+transaction.orderID
    }

    binding.tvDate.text=transaction.transaction_at


    if (transaction.type.equals("CREDIT")){
        binding.ivTransaction.setImageDrawable(context.getDrawable(R.drawable.greenbutton))
    }else{
        binding.ivTransaction.setImageDrawable(context.getDrawable(R.drawable.redbutton))
    }

}
    }
    var list=ArrayList<Transactionn>()
    public  fun addTransactions(list: List<Transactionn>){
        this.list= list as ArrayList<Transactionn>
        notifyDataSetChanged()
    }





    private fun makeToast(message: String?) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }


}