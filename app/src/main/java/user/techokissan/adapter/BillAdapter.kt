package user.techokissan.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.activity.SubscriptionScreenActivity
import user.techokissan.databinding.BillRowsBinding
import user.techokissan.databinding.ItemListSubscriptionPackageBinding
import user.techokissan.databinding.MySubsRowsBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.ProductPackageRes
import user.techokissan.models.SubscriptionData
import user.techokissan.networking.RestClient

class BillAdapter(
    private var context: Context,var callback: Callbackk
) : RecyclerView.Adapter<BillAdapter.ViewHolder>() {

public  interface  Callbackk{
    fun onClickONBill(bill :SubscriptionData)
}
    class ViewHolder(var viewBinding: BillRowsBinding) : RecyclerView.ViewHolder(viewBinding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            BillRowsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.viewBinding.title.text = list.get(position).packageDetail.name
        holder.viewBinding.tvSubscriptiponPageSubHead.text =
            list[position].packageDetail.qty + " packs @ ₹ " + list[position].packageDetail.amount.toDouble() / list[position].packageDetail.qty.toInt() + "/pk"
        holder.viewBinding.tvSubType.text = list.get(position).subscription
        holder.viewBinding.tvExpiry.text = "Expires on " + list.get(position).exp_date
        holder.viewBinding.tvBillGenDate.visibility=View.GONE
        holder.viewBinding.tvTotalAmount.text = "Amount INR " + list.get(position).billAmount

        if (list.get(position).packageDetail.trial==("N")) {
            holder.viewBinding.tvTrial.text = ""
            holder.viewBinding.tvTrial.visibility = View.GONE
        } else {
            holder.viewBinding.tvTrial.text = "Trial Pack"
            holder.viewBinding.tvTrial.visibility = View.VISIBLE
        }

        if (list[position].payment=="pending"){
            holder.viewBinding.tvStatus.text ="Pending"
        }else{
            holder.viewBinding.tvStatus.text ="Paid"
        }

        holder.itemView.setOnClickListener {
         if (list[position].payment=="pending"){
             callback.onClickONBill(list[position])
         }
        }


    }

    override fun getItemCount(): Int {
        return list.size

    }


    var list = ArrayList<SubscriptionData>()
    fun addPackages(list: List<SubscriptionData>) {
        this.list = list as ArrayList<SubscriptionData>
    }

}