package user.techokissan.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import user.techokissan.databinding.ItemListOrderDataBinding
import user.techokissan.dialog.DialogRatenReview
import user.techokissan.models.OrderItemsDataList

class OrderDetailsShowAdapter(
        private var context: Context,
        private var data: ArrayList<OrderItemsDataList>
) : RecyclerView.Adapter<OrderDetailsShowAdapter.ViewHolder>() {


    class ViewHolder(var viewBinding: ItemListOrderDataBinding) : RecyclerView.ViewHolder(viewBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                ItemListOrderDataBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.viewBinding.mDescTV.text = data[position].productName
      /*  holder.viewBinding.mSizeTextTV.text = "Size "
        holder.viewBinding.mTotalTextTV.text = "Amount "*/
       // holder.viewBinding.mNameTV.text = data[position].c
        holder.viewBinding.mAmtTV.text = "Order Amt ₹ " + data[position].price
        holder.viewBinding.mQtyTV.text = "Qty : " + data[position].qty
        holder.viewBinding.mTotalAmtTV.text = "Total Amt ₹ " +((data[position].qty.toDouble() * data[position].price.toDouble()).toString())


holder.viewBinding.tvrateValue.setOnClickListener {
    var addRatingDialog=DialogRatenReview(context,data[position].id,data[position].image,data[position].productID)
    addRatingDialog.show()
}

        if(data[position].image.isNullOrEmpty()) { } else {
            Glide.with(context).load(data[position].image).into(holder.viewBinding.mImgIV)
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

}