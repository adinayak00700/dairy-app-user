import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import user.techokissan.R
import user.techokissan.activity.ProductShowDetailsScreenActivity
import user.techokissan.databinding.VendorRowsBinding
import user.techokissan.models.Vendor
import user.techokissan.utilities.IntentHelper


class VendorAdapter(var context: Context, var callback: Callbackk) :
    RecyclerView.Adapter<VendorAdapter.MyViewHolder>() {
    var vendorList = ArrayList<Vendor>();

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.vendor_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<VendorRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(vendorList[position])
        holder.itemView.setOnClickListener {
            callback.onClickOnVendor(vendorList[position])
        }

    }

    override fun getItemCount(): Int {
        return vendorList.size
    }

    public interface Callbackk {
        fun onClickOnVendor(vendor: Vendor)
    }

    inner class MyViewHolder(var binding: VendorRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: Vendor) {

            binding.ratingBarrr.rating = product.ratingAvg.toFloat()
            binding.tvVendorName.text = product.name
            Glide.with(context).load(product.image).into(binding.ivVendorImage)

            binding.tvViewReport.setOnClickListener {
                /*              val intet = Intent(context , ProductShowDetailsScreenActivity::class.java)
                              intet.putExtra("vendorId" , product.id)
                              context.startActivity(intet)
             */

                try {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(product.image))
                    startActivity(context, browserIntent, null)
                } catch (e: Exception) {

                }

            }

            binding.tvViewReviews.setOnClickListener {


                context.startActivity(
                    IntentHelper.getReviewScreen(context).putExtra("productId", product.id)
                        .putExtra("type", "vendor")
                )

            }


        }
    }

    fun addProducts(data: List<Vendor>) {
        Log.d("asdasd", data.size.toString())
        this.vendorList = data as ArrayList<Vendor>
        notifyDataSetChanged()
    }


    private fun makeToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}