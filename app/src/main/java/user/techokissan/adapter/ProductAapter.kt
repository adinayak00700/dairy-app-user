import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datingapp.utilities.Constants
import com.example.mechanicforyoubusiness.utilities.PrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import user.techokissan.R
import user.techokissan.activity.ProductDetailsScreenActivity
import user.techokissan.activity.ProductShowDetailsScreenActivity
import user.techokissan.databinding.ProductRowsBinding
import user.techokissan.models.AddProductModelRes
import user.techokissan.models.Product
import user.techokissan.networking.RestClient


class ProductAdapter(var context: Context, var callback: Callbackk) :
    RecyclerView.Adapter<ProductAdapter.MyViewHolder>() {
    var productsList = ArrayList<Product>();

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.product_rows, parent, false)
        return MyViewHolder(DataBindingUtil.bind<ProductRowsBinding>(view)!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(productsList[position])

        if (productsList[position].`package`.size > 0) {
            holder.binding.mAddTV.text = "Buy"
            holder.binding.mCardCV.isClickable = false
        } else {
            holder.binding.mAddTV.text = "Add To Cart"
            holder.binding.mCardCV.isClickable = true
            if (productsList[position].cartCount > 0) {
                holder.binding.mCountLL.visibility = View.VISIBLE
                holder.binding.mAddLL.visibility = View.GONE
                holder.binding.mCountTV.text = productsList[position].cartCount.toString()
            } else {
                holder.binding.mCountLL.visibility = View.GONE
                holder.binding.mAddLL.visibility = View.VISIBLE
            }

        }

        if (productsList[position].`package`.isNotEmpty()) {
            holder.binding.mCartItemCountLL.visibility + View.GONE
            //holder.binding.mCardCV.isClickable = false
        } else {
            holder.binding.mCartItemCountLL.visibility + View.VISIBLE
            // holder.binding.mCardCV.isClickable = true
        }
        holder.binding.mCardCV.setOnClickListener {
            if (!productsList[position].`package`.isNotEmpty()) {
                val intent = Intent(context, ProductDetailsScreenActivity::class.java)
                intent.putExtra("product", productsList)
                intent.putExtra("position", position)
                context.startActivity(intent)
            }

        }






        holder.binding.mAddLL.setOnClickListener {
            if (holder.binding.mAddTV.text.toString().equals("Buy", true)) {
                val intent = Intent(context, ProductShowDetailsScreenActivity::class.java)
                intent.putExtra("productID", productsList[position].id)
                context.startActivity(intent)
            } else {

                productsList[position].cartCount++
                holder.binding.mCountLL.visibility = View.VISIBLE
                holder.binding.mAddLL.visibility = View.GONE
                holder.binding.mCountTV.text = productsList[position].cartCount.toString()
                addToCart(
                    productsList[position].cartCount.toString(),
                    productsList[position].id,
                    "0"
                )
            }

        }

        holder.binding.mPlusLL.setOnClickListener {
            productsList[position].cartCount++
            holder.binding.mCountLL.visibility = View.VISIBLE
            holder.binding.mAddLL.visibility = View.GONE
            holder.binding.mCountTV.text = productsList[position].cartCount.toString()
            addToCart(productsList[position].cartCount.toString(), productsList[position].id, "0")
        }

        holder.binding.mMinusLL.setOnClickListener {
            productsList[position].cartCount--
            if (productsList[position].cartCount > 0) {
                holder.binding.mCountTV.text = productsList[position].cartCount.toString()
                holder.binding.mCountLL.visibility = View.VISIBLE
                holder.binding.mAddLL.visibility = View.GONE
                addToCart(
                    productsList[position].cartCount.toString(),
                    productsList[position].id,
                    "0"
                )
            } else {
                holder.binding.mCountLL.visibility = View.GONE
                holder.binding.mAddLL.visibility = View.VISIBLE
                addToCart(
                    productsList[position].cartCount.toString(),
                    productsList[position].id,
                    "0"
                )
            }
        }


    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    public interface Callbackk {
        fun onCartValueChanged()
    }

    inner class MyViewHolder(var binding: ProductRowsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: Product) {
            binding.tvProductName.text = product.product_name
            binding.tvPrice.text = product.cost_price
            binding.tvQuantity.text = product.unit_value + " " + product.unit
            binding.tvProductName.text = product.product_name
            binding.tvPrice.text = "₹ " + product.price
            binding.tvQuantity.text = product.unit_value + " " + product.unit
/*binding.ratingBarrr.rating=product.ra*/
            Glide.with(context).load(product.product_image).into(binding.ivProductImage)

            /* binding.mCartCL.setOnClickListener {
                 val intet = Intent(context , ProductShowDetailsScreenActivity::class.java)
                 intet.putExtra("productID" , product.id)
                 context.startActivity(intet)
             }*/


        }
    }

    fun addProducts(data: List<Product>) {
        this.productsList = data as ArrayList<Product>
    }

    private fun addToCart(qty: String, productID: String, package_id: String) {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[Constants.USER_TOKEN] = PrefManager.getInstance(context)!!.userDetail.token
        hashMap["productID"] = productID
        hashMap["qty"] = qty
        hashMap["package_id"] = package_id
        RestClient.getInst().ADD_PRODUCT(hashMap).enqueue(object :
            Callback<AddProductModelRes?> {
            override fun onResponse(
                call: Call<AddProductModelRes?>?,
                response: Response<AddProductModelRes?>
            ) {
                if (response.body()!!.result) {
                    callback.onCartValueChanged()
                } else {
                }
            }

            override fun onFailure(call: Call<AddProductModelRes?>?, t: Throwable) {

            }
        })
    }


    private fun makeToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}